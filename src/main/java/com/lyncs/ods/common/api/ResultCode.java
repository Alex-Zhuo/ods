package com.lyncs.ods.common.api;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 枚举了一些常用API操作码
 * <p>
 * 数字类错误吗为基本错误码
 * B开头的为业务类错误码，业务模块间的步长为100
 *
 * @author alex
 * @date 2022/01/25
 */
@Getter
@AllArgsConstructor
public enum ResultCode implements IErrorCode {
    /**
     * success
     */
    SUCCESS("200", "操作成功"),

    /**
     * system error
     */
    FAILED("500", "操作失败"),

    /**
     * bed request
     */
    VALIDATE_FAILED("400", "参数检验失败"),

    /**
     * unauthorized
     */
    UNAUTHORIZED("401", "暂未登录或token已经过期"),

    /**
     * forbidden
     */
    FORBIDDEN("403", "没有相关权限"),

    /**
     * user:account disable
     */
    ACCOUNT_DISABLE("B1001", "帐户已被禁用，请联系管理员"),

    /**
     * user:phone number is not registered
     */
    PHONE_NOT_REGISTERED("B1002", "帐户已被禁用，请联系管理员"),


    ;


    private String code;
    private String message;
}
