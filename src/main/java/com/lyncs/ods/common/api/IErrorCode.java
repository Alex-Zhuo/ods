package com.lyncs.ods.common.api;

/**
 * 封装API的错误码
 *
 * @author alex
 * @date 2022/01/25
 */
public interface IErrorCode {

    /**
     * get error code
     *
     * @return error code
     */
    String getCode();

    /**
     * get error message
     *
     * @return error message
     */
    String getMessage();
}
