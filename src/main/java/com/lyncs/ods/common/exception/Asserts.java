package com.lyncs.ods.common.exception;


import com.lyncs.ods.common.api.IErrorCode;

/**
 * 断言处理类，用于抛出各种API异常
 *
 * @author alex
 * @date 2022/01/25
 */
public class Asserts {
    public static void fail(String message) {
        throw new ApiException(message);
    }

    public static void fail(IErrorCode errorCode) {
        throw new ApiException(errorCode);
    }
}
