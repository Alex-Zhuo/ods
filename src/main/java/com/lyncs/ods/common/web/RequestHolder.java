package com.lyncs.ods.common.web;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.IdUtil;
import com.alibaba.ttl.TransmittableThreadLocal;
import com.lyncs.ods.constant.LyncsOdsConstant;
import com.lyncs.ods.security.util.JwtTokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.*;

/**
 * @Author: alex
 * @Description:
 * @Date: Create in 2020/1/15 09:51
 */
@Component
@Slf4j
public class RequestHolder {
    private static final TransmittableThreadLocal<Map<String, Object>> THREAD_LOCAL = new TransmittableThreadLocal<>();

    private static JwtTokenUtil jwtTokenUtil;
    private static HttpServletRequest request;

    @Autowired
    private void autowiredService(JwtTokenUtil autowiredJwtTokenUtil,
                                  HttpServletRequest autowiredRequest) {
        jwtTokenUtil = autowiredJwtTokenUtil;
        request = autowiredRequest;
    }

    public static LocalDateTime getRequestDateTime() {
        return getData(LyncsOdsConstant.Header.REQUEST_TIME);
    }

    public static Long getUserId() {
        return jwtTokenUtil.getUserIdFromToken(getToken());
    }

    public static String getOpenId() {
        return jwtTokenUtil.getOpenIdFromToken(getToken());
    }

    public static String getAuthType() {
        return jwtTokenUtil.getAuthTypeFromToken(getToken());
    }

    public static String getCookie() {
        return getData(LyncsOdsConstant.Header.COOKIE);
    }

    public static void removeCookie() {
        setData(LyncsOdsConstant.Header.COOKIE, null);
    }

    public static String getToken() {
        return getData(LyncsOdsConstant.Header.COOKIE_PT);
    }

    public static String getPlatform() {
        return getData(LyncsOdsConstant.Header.AUTH_PLATFORM);
    }

    public static Long getCompanyId() {
        return getData(LyncsOdsConstant.Header.COOKIE_PC);
    }

    private static Map<String, Object> init() {
        Map<String, Object> map = new HashMap<>(16);
        map.put(LyncsOdsConstant.Header.REQUEST_TIME, LocalDateTime.now());
        map.put(LyncsOdsConstant.Header.REQUEST_ID, IdUtil.nanoId(8).toLowerCase());
        String cookie = request.getHeader(LyncsOdsConstant.Header.COOKIE);
        map.put(LyncsOdsConstant.Header.COOKIE, cookie);
        if (StringUtils.isNotEmpty(cookie)) {
            map.putAll(Arrays.stream(cookie.split(LyncsOdsConstant.Common.SEMICOLON)).filter(StringUtils::isNotBlank).map(item -> Map.of(item.split(LyncsOdsConstant.Common.COLON)[0], item.split(LyncsOdsConstant.Common.COLON)[1])).collect(HashMap::new, HashMap::putAll, HashMap::putAll));
        }
        THREAD_LOCAL.set(map);
        return map;
    }

    public static void init(Map<String, Object> map) {
        if (map != null) {
            THREAD_LOCAL.set(map);
        }
    }

    public static void remove() {
        THREAD_LOCAL.remove();
    }

    public static Map<String, Object> map() {
        Map<String, Object> map = THREAD_LOCAL.get();
        if (CollectionUtil.isEmpty(map)) {
            map = init();
        }
        return map;
    }

    public static <T> void setData(String key, T t) {
        map().put(key, t);
    }

    public static <T> T getData(String key) {
        Object obj = map().get(key);
        return obj == null ? null : (T) obj;
    }

    public static void setHeader(Map<String, List<String>> header) {
        setData("header", header);
    }

    public static void addCookie(String key, String val) {
        String cookie = getCookie();
        setData(LyncsOdsConstant.Header.COOKIE, StringUtils.join(cookie, StringUtils.isEmpty(cookie) ? "" : LyncsOdsConstant.Common.SEMICOLON, key, LyncsOdsConstant.Common.COLON, val));
    }

    public static void saveCookie(String key, String val) {
        String oldVal = getData(key);
        String cookie = getCookie();
        if (StringUtils.isEmpty(oldVal)) {
            addCookie(key, val);
        } else {
            String newCookie = StringUtils.replace(cookie, oldVal, val);
            setData(LyncsOdsConstant.Header.COOKIE, newCookie);
        }
    }

    public static Map<String, List<String>> getHeader() {
        Map<String, List<String>> header = getData("header");
        if (CollectionUtil.isEmpty(header)) {
            return Map.of();
        }
        return header;
    }

    /**
     * 根据key取值，大部分场景只取第一个值
     *
     * @param key
     * @return
     */
    public static String header(String key) {
        return header(key, StringUtils.EMPTY);
    }

    public static String header(String key, String defaultVal) {
        List<String> list = headers(key, List.of(defaultVal));
        return CollectionUtil.isNotEmpty(list) ? list.get(0) : defaultVal;
    }

    public static List<String> headers(String key, List<String> defaultVal) {
        Map<String, List<String>> header = getHeader();
        key = key.toLowerCase();
        return header.getOrDefault(key, defaultVal);
    }

}
