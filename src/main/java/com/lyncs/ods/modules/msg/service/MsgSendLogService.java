package com.lyncs.ods.modules.msg.service;

import com.lyncs.ods.modules.msg.model.MsgSendLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 发送消息记录表 服务类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface MsgSendLogService extends IService<MsgSendLog> {

}
