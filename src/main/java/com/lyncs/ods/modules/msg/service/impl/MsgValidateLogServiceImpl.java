package com.lyncs.ods.modules.msg.service.impl;

import com.lyncs.ods.modules.msg.model.MsgValidateLog;
import com.lyncs.ods.modules.msg.mapper.MsgValidateLogMapper;
import com.lyncs.ods.modules.msg.service.MsgValidateLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 消息验证流水表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
@Service
public class MsgValidateLogServiceImpl extends ServiceImpl<MsgValidateLogMapper, MsgValidateLog> implements MsgValidateLogService {

}
