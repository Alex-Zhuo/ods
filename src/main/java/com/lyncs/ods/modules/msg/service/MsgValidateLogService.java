package com.lyncs.ods.modules.msg.service;

import com.lyncs.ods.modules.msg.model.MsgValidateLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 消息验证流水表 服务类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface MsgValidateLogService extends IService<MsgValidateLog> {

}
