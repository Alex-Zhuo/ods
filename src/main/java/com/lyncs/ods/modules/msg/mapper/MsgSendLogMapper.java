package com.lyncs.ods.modules.msg.mapper;

import com.lyncs.ods.modules.msg.model.MsgSendLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 发送消息记录表 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface MsgSendLogMapper extends BaseMapper<MsgSendLog> {

}
