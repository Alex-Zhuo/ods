package com.lyncs.ods.modules.msg.service.impl;

import com.lyncs.ods.modules.msg.model.MsgSendLog;
import com.lyncs.ods.modules.msg.mapper.MsgSendLogMapper;
import com.lyncs.ods.modules.msg.service.MsgSendLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 发送消息记录表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
@Service
public class MsgSendLogServiceImpl extends ServiceImpl<MsgSendLogMapper, MsgSendLog> implements MsgSendLogService {

}
