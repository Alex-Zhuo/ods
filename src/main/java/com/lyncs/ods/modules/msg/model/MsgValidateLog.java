package com.lyncs.ods.modules.msg.model;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 消息验证流水表
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("msg_validate_log")
@ApiModel(value = "MsgValidateLog对象", description = "消息验证流水表")
public class MsgValidateLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id")
    @TableId(value = "id")
    private Long id;

    @ApiModelProperty("发送渠道:1.短信;2.邮箱;3.服务通知(微信);4.订阅号通知(微信);5.服务号通知(微信)")
    @TableField("type")
    private Integer type;

    @ApiModelProperty("企业id")
    @TableField("company_id")
    private Long companyId;

    @ApiModelProperty("用户id")
    @TableField("user_id")
    private Long userId;

    @ApiModelProperty("验证码")
    @TableField("code")
    private String code;

    @ApiModelProperty("状态：0.未发送;1.发送成功;2.发送失败;3.已超时;4.已验证")
    @TableField("status")
    private Integer status;

    @ApiModelProperty("当前校验次数")
    @TableField("count")
    private Integer count;

    @ApiModelProperty("最大校验次数")
    @TableField("max_count")
    private Integer maxCount;

    @ApiModelProperty("过期时间")
    @TableField("expire_time")
    private LocalDateTime expireTime;

    @ApiModelProperty("创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


}
