package com.lyncs.ods.modules.msg.service;

/**
 * @author alex
 * @date 2022/4/3 23:55
 * @description
 */
public interface MessageService {

    /**
     * 发送邮箱验证码
     *
     * @param email email
     */
    void sendEmailCode(String email);

    /**
     * 发送短信验证码
     *
     * @param phone phone
     */
    void sendSmsCode(String phone);
}
