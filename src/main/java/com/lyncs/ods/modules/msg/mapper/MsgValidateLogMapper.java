package com.lyncs.ods.modules.msg.mapper;

import com.lyncs.ods.modules.msg.model.MsgValidateLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 消息验证流水表 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface MsgValidateLogMapper extends BaseMapper<MsgValidateLog> {

}
