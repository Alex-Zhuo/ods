package com.lyncs.ods.modules.msg.service.impl;

import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.extra.mail.MailUtil;
import com.alibaba.fastjson.JSON;
import com.lyncs.ods.common.exception.ApiException;
import com.lyncs.ods.constant.LyncsOdsConstant;
import com.lyncs.ods.modules.msg.model.MsgSendLog;
import com.lyncs.ods.modules.msg.model.MsgValidateLog;
import com.lyncs.ods.modules.msg.service.MessageService;
import com.lyncs.ods.modules.msg.service.MsgSendLogService;
import com.lyncs.ods.modules.msg.service.MsgValidateLogService;
import com.lyncs.ods.modules.user.model.UserInfo;
import com.lyncs.ods.modules.user.service.UserInfoService;
import com.lyncs.ods.task.CleanTask;
import com.lyncs.ods.utils.AliUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.PropertyPlaceholderHelper;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Timer;
import java.util.function.Consumer;

/**
 * @author alex
 * @date 2022/4/3 23:55
 * @description
 */
@Service
@Slf4j
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MsgValidateLogService msgValidateLogService;
    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private MsgSendLogService msgSendLogService;

    @Value("${ali.sms.code.content}")
    private String smsContent;
    @Value("${ali.sms.code.sign}")
    private String smsSign;
    @Value("${ali.sms.code.templateCode}")
    private String smsTemplateCode;
    @Value("#{T(java.lang.Integer).parseInt('${ali.sms.code.expiration:5}')}")
    private Integer smsExpiration;
    @Value("${email.code.subject}")
    private String emailSubject;
    @Value("${email.code.content}")
    private String emailContent;
    @Value("#{T(java.lang.Integer).parseInt('${email.code.expiration:5}')}")
    private Integer emailExpiration;

    PropertyPlaceholderHelper propertyPlaceholderHelper = new PropertyPlaceholderHelper("{", "}");

    @Override
    public void sendEmailCode(String email) {
        if (!Validator.isEmail(email)) {
            throw new ApiException("invalid email:" + email);
        }
        Long userId = getUserId(null, email);
        if (userId == null) {
            throw new ApiException("user not found by email:" + email);
        }
        String code = RandomUtil.randomNumbers(4);
        Map<String, String> map = Map.of("code", code);
        String content = propertyPlaceholderHelper.replacePlaceholders(emailContent, map::get);
        saveMessageLog(userId, null, content, code, LyncsOdsConstant.MessageType.EMAIL.getKey(), emailExpiration);
        MailUtil.send(email, emailSubject, content, true);
    }

    @Override
    public void sendSmsCode(String phone) {
        if (!Validator.isMobile(phone)) {
            throw new ApiException("invalid phone:" + phone);
        }
        Long userId = getUserId(phone, null);
        if (userId == null) {
            throw new ApiException("user not found by phone:" + phone);
        }
        String code = RandomUtil.randomNumbers(4);
        Map<String, String> param = Map.of("code", code);
        String content = propertyPlaceholderHelper.replacePlaceholders(smsContent, param::get);

        Long validateLogId = saveMessageLog(userId, null, content, code, LyncsOdsConstant.MessageType.SMS.getKey(), smsExpiration);

        Consumer<Long> successFunc = (id) -> updateMessageStatus(id, LyncsOdsConstant.MessageStatus.SUCCESS.getKey());
        Consumer<Long> failedFunc = (id) -> updateMessageStatus(id, LyncsOdsConstant.MessageStatus.SUCCESS.getKey());
        AliUtils.sendSms(phone, smsSign, smsTemplateCode, JSON.toJSONString(param), successFunc, failedFunc, validateLogId);
    }

    private void updateMessageStatus(Long id, Integer status) {
        msgValidateLogService.lambdaUpdate().eq(MsgValidateLog::getId, id)
                .set(MsgValidateLog::getStatus, status)
                .set(LyncsOdsConstant.MessageStatus.VERIFIED.getKey().equals(status), MsgValidateLog::getCount, 1)
                .update();
    }

    private Long saveMessageLog(Long userId, Long companyId, String content, String code, Integer type, Integer expiration) {
        LocalDateTime now = LocalDateTime.now();
        MsgValidateLog msgValidateLog = new MsgValidateLog()
                .setId(IdUtil.getSnowflakeNextId())
                .setType(type)
                .setCompanyId(companyId)
                .setUserId(userId)
                .setCode(code)
                .setStatus(LyncsOdsConstant.MessageStatus.INIT.getKey())
                .setCount(0)
                .setMaxCount(1)
                .setExpireTime(LocalDateTime.now().plusMinutes(expiration))
                .setCreateTime(now)
                .setUpdateTime(now);
        MsgSendLog msgSendLog = new MsgSendLog()
                .setId(IdUtil.getSnowflakeNextId())
                .setType(type)
                .setCompanyId(companyId)
                .setUserId(userId)
                .setContent(content)
                .setStatus(LyncsOdsConstant.MessageStatus.INIT.getKey())
                //.setRemark(bizCode)
                .setCreateTime(now)
                .setUpdateTime(now);
        msgValidateLogService.save(msgValidateLog);
        msgSendLogService.save(msgSendLog);
        new Timer().schedule(new CleanTask(msgValidateLog.getId(), msgSendLog.getId(), msgValidateLogService, msgSendLogService), Long.valueOf(expiration) * 60 * 1000);
        return msgValidateLog.getId();
    }

    private Long getUserId(String phone, String email) {
        if (StringUtils.isAllEmpty(phone, email)) {
            return null;
        }
        UserInfo userInfo = userInfoService.lambdaQuery().eq(StringUtils.isNotEmpty(phone), UserInfo::getPhone, phone)
                .eq(StringUtils.isNotEmpty(email), UserInfo::getEmail, email)
                .eq(UserInfo::getStatus, LyncsOdsConstant.UserStatus.ENABLE.getKey())
                .last("limit 1")
                .one();
        return userInfo == null ? null : userInfo.getId();
    }

}

