package com.lyncs.ods.modules.wx.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.crypto.digest.MD5;
import com.lyncs.ods.common.api.CommonResult;
import com.lyncs.ods.config.WxMpConfiguration;
import com.lyncs.ods.constant.LyncsOdsConstant;
import com.lyncs.ods.modules.user.service.UserService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * @author alex
 * @date 2022/1/28 15:49
 * @description
 */
@Slf4j
@RestController
@RequestMapping("/auth/{appid}")
@Api(tags = {"微信授权相关接口"})
public class WxAuthController {

    @Autowired
    private UserService userService;

    /**
     * 获取微信登陆二维码地址
     *
     * @return
     */
    @ApiOperation(value = "获取微信登录二维码")
    @GetMapping("/getQRCodeUrl")
    @ApiResponses(@ApiResponse(code = 200, message = "返回url"))
    public CommonResult<String> getQRCodeUrl(@PathVariable String appid) {
        WxMpService wxMpService = WxMpConfiguration.getMpServices().get(appid);
        String date = DateUtil.format(new Date(), LyncsOdsConstant.Common.DATE_PATTERN);
        String state = MD5.create().digestHex(StringUtils.join(LyncsOdsConstant.Common.MD5_PREFIX, date));
        String qrConnectUrl = wxMpService.buildQrConnectUrl("https://1afb-116-77-153-220.ngrok.io/auth/wx5fbafb66272c83fa/login", "snsapi_login", state);
        log.info("getQRCodeUrl:{}", qrConnectUrl);
        return CommonResult.success(qrConnectUrl);
    }

    @GetMapping("/login")
    @ApiOperation(value = "微信扫码登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appid", value = "授权appid", dataType = "String", paramType = "path"),
            @ApiImplicitParam(name = "code", value = "授权code", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "state", value = "授权state", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "platform", value = "授权登录平台:weChatWebApp,weChatMiniApp。默认weChatWebApp", dataType = "String", paramType = "query"),
    })
    @ApiResponses(@ApiResponse(code = 200, message = "返回userId"))
    public CommonResult<Long> login(@PathVariable String appid, @RequestParam String code, String state, @RequestHeader(required = false, defaultValue = "weChatWebApp") String platform) throws WxErrorException {
        return CommonResult.success(userService.login(appid, code, state, platform));
    }

}
