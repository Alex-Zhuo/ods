package com.lyncs.ods.modules.company.model;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 企业通讯录
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("company_contact_info")
@ApiModel(value = "CompanyContactInfo对象", description = "企业通讯录")
public class CompanyContactInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("自增主键id")
    @TableId(value = "id")
    private Long id;

    @ApiModelProperty("企业id")
    @TableField("company_id")
    private Long companyId;

    @ApiModelProperty("被关联的B类企业初始id")
    @TableField("init_company_id")
    private Long initCompanyId;

    @ApiModelProperty("被关联的企业id")
    @TableField("relation_id")
    private Long relationId;

    @ApiModelProperty("备注名称")
    @TableField("nickname")
    private String nickname;

    @ApiModelProperty("状态:1.已绑定;2.已解绑")
    @TableField("status")
    private Integer status;

    @ApiModelProperty("关系类型:1.供应商;2.客户")
    @TableField("type")
    private Integer type;

    @ApiModelProperty("创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


}
