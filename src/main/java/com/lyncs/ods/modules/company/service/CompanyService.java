package com.lyncs.ods.modules.company.service;

import com.lyncs.ods.req.CompanyRegisterReq;
import com.lyncs.ods.req.SavePartnerCompanyReq;
import com.lyncs.ods.req.SavePaymentReq;
import com.lyncs.ods.resp.*;

import java.util.List;

/**
 * @author alex
 * @date 2022/4/04 14:05
 * @description
 */
public interface CompanyService {
    /**
     * 用户注册公司并关联
     *
     * @param req req
     */
    Long registerCompany(CompanyRegisterReq req);

    /**
     * create one company for Partner
     *
     * @param req request info
     */
    void savePartnerCompany(SavePartnerCompanyReq req);

    /**
     * list all partner companies (all available)
     *
     * @param type Identity type: 1.supplier 2.Partner
     * @return companies short info
     */
    List<CompanyShortInfoResp> listPartnerCompanies(Integer type);

    /**
     * get staff info by company id
     *
     * @param companyId companyId
     * @return staffs
     */
    List<StaffInfoResp> listCompanyStaffs(Long companyId);

    /**
     * 获取收款信息
     *
     * @param companyId companyId
     * @return 收款信息
     */
    List<CompanyPaymentInfoResp> listPaymentInfo(Long companyId);

    /**
     * 保存款项配置
     *
     * @param req request info
     */
    void savePaymentInfo(SavePaymentReq req);

    /**
     * 获取企业配置款项信息
     *
     * @param companyId companyId
     * @return 款项信息
     */
    List<String> listCompanyFees(Long companyId);

    /**
     * 保存企业配置款项信息
     *
     * @param feeNames 配置款项信息
     */
    void saveCompanyFees(List<String> feeNames);

    /**
     * 启用/禁用 收款信息
     *
     * @param id     id
     * @param status status
     */
    void updatePaymentStatus(Long id, Integer status);

    /**
     * get company detail info by id
     *
     * @param companyId companyId
     * @return detail info
     */
    CompanyDetailInfoResp getCompanyDetailInfo(Long companyId);

    /**
     * 获取与公司关联的spu信息
     *
     * @param companyId companyId
     * @return 与公司关联的spu信息
     */
    CompanySpuInfoResp listCompanySpu(String companyId);

    /**
     * 关联/解绑 spu
     *
     * @param companyId companyId
     * @param spuId     spuId
     * @param type      操作类型：1.关联;2.解绑
     */
    void saveCompanySpu(Long companyId, Long spuId, Integer type);
}
