package com.lyncs.ods.modules.company.service.impl;

import com.lyncs.ods.modules.company.model.CompanyPaymentInfo;
import com.lyncs.ods.modules.company.mapper.CompanyPaymentInfoMapper;
import com.lyncs.ods.modules.company.service.CompanyPaymentInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 企业收款信息 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-03-29
 */
@Service
public class CompanyPaymentInfoServiceImpl extends ServiceImpl<CompanyPaymentInfoMapper, CompanyPaymentInfo> implements CompanyPaymentInfoService {

}
