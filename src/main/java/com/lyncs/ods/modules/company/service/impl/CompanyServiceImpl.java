package com.lyncs.ods.modules.company.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.IdUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lyncs.ods.common.exception.ApiException;
import com.lyncs.ods.common.web.RequestHolder;
import com.lyncs.ods.constant.LyncsOdsConstant;
import com.lyncs.ods.modules.company.model.CompanyContactInfo;
import com.lyncs.ods.modules.company.model.CompanyFeeInfo;
import com.lyncs.ods.modules.company.model.CompanyInfo;
import com.lyncs.ods.modules.company.model.CompanyPaymentInfo;
import com.lyncs.ods.modules.company.service.*;
import com.lyncs.ods.modules.setting.model.PaymentSetting;
import com.lyncs.ods.modules.setting.service.PaymentSettingService;
import com.lyncs.ods.modules.user.model.CompanyUserRelation;
import com.lyncs.ods.modules.user.model.CompanyUserRelationLog;
import com.lyncs.ods.modules.user.model.UserInfo;
import com.lyncs.ods.modules.user.service.CompanyUserRelationLogService;
import com.lyncs.ods.modules.user.service.CompanyUserRelationService;
import com.lyncs.ods.modules.user.service.UserInfoService;
import com.lyncs.ods.req.CompanyRegisterReq;
import com.lyncs.ods.req.SavePartnerCompanyReq;
import com.lyncs.ods.req.SavePaymentReq;
import com.lyncs.ods.resp.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author alex
 * @date 2022/4/10 14:05
 * @description
 */
@Slf4j
@Service
public class CompanyServiceImpl implements CompanyService {
    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private CompanyInfoService companyInfoService;
    @Autowired
    private CompanyUserRelationService companyUserRelationService;
    @Autowired
    private CompanyUserRelationLogService relationRecordService;
    @Autowired
    private CompanyContactInfoService companyContactInfoService;
    @Autowired
    private CompanyFeeInfoService companyFeeInfoService;
    @Autowired
    private CompanyPaymentInfoService companyPaymentInfoService;
    @Autowired
    private PaymentSettingService paymentSettingService;


    /**
     * 用户注册公司并关联
     *
     * @param req req
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long registerCompany(CompanyRegisterReq req) {
        if (!Validator.isUrl(req.getAvatarUrl())) {
            throw new ApiException("invalid avatar-url");
        }
        CompanyInfo companyInfo = new CompanyInfo();
        BeanUtils.copyProperties(req, companyInfo);
        if (LyncsOdsConstant.VIPStatus.FREE.getKey().equals(companyInfo.getLevelStatus())) {
            companyInfo.setLevelExpireTime(LocalDateTime.now().plusDays(7));
        }
        companyInfo.setId(IdUtil.getSnowflakeNextId()).setCreator(RequestHolder.getUserId()).setCreateTime(LocalDateTime.now()).setUpdateTime(LocalDateTime.now());
        companyInfoService.save(companyInfo);
        companyUserRelation(RequestHolder.getUserId(), companyInfo.getId(), LyncsOdsConstant.CompanyRole.ADMIN.getKey());
        userInfoService.lambdaUpdate().eq(UserInfo::getId, RequestHolder.getUserId())
                .set(UserInfo::getRecentLoginCompanyId, companyInfo.getId())
                .update();
        //插入默认的款项信息
        List<CompanyFeeInfo> commonFeeInfos = getCommonFeeInfos();
        if (CollectionUtil.isNotEmpty(commonFeeInfos)) {
            List<CompanyFeeInfo> companyFeeInfos = commonFeeInfos.stream().map(fee -> {
                CompanyFeeInfo feeInfo = new CompanyFeeInfo();
                BeanUtils.copyProperties(fee, feeInfo);
                feeInfo.setCompanyId(companyInfo.getId())
                        .setId(IdUtil.getSnowflakeNextId())
                        .setCreateTime(LocalDateTime.now())
                        .setUpdateTime(LocalDateTime.now())
                        .setStatus(1);
                return feeInfo;
            }).collect(Collectors.toList());
            companyFeeInfoService.saveBatch(companyFeeInfos);
        }
        RequestHolder.saveCookie(LyncsOdsConstant.Header.COOKIE_PC, companyInfo.getId().toString());
        return companyInfo.getId();
    }

    /**
     * create one company for Partner
     *
     * @param req request info
     */
    @Override
    public void savePartnerCompany(SavePartnerCompanyReq req) {
        Long companyId = RequestHolder.getCompanyId();
        CompanyInfo virtualCompany;
        List<CompanyContactInfo> companyContactInfos = new ArrayList<>();
        if (req.getCompanyId() != null) {
            companyContactInfos = companyContactInfoService.lambdaQuery()
                    .eq(CompanyContactInfo::getInitCompanyId, req.getCompanyId())
                    .eq(CompanyContactInfo::getCompanyId, RequestHolder.getCompanyId())
                    .eq(CompanyContactInfo::getStatus, LyncsOdsConstant.BindStatus.BIND.getKey())
                    .list();
            if (CollectionUtil.isEmpty(companyContactInfos)) {
                throw new ApiException("not your partner company");
            }
            virtualCompany = companyInfoService.lambdaQuery()
                    .eq(CompanyInfo::getId, req.getCompanyId())
                    .eq(CompanyInfo::getStatus, LyncsOdsConstant.CompanyStatus.VIRTUAL.getKey())
                    .eq(CompanyInfo::getCreator, RequestHolder.getUserId())
                    .one();
            if (Objects.isNull(virtualCompany)) {
                throw new ApiException("partner company does not exist or can not be edited");
            }
        } else {
            virtualCompany = new CompanyInfo()
                    .setId(IdUtil.getSnowflakeNextId())
                    .setStatus(LyncsOdsConstant.CompanyStatus.VIRTUAL.getKey())
                    .setCreator(RequestHolder.getUserId())
                    .setCreateTime(LocalDateTime.now());
        }
        virtualCompany.setTelephone(req.getTelephone())
                .setFax(req.getFax())
                .setEmail(req.getEmail())
                .setAddress(req.getAddress())
                .setUpdateTime(LocalDateTime.now())
                .setName(req.getNickname())
                .setShortName(req.getNickname());
        companyInfoService.saveOrUpdate(virtualCompany);
        List<CompanyContactInfo> allContactInfos = getAllContactInfos(req, companyContactInfos, virtualCompany);
        companyContactInfoService.saveOrUpdateBatch(allContactInfos);
    }

    /**
     * list all partner companies (all available)
     *
     * @param type Identity type: 1.supplier 2.partner
     * @return companies short info
     */
    @Override
    public List<CompanyShortInfoResp> listPartnerCompanies(Integer type) {
        List<Integer> allType = Arrays.stream(LyncsOdsConstant.ContactType.values()).map(LyncsOdsConstant.ContactType::getKey).collect(Collectors.toList());
        allType.remove(type);
        Integer oppositeType = allType.get(0);
        //正向查询，如查"客户"，则查询此企业建立的客户伙伴
        List<CompanyContactInfo> companyContactInfos = companyContactInfoService.lambdaQuery().eq(CompanyContactInfo::getCompanyId, RequestHolder.getCompanyId()).eq(CompanyContactInfo::getType, type)
                //逆向查询，如查"客户"，则查询伙伴企业建立的供应商关系
                .eq(CompanyContactInfo::getRelationId, RequestHolder.getCompanyId()).eq(CompanyContactInfo::getType, oppositeType)
                .eq(CompanyContactInfo::getStatus, LyncsOdsConstant.BindStatus.BIND.getKey())
                .list();
        if (CollectionUtil.isEmpty(companyContactInfos)) {
            return null;
        }
        List<Long> allCompanyIds = companyContactInfos.stream().map(info -> type.equals(info.getType()) ? Objects.nonNull(info.getInitCompanyId()) ? info.getInitCompanyId() : info.getRelationId() : info.getCompanyId()).collect(Collectors.toList());
        List<CompanyInfo> companyInfos = companyInfoService.lambdaQuery().in(CompanyInfo::getId, allCompanyIds).eq(CompanyInfo::getStatus, LyncsOdsConstant.CompanyStatus.ENABLE.getKey()).list();
        Map<Long, CompanyInfo> companyInfoMap = companyInfos.stream().collect(Collectors.toMap(CompanyInfo::getId, o -> o, (o1, o2) -> o2));
        return companyContactInfos.stream().map(info -> {
            Long companyId = type.equals(info.getType()) ? Objects.nonNull(info.getInitCompanyId()) ? info.getInitCompanyId() : info.getRelationId() : info.getCompanyId();
            CompanyInfo companyInfo = companyInfoMap.get(companyId);
            if (Objects.isNull(companyInfo)) {
                return null;
            }
            return new CompanyShortInfoResp()
                    .setAvatarUrl(companyInfo.getAvatarUrl())
                    .setCompanyId(companyInfo.getId())
                    .setNickname(StringUtils.isEmpty(info.getNickname()) ? companyInfo.getShortName() : info.getNickname())
                    .setAddress(companyInfo.getAddress());
        }).filter(Objects::nonNull).collect(Collectors.toList());
    }

    /**
     * get staff info by company id
     *
     * @param companyId companyId
     * @return staffs
     */
    @Override
    public List<StaffInfoResp> listCompanyStaffs(Long companyId) {
        List<CompanyUserRelation> companyUserRelations = companyUserRelationService.lambdaQuery().eq(CompanyUserRelation::getCompanyId, companyId).eq(CompanyUserRelation::getStatus, LyncsOdsConstant.BindStatus.BIND.getKey()).list();
        if (CollectionUtil.isEmpty(companyUserRelations)) {
            return null;
        }
        Map<Long, CompanyUserRelation> userRelationMap = companyUserRelations.stream().collect(Collectors.toMap(CompanyUserRelation::getUserId, o -> o, (o1, o2) -> o1));
        List<UserInfo> userInfos = userInfoService.lambdaQuery().in(UserInfo::getId, userRelationMap.keySet()).eq(UserInfo::getStatus, LyncsOdsConstant.UserStatus.ENABLE.getKey()).list();
        return userInfos.stream().map(info -> {
            StaffInfoResp resp = new StaffInfoResp();
            CompanyUserRelation companyUserInfo = userRelationMap.get(info.getId());
            BeanUtils.copyProperties(info, resp);
            resp.setName(info.getUsername())
                    .setNickname(companyUserInfo.getNickname())
                    .setDept(companyUserInfo.getDept())
                    .setPost(companyUserInfo.getPost())
                    .setRole(companyUserInfo.getRole())
                    .setRemark(companyUserInfo.getRemark());
            return resp;
        }).collect(Collectors.toList());
    }

    @Override
    public List<CompanyPaymentInfoResp> listPaymentInfo(Long companyId) {
        List<CompanyPaymentInfo> companyPaymentInfos = companyPaymentInfoService.lambdaQuery().eq(CompanyPaymentInfo::getCompanyId, companyId)
                .eq(CompanyPaymentInfo::getStatus, LyncsOdsConstant.EnableStatus.ENABLE.getKey())
                .list();
        List<PaymentSetting> paymentSettings = paymentSettingService.lambdaQuery().eq(PaymentSetting::getStatus, LyncsOdsConstant.EnableStatus.ENABLE.getKey()).list();
        Map<Long, PaymentSetting> paymentSettingMap = paymentSettings.stream().collect(Collectors.toMap(PaymentSetting::getId, o -> o, (o1, o2) -> o2));
        return companyPaymentInfos.stream().map(info -> {
            PaymentSetting paymentSetting = paymentSettingMap.get(info.getPaymentId());
            if (Objects.isNull(paymentSetting)) {
                return null;
            }
            return new CompanyPaymentInfoResp().setName(paymentSetting.getName()).setLogo(paymentSetting.getLogo()).setAttrs(JSONArray.parseArray(info.getPaymentSetting(), JSONObject.class));
        }).filter(Objects::nonNull).collect(Collectors.toList());
    }

    @Override
    public void savePaymentInfo(SavePaymentReq req) {
        Long paymentId = req.getPaymentId();
        List<Map<String, Object>> attrs = req.getAttrs();
        Long id = req.getId();
        CompanyPaymentInfo companyPaymentInfo = new CompanyPaymentInfo();
        if (Objects.isNull(req.getId())) {
            id = IdUtil.getSnowflakeNextId();
            companyPaymentInfo.setCreateTime(RequestHolder.getRequestDateTime());
        }

        companyPaymentInfo.setId(id)
                .setCompanyId(RequestHolder.getCompanyId())
                .setPaymentId(paymentId)
                .setPaymentSetting(JSON.toJSONString(attrs))
                .setStatus(LyncsOdsConstant.EnableStatus.ENABLE.getKey())
                .setUpdateTime(LocalDateTime.now());

        companyPaymentInfoService.saveOrUpdate(companyPaymentInfo);
    }

    @Override
    public List<String> listCompanyFees(Long companyId) {
        List<CompanyFeeInfo> companyFeeInfos = companyFeeInfoService.lambdaQuery().eq(CompanyFeeInfo::getCompanyId, companyId).eq(CompanyFeeInfo::getStatus, LyncsOdsConstant.EnableStatus.ENABLE.getKey()).list();
        return companyFeeInfos.stream().map(CompanyFeeInfo::getFeeName).collect(Collectors.toList());
    }

    @Override
    public void saveCompanyFees(List<String> feeNames) {
        List<CompanyFeeInfo> companyFeeInfos = feeNames.stream().map(name -> new CompanyFeeInfo()
                .setId(IdUtil.getSnowflakeNextId())
                .setCompanyId(RequestHolder.getCompanyId())
                .setFeeName(name)
                .setUpdateTime(LocalDateTime.now())
                .setCreateTime(LocalDateTime.now())
                .setStatus(LyncsOdsConstant.EnableStatus.ENABLE.getKey())).collect(Collectors.toList());
        companyFeeInfoService.lambdaUpdate().eq(CompanyFeeInfo::getCompanyId, RequestHolder.getCompanyId()).remove();
        companyFeeInfoService.saveBatch(companyFeeInfos);
    }

    @Override
    public void updatePaymentStatus(Long id, Integer status) {
        CompanyPaymentInfo companyPaymentInfo = companyPaymentInfoService.lambdaQuery().eq(CompanyPaymentInfo::getId, id).one();
        if (Objects.isNull(companyPaymentInfo) || companyPaymentInfo.getStatus().equals(status)) {
            throw new ApiException("payment info does not exist or already in the current status");
        }
        companyPaymentInfoService.lambdaUpdate().eq(CompanyPaymentInfo::getId, id).set(CompanyPaymentInfo::getStatus, status).update();
    }

    /**
     * get company detail info by id
     *
     * @param companyId companyId
     * @return detail info
     */
    @Override
    public CompanyDetailInfoResp getCompanyDetailInfo(Long companyId) {
        if (Objects.isNull(companyId)) {
            throw new ApiException("companyId & partnerId cannot be null at the same time");
        }
        CompanyInfo companyInfo = companyInfoService.lambdaQuery().eq(CompanyInfo::getId, companyId).eq(CompanyInfo::getStatus, LyncsOdsConstant.CompanyStatus.ENABLE.getKey()).one();
        if (Objects.isNull(companyInfo)) {
            throw new ApiException("company not exist or disabled");
        }
        return new CompanyDetailInfoResp(companyInfo);
    }

    @Override
    public CompanySpuInfoResp listCompanySpu(String companyId) {
        return null;
    }

    @Override
    public void saveCompanySpu(Long companyId, Long spuId, Integer type) {

    }

    private List<CompanyContactInfo> getAllContactInfos(SavePartnerCompanyReq req, List<CompanyContactInfo> exitContactInfos, CompanyInfo virtualCompany) {
        List<Integer> rawType = Arrays.stream(LyncsOdsConstant.ContactType.values()).map(LyncsOdsConstant.ContactType::getKey).collect(Collectors.toList());
        List<CompanyContactInfo> contactInfos = new ArrayList<>();
        Map<Integer, CompanyContactInfo> relationTypeMap = exitContactInfos.stream().collect(Collectors.toMap(CompanyContactInfo::getType, o -> o, (o1, o2) -> o2));
        List<Integer> relationType = Arrays.stream(req.getType().split(LyncsOdsConstant.Common.COMMA)).map(Integer::valueOf).distinct().collect(Collectors.toList());
        relationType.forEach(type -> {
            if (!rawType.contains(type)) {
                throw new ApiException("invalid relation type:" + type);
            }
            CompanyContactInfo companyContactInfo = relationTypeMap.get(type);
            if (Objects.isNull(companyContactInfo)) {
                companyContactInfo = new CompanyContactInfo()
                        .setCompanyId(RequestHolder.getCompanyId())
                        .setInitCompanyId(virtualCompany.getId())
                        .setRelationId(virtualCompany.getId())
                        .setStatus(LyncsOdsConstant.BindStatus.BIND.getKey())
                        .setCreateTime(LocalDateTime.now())
                        .setType(type);
            }
            companyContactInfo.setNickname(req.getNickname());
            contactInfos.add(companyContactInfo);
        });
        return contactInfos;
    }

    private List<CompanyFeeInfo> getCommonFeeInfos() {
        return companyFeeInfoService.lambdaQuery().eq(CompanyFeeInfo::getCompanyId, 0).eq(CompanyFeeInfo::getStatus, 1).list();
    }

    private void companyUserRelation(Long userId, Long cid, Integer role) {
        UserInfo userInfo = userInfoService.lambdaQuery().eq(UserInfo::getId, userId).one();
        companyUserRelationService.save(new CompanyUserRelation().setCompanyId(cid).setUserId(userId).setRole(role).setNickname(userInfo.getUsername()));
        relationRecordService.save(new CompanyUserRelationLog().setId(IdUtil.getSnowflakeNextId()).setUserId(userId).setCompanyId(cid).setStatus(LyncsOdsConstant.BindStatus.BIND.getKey()));
    }
}
