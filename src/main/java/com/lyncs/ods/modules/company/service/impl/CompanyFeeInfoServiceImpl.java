package com.lyncs.ods.modules.company.service.impl;

import com.lyncs.ods.modules.company.model.CompanyFeeInfo;
import com.lyncs.ods.modules.company.mapper.CompanyFeeInfoMapper;
import com.lyncs.ods.modules.company.service.CompanyFeeInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 企业款项配置表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-03-29
 */
@Service
public class CompanyFeeInfoServiceImpl extends ServiceImpl<CompanyFeeInfoMapper, CompanyFeeInfo> implements CompanyFeeInfoService {

}
