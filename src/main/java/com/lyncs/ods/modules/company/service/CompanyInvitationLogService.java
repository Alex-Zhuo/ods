package com.lyncs.ods.modules.company.service;

import com.lyncs.ods.modules.company.model.CompanyInvitationLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 公司邀请记录表 服务类
 * </p>
 *
 * @author alex
 * @since 2022-03-29
 */
public interface CompanyInvitationLogService extends IService<CompanyInvitationLog> {

}
