package com.lyncs.ods.modules.company.controller;


import com.lyncs.ods.common.api.CommonResult;
import com.lyncs.ods.modules.company.service.CompanyService;
import com.lyncs.ods.req.CompanyRegisterReq;
import com.lyncs.ods.req.SavePartnerCompanyReq;
import com.lyncs.ods.req.SavePaymentReq;
import com.lyncs.ods.resp.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 公司账号信息表 前端控制器
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
@RestController
@RequestMapping("/company")
@Api(tags = {"企业相关接口"})
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    /**
     * 注册公司并关联到当前用户
     *
     * @param req company info
     * @return T/F
     */
    @PostMapping("/register")
    @ApiOperation(value = "用户创建公司")
    public CommonResult<Long> registerCompany(@Valid @RequestBody CompanyRegisterReq req) {
        return CommonResult.success(companyService.registerCompany(req));
    }

    /**
     * create one company for partner
     *
     * @return T/F
     */
    @PostMapping("partner/save")
    @ApiOperation(value = "新增一个合作企业 用于发货/交付/结算等")
    public CommonResult<?> savePartnerCompany(SavePartnerCompanyReq req) {
        companyService.savePartnerCompany(req);
        return CommonResult.success();
    }

    /**
     * list all partner company()
     *
     * @return partner companies
     */
    @GetMapping("partner/list")
    @ApiOperation(value = "通讯录：获取合作伙伴公司")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "type", value = "请求类型：1.请求合作供应商公司;2.请求合作企业公司", dataType = "Integer", defaultValue = "1")
    )
    public CommonResult<List<CompanyShortInfoResp>> listPartnerCompanies(@RequestParam(defaultValue = "1") Integer type) {
        return CommonResult.success(companyService.listPartnerCompanies(type));
    }

    /**
     * get company staffs by company id
     *
     * @return company staff info
     */
    @GetMapping("{companyId}/staffs")
    @ApiOperation(value = "获取公司员工信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "company_id", value = "公司ID", dataType = "Long", paramType = "path"),
    })
    public CommonResult<List<StaffInfoResp>> listCompanyStaffs(@PathVariable Long companyId) {
        //获取公司员工信息
        return CommonResult.success(companyService.listCompanyStaffs(companyId));
    }

    @GetMapping("{companyId}/payments")
    @ApiOperation(value = "获取公司配置收款信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "company_id", value = "公司ID", dataType = "Long", paramType = "path"),
    })
    public CommonResult<List<CompanyPaymentInfoResp>> listPaymentInfo(@PathVariable Long companyId) {
        return CommonResult.success(companyService.listPaymentInfo(companyId));
    }

    @GetMapping("/payment/save")
    @ApiOperation(value = "保存公司配置收款信息")
    public CommonResult<?> savePaymentInfo(@RequestBody SavePaymentReq req) {
        companyService.savePaymentInfo(req);
        return CommonResult.success();
    }

    @GetMapping("/payment/status")
    @ApiOperation(value = "收款方式启用/禁用")
    public CommonResult<?> updatePaymentStatus(@RequestParam("id") Long id, @RequestParam("status") Integer status) {
        companyService.updatePaymentStatus(id, status);
        return CommonResult.success();
    }

    @GetMapping("{companyId}/fees")
    @ApiOperation(value = "获取结算款项配置信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "company_id", value = "公司ID", dataType = "Long", paramType = "path"),
    })
    public CommonResult<List<String>> listCompanyFees(@PathVariable Long companyId) {
        //获取公司员工信息
        return CommonResult.success(companyService.listCompanyFees(companyId));
    }

    @GetMapping("/setting/save")
    @ApiOperation(value = "保存结算款项配置信息，传所有款项")
    public CommonResult<?> saveCompanyFees(@RequestParam("fee_names") List<String> feeNames) {
        companyService.saveCompanyFees(feeNames);
        return CommonResult.success();
    }

    /**
     * get one company detail info by id
     *
     * @return detail info
     */
    @GetMapping("{companyId}/detail")
    @ApiOperation(value = "获取公司详细信息。companyId：获取公司详情；partnerId：获取合作企业详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "company_id", value = "公司ID", dataType = "Long", paramType = "path"),
    })
    public CommonResult<CompanyDetailInfoResp> getCompanyDetail(@PathVariable Long companyId) {
        return CommonResult.success(companyService.getCompanyDetailInfo(companyId));
    }

    @GetMapping("{companyId}/spu/list")
    @ApiOperation(value = "获取公司关联spu信息。")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "company_id", value = "公司ID", dataType = "Long", paramType = "path"),
    })
    public CommonResult<CompanySpuInfoResp> listCompanySpu(@PathVariable String companyId) {
        return CommonResult.success(companyService.listCompanySpu(companyId));
    }

    @GetMapping("{companyId}/spu/relation")
    @ApiOperation(value = "公司关联spu")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "company_id", value = "公司ID", dataType = "Long", paramType = "path"),
            @ApiImplicitParam(name = "spu_id", value = "spu ID", dataType = "Long"),
            @ApiImplicitParam(name = "type", value = "操作类型：1.关联;2.解绑", dataType = "Integer"),
    })
    public CommonResult<?> saveCompanySpu(@PathVariable Long companyId, Long spuId, Integer type) {
        companyService.saveCompanySpu(companyId, spuId, type);
        return CommonResult.success();
    }
}

