package com.lyncs.ods.modules.company.model;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 企业收款信息
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("company_payment_info")
@ApiModel(value = "CompanyPaymentInfo对象", description = "企业收款信息")
public class CompanyPaymentInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("自增id")
    @TableId(value = "id")
    private Long id;

    @ApiModelProperty("企业ID")
    @TableField("company_id")
    private Long companyId;

    @ApiModelProperty("付款方式ID")
    @TableField("payment_id")
    private Long paymentId;

    @ApiModelProperty("付款配置")
    @TableField("payment_setting")
    private String paymentSetting;

    @ApiModelProperty("状态：1.启用；2.禁用")
    @TableField("status")
    private Integer status;

    @ApiModelProperty("创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


}
