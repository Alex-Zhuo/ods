package com.lyncs.ods.modules.company.mapper;

import com.lyncs.ods.modules.company.model.CompanyPaymentInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 企业收款信息 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2022-03-29
 */
@Mapper
public interface CompanyPaymentInfoMapper extends BaseMapper<CompanyPaymentInfo> {

}
