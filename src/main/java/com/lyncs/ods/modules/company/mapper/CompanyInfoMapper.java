package com.lyncs.ods.modules.company.mapper;

import com.lyncs.ods.modules.company.model.CompanyInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 公司账号信息表 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2022-03-29
 */
@Mapper
public interface CompanyInfoMapper extends BaseMapper<CompanyInfo> {

}
