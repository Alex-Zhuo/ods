package com.lyncs.ods.modules.company.model;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 企业邀请记录表
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("company_invitation_log")
@ApiModel(value = "CompanyInvitationLog对象", description = "企业邀请记录表")
public class CompanyInvitationLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("自增主键id")
    @TableId(value = "id")
    private Long id;

    @ApiModelProperty("邀请企业id")
    @TableField("company_id")
    private Long companyId;

    @ApiModelProperty("受邀企业id")
    @TableField("invited_company_id")
    private Long invitedCompanyId;

    @ApiModelProperty("类型：1.分享订单；2.分享交付单；3.分享结算单；4.邀请企业加入；5.邀请员工加入")
    @TableField("type")
    private Boolean type;

    @ApiModelProperty("短链key")
    @TableField("key")
    private String key;

    @ApiModelProperty("短链对应的数据")
    @TableField("data")
    private String data;

    @ApiModelProperty("发送邀请记录id(msg_send_log)")
    @TableField("send_log_id")
    private Long sendLogId;

    @ApiModelProperty("邀请过期时间")
    @TableField("expire_time")
    private LocalDateTime expireTime;

    @ApiModelProperty("状态：0.待发送 1.已发送；2.已接受")
    @TableField("status")
    private Boolean status;

    @ApiModelProperty("备注")
    @TableField("remark")
    private String remark;

    @ApiModelProperty("发起用户id")
    @TableField("creator")
    private Long creator;

    @ApiModelProperty("创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


}
