package com.lyncs.ods.modules.company.mapper;

import com.lyncs.ods.modules.company.model.CompanyFeeInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 企业款项配置表 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2022-03-29
 */
@Mapper
public interface CompanyFeeInfoMapper extends BaseMapper<CompanyFeeInfo> {

}
