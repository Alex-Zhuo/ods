package com.lyncs.ods.modules.company.service.impl;

import com.lyncs.ods.modules.company.model.CompanyInvitationLog;
import com.lyncs.ods.modules.company.mapper.CompanyInvitationLogMapper;
import com.lyncs.ods.modules.company.service.CompanyInvitationLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 公司邀请记录表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-03-29
 */
@Service
public class CompanyInvitationLogServiceImpl extends ServiceImpl<CompanyInvitationLogMapper, CompanyInvitationLog> implements CompanyInvitationLogService {

}
