package com.lyncs.ods.modules.company.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.lyncs.ods.constant.LyncsOdsConstant;
import com.lyncs.ods.modules.company.model.CompanyContactInfo;
import com.lyncs.ods.modules.company.mapper.CompanyContactInfoMapper;
import com.lyncs.ods.modules.company.service.CompanyContactInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 * 企业通讯录 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-03-29
 */
@Service
public class CompanyContactInfoServiceImpl extends ServiceImpl<CompanyContactInfoMapper, CompanyContactInfo> implements CompanyContactInfoService {

    @Override
    public List<CompanyContactInfo> getConcatInfoByCompanyIds(List<Long> companyIds, Integer type) {
        if (CollectionUtil.isEmpty(companyIds) || Objects.isNull(type)) {
            return List.of();
        }
        LambdaQueryChainWrapper<CompanyContactInfo> queryChainWrapper = this.lambdaQuery().eq(CompanyContactInfo::getStatus, LyncsOdsConstant.BindStatus.BIND.getKey());
        //理论上不会有or这种反向查询好友关系的场景
        if (LyncsOdsConstant.ContactType.SUPPLIER.getKey().equals(type)) {
            //查客户好友关系
            queryChainWrapper.and(wrapper -> wrapper.in(CompanyContactInfo::getCompanyId, companyIds).eq(CompanyContactInfo::getType, LyncsOdsConstant.ContactType.CUSTOMER.getKey()))
                    .or(wrapper -> wrapper.in(CompanyContactInfo::getRelationId, companyIds).eq(CompanyContactInfo::getType, LyncsOdsConstant.ContactType.SUPPLIER.getKey()));
        } else if (LyncsOdsConstant.ContactType.CUSTOMER.getKey().equals(type)) {
            //查供应商好友关系
            queryChainWrapper.and(wrapper -> wrapper.in(CompanyContactInfo::getCompanyId, companyIds).eq(CompanyContactInfo::getType, LyncsOdsConstant.ContactType.SUPPLIER.getKey()))
                    .or(wrapper -> wrapper.in(CompanyContactInfo::getRelationId, companyIds).eq(CompanyContactInfo::getType, LyncsOdsConstant.ContactType.CUSTOMER.getKey()));
        }
        return queryChainWrapper.list();
    }

    @Override
    public Map<Long, String> getConcatNameMap(List<Long> companyIds, Integer type) {
        if (CollectionUtil.isEmpty(companyIds) || Objects.isNull(type)) {
            return Map.of();
        }
        List<CompanyContactInfo> companyContactInfos = getConcatInfoByCompanyIds(companyIds, type);
        //暂不支持被动建立好友关系的情景，B类企业不会有查看好友的操作
        return companyContactInfos.stream().filter(info -> companyIds.contains(info.getCompanyId())).collect(Collectors.toMap(CompanyContactInfo::getRelationId, CompanyContactInfo::getNickname));
    }
}
