package com.lyncs.ods.modules.company.service;

import com.lyncs.ods.modules.company.model.CompanyPaymentInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 企业收款信息 服务类
 * </p>
 *
 * @author alex
 * @since 2022-03-29
 */
public interface CompanyPaymentInfoService extends IService<CompanyPaymentInfo> {

}
