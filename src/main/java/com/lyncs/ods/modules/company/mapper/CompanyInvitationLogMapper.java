package com.lyncs.ods.modules.company.mapper;

import com.lyncs.ods.modules.company.model.CompanyInvitationLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 公司邀请记录表 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2022-03-29
 */
@Mapper
public interface CompanyInvitationLogMapper extends BaseMapper<CompanyInvitationLog> {

}
