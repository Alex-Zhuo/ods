package com.lyncs.ods.modules.company.service.impl;

import com.lyncs.ods.modules.company.model.CompanySpuRelation;
import com.lyncs.ods.modules.company.mapper.CompanySpuRelationMapper;
import com.lyncs.ods.modules.company.service.CompanySpuRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 企业与spu关联表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
@Service
public class CompanySpuRelationServiceImpl extends ServiceImpl<CompanySpuRelationMapper, CompanySpuRelation> implements CompanySpuRelationService {

}
