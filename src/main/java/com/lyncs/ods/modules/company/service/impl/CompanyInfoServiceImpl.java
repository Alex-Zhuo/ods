package com.lyncs.ods.modules.company.service.impl;

import com.lyncs.ods.modules.company.model.CompanyInfo;
import com.lyncs.ods.modules.company.mapper.CompanyInfoMapper;
import com.lyncs.ods.modules.company.service.CompanyInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 公司账号信息表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-03-29
 */
@Service
public class CompanyInfoServiceImpl extends ServiceImpl<CompanyInfoMapper, CompanyInfo> implements CompanyInfoService {

}
