package com.lyncs.ods.modules.company.service;

import com.lyncs.ods.modules.company.model.CompanySpuRelation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 企业与spu关联表 服务类
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
public interface CompanySpuRelationService extends IService<CompanySpuRelation> {

}
