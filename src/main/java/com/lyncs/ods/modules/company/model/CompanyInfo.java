package com.lyncs.ods.modules.company.model;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 企业账号信息表
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("company_info")
@ApiModel(value = "CompanyInfo对象", description = "企业账号信息表")
public class CompanyInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("企业id")
    @TableId(value = "id")
    private Long id;

    @ApiModelProperty("企业名称")
    @TableField("name")
    private String name;

    @ApiModelProperty("企业简称")
    @TableField("short_name")
    private String shortName;

    @ApiModelProperty("所属行业")
    @TableField("industry")
    private String industry;

    @ApiModelProperty("员工规模")
    @TableField("scale")
    private String scale;

    @ApiModelProperty("企业头像url")
    @TableField("avatar_url")
    private String avatarUrl;

    @ApiModelProperty("状态：0.体验;1.VIP;2.SVIP")
    @TableField("level_status")
    private Boolean levelStatus;

    @ApiModelProperty("企业当前等级过期时间")
    @TableField("level_expire_time")
    private LocalDateTime levelExpireTime;

    @ApiModelProperty("电话号码")
    @TableField("telephone")
    private String telephone;

    @ApiModelProperty("传真号码")
    @TableField("fax")
    private String fax;

    @ApiModelProperty("邮箱")
    @TableField("email")
    private String email;

    @ApiModelProperty("企业地址")
    @TableField("address")
    private String address;

    @ApiModelProperty("企业标语")
    @TableField("slogan")
    private String slogan;

    @ApiModelProperty("企业背景图URL")
    @TableField("background_img")
    private String backgroundImg;

    @ApiModelProperty("状态：1.启用；2.禁用")
    @TableField("status")
    private Integer status;

    @ApiModelProperty("创建者用户id")
    @TableField("creator")
    private Long creator;

    @ApiModelProperty("创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


}
