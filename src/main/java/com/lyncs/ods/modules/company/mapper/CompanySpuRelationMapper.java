package com.lyncs.ods.modules.company.mapper;

import com.lyncs.ods.modules.company.model.CompanySpuRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 企业与spu关联表 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
@Mapper
public interface CompanySpuRelationMapper extends BaseMapper<CompanySpuRelation> {

}
