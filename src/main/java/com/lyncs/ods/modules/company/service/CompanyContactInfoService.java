package com.lyncs.ods.modules.company.service;

import com.lyncs.ods.modules.company.model.CompanyContactInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 企业通讯录 服务类
 * </p>
 *
 * @author alex
 * @since 2022-03-29
 */
public interface CompanyContactInfoService extends IService<CompanyContactInfo> {

    List<CompanyContactInfo> getConcatInfoByCompanyIds(List<Long> companyIds, Integer type);

    Map<Long, String> getConcatNameMap(List<Long> companyIds, Integer type);
}
