package com.lyncs.ods.modules.company.service;

import com.lyncs.ods.modules.company.model.CompanyFeeInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 企业款项配置表 服务类
 * </p>
 *
 * @author alex
 * @since 2022-03-29
 */
public interface CompanyFeeInfoService extends IService<CompanyFeeInfo> {

}
