package com.lyncs.ods.modules.company.service;

import com.lyncs.ods.modules.company.model.CompanyInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 公司账号信息表 服务类
 * </p>
 *
 * @author alex
 * @since 2022-03-29
 */
public interface CompanyInfoService extends IService<CompanyInfo> {

}
