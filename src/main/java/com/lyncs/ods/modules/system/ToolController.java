package com.lyncs.ods.modules.system;

import cn.hutool.core.lang.Validator;
import cn.hutool.extra.mail.MailUtil;
import com.lyncs.ods.common.api.CommonResult;
import com.lyncs.ods.modules.msg.service.MessageService;
import com.lyncs.ods.utils.CurrencyUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;

/**
 * @author alex
 * @date 2022/3/20 20:14
 * @description
 */
@RestController
@Api(tags = {"工具相关接口"})
@RequestMapping("tool")
public class ToolController {

    @Autowired
    MessageService messageService;

    @GetMapping("sms")
    @ApiOperation(value = "发送短信验证码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "手机号码", dataType = "String", paramType = "query"),
    })
    public CommonResult<?> sendSms(String phone) {
        messageService.sendSmsCode(phone);
        return CommonResult.success();
    }

    @GetMapping("upload")
    @ApiOperation(value = "上传图片/文件")
    @ApiResponses(@ApiResponse(code = 200, message = "返回文件上传后的地址"))
    public CommonResult<String> sendSms(@RequestParam("file") MultipartFile file) {
        return CommonResult.success();
    }

    @GetMapping("email")
    @ApiOperation(value = "发送邮箱验证码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "email", value = "收件人邮箱地址", dataType = "String", paramType = "query"),
    })
    public CommonResult<?> sendEmail1(String email) {
        if (!Validator.isEmail(email)) {
            throw new RuntimeException("invalid email");
        }
        messageService.sendEmailCode(email);
        return CommonResult.success();
    }

    @GetMapping("exchange")
    @ApiOperation(value = "汇率转换")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "fromCurrency", value = "原货币", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "toCurrency", value = "目标货币", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "amount", value = "转换金额", dataType = "BigDecimal", paramType = "query"),
    })
    public CommonResult<BigDecimal> exchange(@RequestParam String fromCurrency, @RequestParam String toCurrency, @RequestParam BigDecimal amount) {
        return CommonResult.success(CurrencyUtil.exchange(fromCurrency, toCurrency, amount));
    }
}
