package com.lyncs.ods.modules.user.service.impl;

import com.lyncs.ods.modules.user.model.CompanyUserRelation;
import com.lyncs.ods.modules.user.mapper.CompanyUserRelationMapper;
import com.lyncs.ods.modules.user.service.CompanyUserRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 个人与公司关联关系表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
@Service
public class CompanyUserRelationServiceImpl extends ServiceImpl<CompanyUserRelationMapper, CompanyUserRelation> implements CompanyUserRelationService {

}
