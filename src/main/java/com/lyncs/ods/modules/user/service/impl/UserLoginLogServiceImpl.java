package com.lyncs.ods.modules.user.service.impl;

import com.lyncs.ods.modules.user.model.UserLoginLog;
import com.lyncs.ods.modules.user.mapper.UserLoginLogMapper;
import com.lyncs.ods.modules.user.service.UserLoginLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户登录日志 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
@Service
public class UserLoginLogServiceImpl extends ServiceImpl<UserLoginLogMapper, UserLoginLog> implements UserLoginLogService {

}
