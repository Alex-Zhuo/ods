package com.lyncs.ods.modules.user.service.impl;

import com.lyncs.ods.modules.user.model.UserAuthInfo;
import com.lyncs.ods.modules.user.mapper.UserAuthInfoMapper;
import com.lyncs.ods.modules.user.service.UserAuthInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 第三方账号信息授权表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
@Service
public class UserAuthInfoServiceImpl extends ServiceImpl<UserAuthInfoMapper, UserAuthInfo> implements UserAuthInfoService {

}
