package com.lyncs.ods.modules.user.model;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 第三方账号信息授权表
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("user_auth_info")
@ApiModel(value = "UserAuthInfo对象", description = "第三方账号信息授权表")
public class UserAuthInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("自增主键id")
    @TableId(value = "id")
    private Long id;

    @ApiModelProperty("用户id")
    @TableField("user_id")
    private Long userId;

    @ApiModelProperty("appId")
    @TableField("app_id")
    private String appId;

    @ApiModelProperty("用户来源类型:WeChatMiniApp/WeChatWebApp...")
    @TableField("auth_type")
    private String authType;

    @ApiModelProperty("第三方授权ID")
    @TableField("open_id")
    private String openId;

    @ApiModelProperty("第三方用户昵称")
    @TableField("nickname")
    private String nickname;

    @ApiModelProperty("第三方授权UnionID")
    @TableField("union_id")
    private String unionId;

    @ApiModelProperty("access_token")
    @TableField("access_token")
    private String accessToken;

    @ApiModelProperty("刷新access_token的凭证")
    @TableField("refresh_token")
    private String refreshToken;

    @ApiModelProperty("创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


}
