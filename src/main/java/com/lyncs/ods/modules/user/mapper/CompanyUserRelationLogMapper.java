package com.lyncs.ods.modules.user.mapper;

import com.lyncs.ods.modules.user.model.CompanyUserRelationLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 公司与个人关联关系操作记录表 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface CompanyUserRelationLogMapper extends BaseMapper<CompanyUserRelationLog> {

}
