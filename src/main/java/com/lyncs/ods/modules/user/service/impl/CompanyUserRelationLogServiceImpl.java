package com.lyncs.ods.modules.user.service.impl;

import com.lyncs.ods.modules.user.model.CompanyUserRelationLog;
import com.lyncs.ods.modules.user.mapper.CompanyUserRelationLogMapper;
import com.lyncs.ods.modules.user.service.CompanyUserRelationLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 公司与个人关联关系操作记录表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
@Service
public class CompanyUserRelationLogServiceImpl extends ServiceImpl<CompanyUserRelationLogMapper, CompanyUserRelationLog> implements CompanyUserRelationLogService {

}
