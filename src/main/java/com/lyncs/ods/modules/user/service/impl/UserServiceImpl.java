package com.lyncs.ods.modules.user.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.crypto.digest.MD5;
import cn.hutool.json.JSONUtil;
import com.lyncs.ods.common.api.ResultCode;
import com.lyncs.ods.common.exception.ApiException;
import com.lyncs.ods.common.web.RequestHolder;
import com.lyncs.ods.config.WxMpConfiguration;
import com.lyncs.ods.constant.LyncsOdsConstant;
import com.lyncs.ods.domain.AdminUserDetails;
import com.lyncs.ods.modules.company.model.CompanyInfo;
import com.lyncs.ods.modules.company.service.CompanyInfoService;
import com.lyncs.ods.modules.msg.model.MsgValidateLog;
import com.lyncs.ods.modules.msg.service.MsgValidateLogService;
import com.lyncs.ods.modules.user.model.*;
import com.lyncs.ods.modules.user.service.*;
import com.lyncs.ods.req.BindCompanyReq;
import com.lyncs.ods.req.CompanyRelateReq;
import com.lyncs.ods.req.SaveUserInfoReq;
import com.lyncs.ods.resp.UserCompanyResp;
import com.lyncs.ods.resp.UserInfoResp;
import com.lyncs.ods.security.util.JwtTokenUtil;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.bean.WxOAuth2UserInfo;
import me.chanjar.weixin.common.bean.oauth2.WxOAuth2AccessToken;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author alex
 * @date 2022/1/29 21:53
 * @description
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    private UserAuthInfoService userAuthInfoService;
    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private UserLoginLogService userLoginLogService;
    @Autowired
    private CompanyInfoService companyInfoService;
    @Autowired
    private CompanyUserRelationService companyUserRelationService;
    @Autowired
    private CompanyUserRelationLogService relationRecordService;
    @Autowired
    private MsgValidateLogService msgValidateService;

    /**
     * 用户微信登录
     *
     * @param appid    appid
     * @param code     微信授权code
     * @param state    微信加密数据
     * @param platform 登录平台
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long login(String appid, String code, String state, String platform) throws WxErrorException {
        WxMpService wxMpService = WxMpConfiguration.getMpServices().get(appid);
        String date = DateUtil.format(new Date(), LyncsOdsConstant.Common.DATE_PATTERN);
        String md5 = MD5.create().digestHex(StringUtils.join(LyncsOdsConstant.Common.MD5_PREFIX, date));
        if (LyncsOdsConstant.AuthPlatform.WECHAT_WEB_APP.getKey().equals(platform) && !md5.equals(state)) {
            throw new ApiException(ResultCode.VALIDATE_FAILED);
        }
        WxOAuth2AccessToken oAuth2AccessToken = wxMpService.getOAuth2Service().getAccessToken(code);
        WxOAuth2UserInfo wxMpUser = wxMpService.getOAuth2Service().getUserInfo(oAuth2AccessToken, null);
        log.info("oAuth2AccessToken:{}", JSONUtil.parse(oAuth2AccessToken));
        log.info("wxMpUser:{}", JSONUtil.parse(wxMpUser));

        //登录注册逻辑
        String openId = oAuth2AccessToken.getOpenId();
        String accessToken = oAuth2AccessToken.getAccessToken();
        String unionId = oAuth2AccessToken.getUnionId();
        String refreshToken = oAuth2AccessToken.getRefreshToken();
        UserInfo userInfo;
        //当前微信应用授权信息
        UserAuthInfo thisAppUserAuthInfo = null;
        List<UserAuthInfo> authInfos = userAuthInfoService.lambdaQuery().eq(UserAuthInfo::getUnionId, oAuth2AccessToken.getUnionId()).list();
        if (CollectionUtil.isNotEmpty(authInfos)) {
            Long userId = authInfos.get(0).getUserId();
            userInfo = userInfoService.lambdaQuery().eq(UserInfo::getId, userId).one();
            thisAppUserAuthInfo = authInfos.stream().filter(info -> info.getAppId().equals(appid) && info.getOpenId().equals(openId)).findFirst().orElse(null);
        } else {
            //当前用户未通过任何微信平台登录过，视为新用户
            Long userId = IdUtil.getSnowflakeNextId();
            userInfo = new UserInfo().setId(userId).setUsername(wxMpUser.getNickname()).setAvatarUrl(wxMpUser.getHeadImgUrl()).setStatus(LyncsOdsConstant.UserStatus.INIT.getKey());
            userInfoService.save(userInfo);
        }

        if (Objects.isNull(thisAppUserAuthInfo)) {
            log.info("user has not registered the wechat app of {}, userId:{}", appid, userInfo.getId());
            thisAppUserAuthInfo = new UserAuthInfo().setUserId(userInfo.getId()).setAppId(appid).setAuthType(wxMpService.getWxMpConfigStorage().getAesKey()).setOpenId(openId).setNickname(wxMpUser.getNickname()).setUnionId(unionId);
        }
        thisAppUserAuthInfo.setAccessToken(accessToken).setRefreshToken(refreshToken);
        userAuthInfoService.saveOrUpdate(thisAppUserAuthInfo);
        if (LyncsOdsConstant.UserStatus.DISABLE.getKey().equals(userInfo.getStatus())) {
            throw new ApiException(ResultCode.ACCOUNT_DISABLE);
        }
        saveTokenAndLog(userInfo, thisAppUserAuthInfo, accessToken, Boolean.FALSE);
        return userInfo.getId();
    }

    /**
     * 根据openID 获取jwt user
     *
     * @param openId openId
     * @return UserDetails
     */
    @Override
    public UserDetails loadUserByOpenId(String openId) {
        UserAuthInfo userAuthInfo = userAuthInfoService.lambdaQuery().eq(UserAuthInfo::getOpenId, openId).one();
        UserInfo userInfo = userInfoService.lambdaQuery().eq(UserInfo::getId, userAuthInfo.getUserId()).one();
        return loadUserDetails(userInfo, userAuthInfo);
    }

    @Override
    public void logout() {
        RequestHolder.removeCookie();
    }

    /**
     * list user companies(all available)
     *
     * @return all available companies
     */
    @Override
    public List<UserCompanyResp> listCompanies() {
        Long userId = RequestHolder.getUserId();
        UserInfo userInfo = userInfoService.lambdaQuery().eq(UserInfo::getId, userId).one();
        List<CompanyUserRelation> companyUserRelations = companyUserRelationService.lambdaQuery().eq(CompanyUserRelation::getUserId, userId).eq(CompanyUserRelation::getStatus, LyncsOdsConstant.BindStatus.BIND.getKey()).list();
        Set<Long> companyIds = companyUserRelations.stream().map(CompanyUserRelation::getCompanyId).collect(Collectors.toSet());
        List<CompanyInfo> companyInfos = companyInfoService.lambdaQuery().in(CompanyInfo::getId, companyIds).eq(CompanyInfo::getStatus, LyncsOdsConstant.CompanyStatus.ENABLE.getKey()).list();
        Map<Long, CompanyInfo> companyInfoMap = companyInfos.stream().collect(Collectors.toMap(CompanyInfo::getId, o -> o));
        return companyUserRelations.stream().map(info -> {
            CompanyInfo companyInfo = companyInfoMap.get(info.getCompanyId());
            if (Objects.isNull(companyInfo)) {
                return null;
            }
            UserCompanyResp resp = new UserCompanyResp();
            resp.setCompanyId(info.getCompanyId());
            resp.setNickname(info.getNickname());
            resp.setAvatarUrl(companyInfo.getAvatarUrl());
            resp.setAddress(companyInfo.getAddress());
            resp.setUserId(userId);
            resp.setRecentLoginCompanyId(userInfo.getRecentLoginCompanyId());
            return resp;
        }).filter(Objects::nonNull).collect(Collectors.toList());
    }

    /**
     * bind one company with login
     *
     * @param req request info
     */
    @Override
    public void bindCompany(BindCompanyReq req) {
        CompanyInfo companyInfo = companyInfoService.lambdaQuery().eq(CompanyInfo::getId, req.getCompanyId()).one();
        if (Objects.isNull(companyInfo) || !LyncsOdsConstant.CompanyStatus.ENABLE.getKey().equals(companyInfo.getStatus())) {
            throw new ApiException("company not found or unavailable.");
        }
        RequestHolder.saveCookie(LyncsOdsConstant.Header.COOKIE_PC, companyInfo.getId().toString());
    }

    /**
     * join one company by QR code
     *
     * @param req request info
     */
    @Override
    public void relateCompany(CompanyRelateReq req) {
        CompanyInfo companyInfo = companyInfoService.lambdaQuery().eq(CompanyInfo::getId, req.getCompanyId()).one();
        if (Objects.isNull(companyInfo) || !LyncsOdsConstant.CompanyStatus.ENABLE.getKey().equals(companyInfo.getStatus())) {
            throw new ApiException("company not found or unavailable.");
        }
        companyUserRelation(RequestHolder.getUserId(), req.getCompanyId(), req.getRole());
        userInfoService.lambdaUpdate().eq(UserInfo::getId, RequestHolder.getUserId())
                .set(UserInfo::getRecentLoginCompanyId, req.getCompanyId())
                .update();
    }

    @Override
    public void saveUserInfo(SaveUserInfoReq req) {
        UserInfo userInfo = userInfoService.lambdaQuery().eq(UserInfo::getId, RequestHolder.getUserId()).one();
        if (Objects.isNull(userInfo)) {
            throw new ApiException("user not exist.");
        }
        if (StringUtils.isNotEmpty(req.getPhone())) {
            checkSmsCode(RequestHolder.getUserId(), LyncsOdsConstant.MessageType.SMS, req.getCode());
        }
        if (StringUtils.isNotEmpty(req.getEmail()) && StringUtils.isEmpty(req.getPhone())) {
            checkSmsCode(RequestHolder.getUserId(), LyncsOdsConstant.MessageType.EMAIL, req.getCode());
        }
        userInfoService.lambdaUpdate().eq(UserInfo::getId, RequestHolder.getUserId())
                .set(StringUtils.isNotEmpty(req.getPhone()), UserInfo::getPhone, req.getPhone())
                .set(StringUtils.isNotEmpty(req.getEmail()), UserInfo::getEmail, req.getEmail())
                .set(StringUtils.isNotEmpty(req.getAvatarUrl()), UserInfo::getAvatarUrl, req.getAvatarUrl())
                .set(StringUtils.isNotEmpty(req.getUsername()), UserInfo::getUsername, req.getUsername())
                .update();
    }

    /**
     * 手机号登录
     *
     * @param phone    手机号
     * @param code     验证码
     * @param platform 授权平台
     */
    @Override
    public Long login(String phone, String code, String platform) {
        UserInfo userInfo = checkPhone(phone);
        if (LyncsOdsConstant.UserStatus.DISABLE.getKey().equals(userInfo.getStatus())) {
            throw new ApiException(ResultCode.ACCOUNT_DISABLE);
        }
        checkSmsCode(userInfo.getId(), LyncsOdsConstant.MessageType.SMS, code);
        UserAuthInfo userAuthInfo = userAuthInfoService.lambdaQuery().eq(UserAuthInfo::getUserId, userInfo.getId()).eq(UserAuthInfo::getAuthType, platform).one();
        saveTokenAndLog(userInfo, userAuthInfo, null, Boolean.TRUE);
        return userInfo.getId();
    }

    /**
     * get user info by token
     *
     * @return user info
     */
    @Override
    public UserInfoResp getUserInfo() {
        String authType = RequestHolder.getAuthType();
        Long userId = RequestHolder.getUserId();
        UserInfo userInfo = userInfoService.lambdaQuery().eq(UserInfo::getId, userId).eq(UserInfo::getStatus, LyncsOdsConstant.UserStatus.ENABLE.getKey()).one();
        UserAuthInfo userAuthInfo = userAuthInfoService.lambdaQuery().eq(UserAuthInfo::getUserId, userId).eq(UserAuthInfo::getAuthType, authType).one();
        if (Objects.isNull(userInfo) || Objects.isNull(userAuthInfo)) {
            throw new ApiException("user not exist or unauthorized.");
        }
        return new UserInfoResp()
                .setAvatarUrl(userInfo.getAvatarUrl())
                .setEmail(userInfo.getEmail())
                .setNickname(userInfo.getUsername())
                .setPhoneNo(userInfo.getPhone())
                .setWeChatNickname(userAuthInfo.getNickname());
    }

    @Override
    public void bindThirdInfo(Integer type, String value, String code) {
        LyncsOdsConstant.MessageType messageType = Arrays.stream(LyncsOdsConstant.MessageType.values()).filter(msg -> msg.getKey().equals(type)).findAny().orElse(null);
        if (Objects.isNull(messageType)) {
            throw new ApiException("invalid type");
        }
        checkSmsCode(RequestHolder.getUserId(), messageType, code);
        userInfoService.lambdaUpdate().eq(UserInfo::getId, RequestHolder.getUserId())
                .set(LyncsOdsConstant.MessageType.SMS.getKey().equals(type), UserInfo::getPhone, value)
                .set(LyncsOdsConstant.MessageType.EMAIL.getKey().equals(type), UserInfo::getEmail, value)
                .update();
    }

    private UserDetails loadUserDetails(UserInfo userInfo, UserAuthInfo userAuthInfo) {
        return new AdminUserDetails(userInfo, userAuthInfo);
    }

    private void companyUserRelation(Long userId, Long cid, Integer role) {
        UserInfo userInfo = userInfoService.lambdaQuery().eq(UserInfo::getId, userId).one();
        companyUserRelationService.save(new CompanyUserRelation().setCompanyId(cid).setUserId(userId).setRole(role).setNickname(userInfo.getUsername()));
        relationRecordService.save(new CompanyUserRelationLog().setId(IdUtil.getSnowflakeNextId()).setUserId(userId).setCompanyId(cid).setStatus(LyncsOdsConstant.BindStatus.BIND.getKey()));
    }

    private void checkSmsCode(Long userId, LyncsOdsConstant.MessageType type, String code) {
        MsgValidateLog record = msgValidateService.lambdaQuery()
                .eq(MsgValidateLog::getType, type.getKey())
                .eq(MsgValidateLog::getUserId, userId)
                .eq(MsgValidateLog::getCount, 0)
                .eq(MsgValidateLog::getStatus, LyncsOdsConstant.MessageStatus.SUCCESS.getKey())
                .orderByDesc(MsgValidateLog::getCreateTime).last("limit 1").one();
        if (Objects.isNull(record) || !record.getCode().equals(code)) {
            throw new ApiException(type.getCode() + " verification code is wrong or has expired.");
        }
        msgValidateService.lambdaUpdate().eq(MsgValidateLog::getId, record.getId())
                .set(MsgValidateLog::getStatus, LyncsOdsConstant.MessageStatus.VERIFIED.getKey())
                .update();
    }

    private UserInfo checkPhone(String phone) {
        UserInfo userInfo = userInfoService.lambdaQuery().eq(UserInfo::getPhone, phone).one();
        if (Objects.isNull(userInfo)) {
            throw new ApiException(ResultCode.PHONE_NOT_REGISTERED);
        }
        return userInfo;
    }

    private void saveTokenAndLog(UserInfo userInfo, UserAuthInfo userAuthInfo, String accessToken, Boolean checkPhone) {
        //save to jwt
        UserDetails userDetails = loadUserDetails(userInfo, userAuthInfo);
        //generate token
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = jwtTokenUtil.generateToken(userDetails);
        log.info("user login, id:{}, token:{}", userInfo.getId(), token);
        //此时还未选择登录公司
        UserLoginLog userLoginRecord = new UserLoginLog().setId(IdUtil.getSnowflakeNextId()).setToken(accessToken).setUserId(userInfo.getId());
        //insertLoginLog(username);
        userLoginLogService.save(userLoginRecord);
        if (checkPhone) {
            checkPhone(userInfo.getPhone());
        }
        RequestHolder.saveCookie(LyncsOdsConstant.Header.COOKIE_PT, token);
    }
}
