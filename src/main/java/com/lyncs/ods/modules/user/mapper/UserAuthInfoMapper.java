package com.lyncs.ods.modules.user.mapper;

import com.lyncs.ods.modules.user.model.UserAuthInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 第三方账号信息授权表 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface UserAuthInfoMapper extends BaseMapper<UserAuthInfo> {

}
