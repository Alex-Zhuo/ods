package com.lyncs.ods.modules.user.service;

import com.lyncs.ods.modules.user.model.UserInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 个人账号信息表 服务类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface UserInfoService extends IService<UserInfo> {

}
