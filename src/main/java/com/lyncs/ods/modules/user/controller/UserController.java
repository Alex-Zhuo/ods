package com.lyncs.ods.modules.user.controller;


import com.lyncs.ods.common.api.CommonResult;
import com.lyncs.ods.modules.user.service.UserService;
import com.lyncs.ods.req.*;
import com.lyncs.ods.resp.*;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 个人账号信息 前端控制器
 * </p>
 *
 * @author alex
 * @since 2022-01-29
 */
@RestController
@RequestMapping("/user")
@Api(value = "用户相关接口", tags = {"用户相关接口"})
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 登录
     *
     * @return T/F
     */
    @ApiOperation(value = "手机号登录")
    @GetMapping("/login")
    @ApiResponses(@ApiResponse(code = 200, message = "返回userId"))
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "手机号", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "code", value = "短信验证码", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "platform", value = "授权登录平台:weChatWebApp,weChatMiniApp。默认weChatWebApp", dataType = "String", paramType = "query"),
    })
    public CommonResult<Long> login(@RequestParam String phone, @RequestParam String code, @RequestParam String platform) {
        return CommonResult.success(userService.login(phone, code, platform));
    }

    @ApiOperation(value = "更换绑定信息")
    @GetMapping("/bind")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "需要换绑个人信息类型: 1.手机号;2.邮箱;3.微信(未实现)", dataType = "Integer", paramType = "query"),
            @ApiImplicitParam(name = "value", value = "换绑后的个人信息: type=1为手机号码;type=2为邮箱账号;type=3为微信号", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "code", value = "对应的验证码信息", dataType = "String", paramType = "query"),
    })
    public CommonResult<?> bindThirdInfo(@RequestParam Integer type, @RequestParam String value, @RequestParam String code) {
        userService.bindThirdInfo(type, value, code);
        return CommonResult.success();
    }

    /**
     * 更新用户信息
     *
     * @return T/F
     */
    @ApiOperation(value = "更新用户信息")
    @PostMapping("/save")
    public CommonResult<?> saveUserInfo(@Valid @RequestBody SaveUserInfoReq req) {
        userService.saveUserInfo(req);
        return CommonResult.success();
    }

    /**
     * 获取个人信息
     *
     * @return user info
     */
    @ApiOperation(value = "获取个人信息")
    @GetMapping("/info")
    public CommonResult<UserInfoResp> getUserInfo() {
        return CommonResult.success(userService.getUserInfo());
    }

    /**
     * 退出登录
     *
     * @return T/F
     */
    @ApiOperation(value = "退出登录")
    @GetMapping("/logout")
    public CommonResult<?> logout() {
        userService.logout();
        return CommonResult.success();
    }


    /**
     * list company by user
     *
     * @return companies
     */
    @GetMapping("/companies")
    @ApiOperation(value = "获取所有可用状态的与用户绑定的公司")
    public CommonResult<List<UserCompanyResp>> listCompanies() {
        return CommonResult.success(userService.listCompanies());
    }

    /**
     * bind one company
     *
     * @return T/F
     */
    @PostMapping("/bindCompany")
    @ApiOperation(value = "登录后选择某个公司作为登录公司")
    public CommonResult<Long> bindCompany(@Valid @RequestBody BindCompanyReq req) {
        userService.bindCompany(req);
        return CommonResult.success();
    }

    /**
     * join one company by QR code
     *
     * @param req request info
     * @return T/F
     */
    @PostMapping("/relateCompany")
    @ApiOperation(value = "扫描加入某公司")
    public CommonResult<?> relateCompany(@Valid @RequestBody CompanyRelateReq req) {
        userService.relateCompany(req);
        return CommonResult.success();
    }


}

