package com.lyncs.ods.modules.user.service;

import com.lyncs.ods.req.*;
import com.lyncs.ods.req.SavePartnerCompanyReq;
import com.lyncs.ods.resp.*;
import me.chanjar.weixin.common.error.WxErrorException;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

/**
 * @author alex
 * @date 2022/1/29 21:53
 * @description
 */
public interface UserService {
    /**
     * 用户微信登录
     *
     * @param appid    appid
     * @param code     微信授权code
     * @param state    微信加密数据
     * @param platform 登录平台
     * @return userId
     * @throws WxErrorException 微信授权异常
     */
    Long login(String appid, String code, String state, String platform) throws WxErrorException;

    /**
     * 根据openID 获取jwt user
     *
     * @param openId openId
     * @return UserDetails
     */
    UserDetails loadUserByOpenId(String openId);



    /**
     * logout
     */
    void logout();

    /**
     * list user companies(all available)
     *
     * @return all available companies
     */
    List<UserCompanyResp> listCompanies();

    /**
     * bind one company with login
     *
     * @param req request info
     */
    void bindCompany(BindCompanyReq req);

    /**
     * join one company by QR code
     *
     * @param req request info
     */
    void relateCompany(CompanyRelateReq req);

    /**
     * 用户绑定手机号
     *
     * @param req request info
     */
    void saveUserInfo(SaveUserInfoReq req);

    /**
     * 手机号登录
     *
     * @param phone    手机号
     * @param code     验证码
     * @param platform 授权平台
     * @return userId
     */
    Long login(String phone, String code, String platform);

    /**
     * get user info by token
     *
     * @return user info
     */
    UserInfoResp getUserInfo();

    /**
     * 更换绑定信息
     *
     * @param type  类型
     * @param value 账号
     * @param code  验证码
     */
    void bindThirdInfo(Integer type, String value, String code);
}
