package com.lyncs.ods.modules.user.service;

import com.lyncs.ods.modules.user.model.CompanyUserRelation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 个人与公司关联关系表 服务类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface CompanyUserRelationService extends IService<CompanyUserRelation> {

}
