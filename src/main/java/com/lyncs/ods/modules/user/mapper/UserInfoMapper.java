package com.lyncs.ods.modules.user.mapper;

import com.lyncs.ods.modules.user.model.UserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 个人账号信息表 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface UserInfoMapper extends BaseMapper<UserInfo> {

}
