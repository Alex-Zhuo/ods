package com.lyncs.ods.modules.user.model;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 个人与企业关联关系表
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("company_user_relation")
@ApiModel(value = "CompanyUserRelation对象", description = "个人与企业关联关系表")
public class CompanyUserRelation implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("自增主键id")
    @TableId(value = "id")
    private Long id;

    @ApiModelProperty("企业id")
    @TableField("company_id")
    private Long companyId;

    @ApiModelProperty("用户id")
    @TableField("user_id")
    private Long userId;

    @ApiModelProperty("用户昵称")
    @TableField("nickname")
    private String nickname;

    @ApiModelProperty("工号")
    @TableField("no")
    private String no;

    @ApiModelProperty("部门")
    @TableField("dept")
    private String dept;

    @ApiModelProperty("岗位")
    @TableField("post")
    private String post;

    @ApiModelProperty("角色:1.admin;")
    @TableField("role")
    private Integer role;

    @ApiModelProperty("备注")
    @TableField("remark")
    private String remark;

    @ApiModelProperty("状态：1.绑定；2.解绑")
    @TableField("status")
    private Boolean status;

    @ApiModelProperty("创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


}
