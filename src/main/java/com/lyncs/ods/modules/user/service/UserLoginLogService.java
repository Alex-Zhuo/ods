package com.lyncs.ods.modules.user.service;

import com.lyncs.ods.modules.user.model.UserLoginLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户登录日志 服务类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface UserLoginLogService extends IService<UserLoginLog> {

}
