package com.lyncs.ods.modules.user.model;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 个人账号信息表
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("user_info")
@ApiModel(value = "UserInfo对象", description = "个人账号信息表")
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("用户id")
    @TableId(value = "id")
    private Long id;

    @ApiModelProperty("用户头像url")
    @TableField("avatar_url")
    private String avatarUrl;

    @ApiModelProperty("用户姓名")
    @TableField("username")
    private String username;

    @ApiModelProperty("用户绑定手机号")
    @TableField("phone")
    private String phone;

    @ApiModelProperty("用户绑定邮箱")
    @TableField("email")
    private String email;

    @ApiModelProperty("上次登录的企业ID")
    @TableField("recent_login_company_id")
    private Long recentLoginCompanyId;

    @ApiModelProperty("状态：0.已创建；1.可用；2.已禁用")
    @TableField("status")
    private Integer status;

    @ApiModelProperty("创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


}
