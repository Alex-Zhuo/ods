package com.lyncs.ods.modules.user.service;

import com.lyncs.ods.modules.user.model.CompanyUserRelationLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 公司与个人关联关系操作记录表 服务类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface CompanyUserRelationLogService extends IService<CompanyUserRelationLog> {

}
