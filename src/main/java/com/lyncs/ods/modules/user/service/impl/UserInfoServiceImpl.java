package com.lyncs.ods.modules.user.service.impl;

import com.lyncs.ods.modules.user.model.UserInfo;
import com.lyncs.ods.modules.user.mapper.UserInfoMapper;
import com.lyncs.ods.modules.user.service.UserInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 个人账号信息表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements UserInfoService {

}
