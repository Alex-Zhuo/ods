package com.lyncs.ods.modules.user.service;

import com.lyncs.ods.modules.user.model.UserAuthInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 第三方账号信息授权表 服务类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface UserAuthInfoService extends IService<UserAuthInfo> {

}
