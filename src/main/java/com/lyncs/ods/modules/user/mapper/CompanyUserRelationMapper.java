package com.lyncs.ods.modules.user.mapper;

import com.lyncs.ods.modules.user.model.CompanyUserRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 个人与公司关联关系表 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface CompanyUserRelationMapper extends BaseMapper<CompanyUserRelation> {

}
