package com.lyncs.ods.modules.user.mapper;

import com.lyncs.ods.modules.user.model.UserLoginLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户登录日志 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface UserLoginLogMapper extends BaseMapper<UserLoginLog> {

}
