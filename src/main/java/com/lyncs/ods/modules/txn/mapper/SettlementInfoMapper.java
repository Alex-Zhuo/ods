package com.lyncs.ods.modules.txn.mapper;

import com.lyncs.ods.modules.txn.model.SettlementInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 结算单总表 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2022-03-10
 */
public interface SettlementInfoMapper extends BaseMapper<SettlementInfo> {

}
