package com.lyncs.ods.modules.txn.model;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 结算单总表
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("settlement_info")
@ApiModel(value = "SettlementInfo对象", description = "结算单总表")
public class SettlementInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("结算单id")
    @TableId(value = "id")
    private Long id;

    @ApiModelProperty("卖方企业id")
    @TableField("seller_id")
    private Long sellerId;

    @ApiModelProperty("买方企业id")
    @TableField("buyer_id")
    private Long buyerId;

    @ApiModelProperty("结算单总金额(冗余字段，值为所有款项金额之和)")
    @TableField("total_amount")
    private String totalAmount;

    @ApiModelProperty("关联的账款数量")
    @TableField("category_count")
    private Integer categoryCount;

    @ApiModelProperty("结算货币")
    @TableField("currency")
    private String currency;

    @ApiModelProperty("结算开始时间")
    @TableField("start_time")
    private LocalDateTime startTime;

    @ApiModelProperty("状态：0.草稿；1.已创建（已出账单）；2.结算中（已结算待确认）；3.已确认")
    @TableField("status")
    private Integer status;

    @ApiModelProperty("备注")
    @TableField("remark")
    private String remark;

    @ApiModelProperty("创建者用户id")
    @TableField("creator")
    private Long creator;

    @ApiModelProperty("创建者所属企业ID")
    @TableField("creator_company_id")
    private Long creatorCompanyId;

    @ApiModelProperty("最后更新用户id")
    @TableField("last_updater")
    private Long lastUpdater;

    @ApiModelProperty("结算截止时间")
    @TableField("deadline")
    private LocalDateTime deadline;

    @ApiModelProperty("创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


}
