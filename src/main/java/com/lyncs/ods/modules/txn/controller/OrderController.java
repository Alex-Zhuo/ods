package com.lyncs.ods.modules.txn.controller;


import com.lyncs.ods.common.api.CommonResult;
import com.lyncs.ods.modules.txn.service.OrderService;
import com.lyncs.ods.req.ListPageSearchReq;
import com.lyncs.ods.req.SaveOrderReq;
import com.lyncs.ods.resp.ListPageResp;
import com.lyncs.ods.resp.OrderDetailInfoResp;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 预约单总表 前端控制器
 * </p>
 *
 * @author alex
 * @since 2022-02-05
 */
@RestController
@RequestMapping("/txn/order")
@ApiModel("交易相关API")
@Api(tags = {"订单相关接口"})
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("save")
    @ApiOperation(value = "创建/修改 订单")
    public CommonResult<?> saveOrder(@Valid @RequestBody SaveOrderReq req) {
        orderService.saveOrder(req);
        return CommonResult.success();
    }

    @PostMapping("list")
    @ApiOperation(value = "获取 订单列表")
    public CommonResult<ListPageResp> listOrder(@RequestBody ListPageSearchReq req) {
        return CommonResult.success(orderService.listOrder(req));
    }

    @PostMapping("{orderId}/detail")
    @ApiOperation(value = "获取 订单详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_id", value = "订单ID", dataType = "Long", paramType = "path"),
    })
    public CommonResult<OrderDetailInfoResp> getOrderDetail(@PathVariable Long orderId) {
        return CommonResult.success(orderService.getOrderDetail(orderId));
    }

    @PostMapping("{orderId}/top")
    @ApiOperation(value = "置顶/取消置顶")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_id", value = "订单ID", dataType = "Long", paramType = "path"),
            @ApiImplicitParam(name = "type", value = "操作类型:0.取消置顶;1.置顶", dataType = "Integer", paramType = "query"),
    })
    public CommonResult<?> stickyOnTop(@PathVariable Long orderId, @RequestParam Integer type) {
        orderService.stickyOnTop(orderId, type);
        return CommonResult.success();
    }

    @GetMapping("{orderId}/reverse")
    @ApiOperation(value = "撤销订单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_id", value = "订单ID", dataType = "Long", paramType = "path"),
    })
    public CommonResult<?> reverseOrder(@PathVariable Long orderId) {
        orderService.reverseOrder(orderId);
        return CommonResult.success();
    }
}

