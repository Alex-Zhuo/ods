package com.lyncs.ods.modules.txn.service;

import com.lyncs.ods.modules.txn.model.SettlementInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 结算单总表 服务类
 * </p>
 *
 * @author alex
 * @since 2022-03-10
 */
public interface SettlementInfoService extends IService<SettlementInfo> {

}
