package com.lyncs.ods.modules.txn.service;

import com.lyncs.ods.modules.txn.model.ReverseInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 撤销记录表 服务类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface ReverseInfoService extends IService<ReverseInfo> {

}
