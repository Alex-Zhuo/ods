package com.lyncs.ods.modules.txn.service.impl;

import com.lyncs.ods.modules.txn.model.DeliverInfo;
import com.lyncs.ods.modules.txn.mapper.DeliverInfoMapper;
import com.lyncs.ods.modules.txn.service.DeliverInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 交付单总表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
@Service
public class DeliverInfoServiceImpl extends ServiceImpl<DeliverInfoMapper, DeliverInfo> implements DeliverInfoService {

}
