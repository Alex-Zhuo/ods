package com.lyncs.ods.modules.txn.service;

import com.lyncs.ods.req.ListPageSearchReq;
import com.lyncs.ods.req.SaveDeliverReq;
import com.lyncs.ods.resp.DeliverDetailInfoResp;
import com.lyncs.ods.resp.DeliverPageResp;
import com.lyncs.ods.resp.ListPageResp;

/**
 * @author alex
 * @date 2022/2/26 18:46
 * @description
 */
public interface DeliverService {
    /**
     * 保存/修改 交付单
     *
     * @param req request info
     */
    void saveDeliver(SaveDeliverReq req);

    /**
     * search deliver info by page
     *
     * @param req request info
     * @return deliver info
     */
    ListPageResp listDeliver(ListPageSearchReq req);

    /**
     * get deliver detail info
     *
     * @param deliverId deliver id
     * @return deliver detail info
     */
    DeliverDetailInfoResp getDeliverDetail(Long deliverId);

    /**
     * reverse deliver
     *
     * @param deliverId deliver id
     */
    void reverseDeliver(Long deliverId);

    /**
     * 交付单 置顶/取消
     *
     * @param deliverId deliverId
     * @param type      type
     */
    void stickyOnTop(Long deliverId, Integer type);
}
