package com.lyncs.ods.modules.txn.controller;


import com.lyncs.ods.common.api.CommonResult;
import com.lyncs.ods.modules.txn.service.SettlementService;
import com.lyncs.ods.req.ListPageSearchReq;
import com.lyncs.ods.req.SaveOrderReq;
import com.lyncs.ods.req.SaveSettlementReq;
import com.lyncs.ods.resp.ListPageResp;
import com.lyncs.ods.resp.OrderDetailInfoResp;
import com.lyncs.ods.resp.SettlementDetailInfoResp;
import com.lyncs.ods.resp.SettlementPageResp;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 结算单详情 前端控制器
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
@RestController
@RequestMapping("/txn/settlement")
@Api(tags = {"结算相关接口"})
public class SettlementController {

    @Autowired
    private SettlementService settlementService;

    @PostMapping("save")
    @ApiOperation(value = "创建/修改 结算单")
    public CommonResult<?> saveSettlement(@Valid @RequestBody SaveSettlementReq req) {
        settlementService.saveSettlement(req);
        return CommonResult.success();
    }

    @PostMapping("list")
    @ApiOperation(value = "获取 结算单列表")
    public CommonResult<SettlementPageResp> listSettlement(@RequestBody ListPageSearchReq req) {
        return CommonResult.success(settlementService.listSettlement(req));
    }

    @PostMapping("{settlementId}/detail")
    @ApiOperation(value = "获取 结算单详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "settlement_id", value = "结算单ID", dataType = "Long", paramType = "path"),
    })
    public CommonResult<SettlementDetailInfoResp> getSettlementDetail(@PathVariable Long settlementId) {
        return CommonResult.success(settlementService.getSettlementDetail(settlementId));
    }

    @PostMapping("{settlementId}/top")
    @ApiOperation(value = "置顶/取消置顶")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "settlement_id", value = "结算单ID", dataType = "Long", paramType = "path"),
            @ApiImplicitParam(name = "type", value = "操作类型:0.取消置顶;1.置顶", dataType = "Integer", paramType = "query"),
    })
    public CommonResult<?> stickyOnTop(@PathVariable Long settlementId, @RequestParam Integer type) {
        settlementService.stickyOnTop(settlementId, type);
        return CommonResult.success();
    }

    @GetMapping("{settlementId}/reverse")
    @ApiOperation(value = "撤销结算单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "settlement_id", value = "结算单ID", dataType = "Long", paramType = "path"),
    })
    public CommonResult<?> reverseSettlement(@PathVariable Long settlementId) {
        settlementService.reverseSettlement(settlementId);
        return CommonResult.success();
    }

    @GetMapping("{settlementId}/status")
    @ApiOperation(value = "结算单状态流转")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "settlement_id", value = "结算单ID", dataType = "Long", paramType = "path"),
            @ApiImplicitParam(name = "status", value = "要更改的状态。1.已出账单;2.发起结算;3.确认结算", dataType = "Integer", paramType = "query"),
    })
    public CommonResult<?> updateStatus(@PathVariable Long settlementId, @RequestParam("status") Integer status) {
        return CommonResult.success();
    }
}

