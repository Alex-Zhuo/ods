package com.lyncs.ods.modules.txn.service;

import com.lyncs.ods.modules.txn.model.TxnFeeDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 款项详情表 服务类
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
public interface TxnFeeDetailService extends IService<TxnFeeDetail> {

}
