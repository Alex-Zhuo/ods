package com.lyncs.ods.modules.txn.service.impl;

import com.lyncs.ods.modules.txn.model.ReverseInfo;
import com.lyncs.ods.modules.txn.mapper.ReverseInfoMapper;
import com.lyncs.ods.modules.txn.service.ReverseInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 撤销记录表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
@Service
public class ReverseInfoServiceImpl extends ServiceImpl<ReverseInfoMapper, ReverseInfo> implements ReverseInfoService {

}
