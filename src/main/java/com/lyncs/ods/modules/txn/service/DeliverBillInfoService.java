package com.lyncs.ods.modules.txn.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.lyncs.ods.modules.txn.model.DeliverBillInfo;

/**
 * <p>
 * 账款信息表 服务类
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
public interface DeliverBillInfoService extends IService<DeliverBillInfo> {

}
