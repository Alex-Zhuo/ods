package com.lyncs.ods.modules.txn.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.lyncs.ods.common.web.RequestHolder;
import com.lyncs.ods.constant.LyncsOdsConstant;
import com.lyncs.ods.modules.txn.model.TxnEditLog;
import com.lyncs.ods.modules.txn.mapper.TxnEditLogMapper;
import com.lyncs.ods.modules.txn.service.TxnEditLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.swagger.annotations.ApiModelProperty;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * ods编辑历史记录 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
@Service
@Slf4j
public class TxnEditLogServiceImpl extends ServiceImpl<TxnEditLogMapper, TxnEditLog> implements TxnEditLogService {
    @Override
    public <T> void saveLog(T oldEntity, T newEntity, Integer type, Long refId) {
        Integer operateType = Objects.isNull(oldEntity) ? LyncsOdsConstant.TxnEditType.ADD.getKey() : Objects.isNull(newEntity) ? LyncsOdsConstant.TxnEditType.DELETE.getKey() : LyncsOdsConstant.TxnEditType.EDIT.getKey();
        JSONObject oldContent = getModInfos(oldEntity);
        JSONObject newContent = getModInfos(newEntity);
        TxnEditLog txnEditLog = new TxnEditLog()
                //.setId(IdUtil.getSnowflakeNextId())
                .setType(type)
                .setRefId(refId)
                .setOperator(RequestHolder.getUserId())
                .setOldContent(oldContent.toString())
                .setNewContent(newContent.toString())
                .setOperateType(operateType)
                .setOperatorCompanyId(RequestHolder.getCompanyId())
                .setCreateTime(RequestHolder.getRequestDateTime());
        this.save(txnEditLog);
    }

    @Override
    public <T> void saveSkuLog(List<T> oldSkus, List<T> newSkus, Integer type, Long refId) {
        Integer operateType = Objects.isNull(oldSkus) ? LyncsOdsConstant.TxnEditType.ADD.getKey() : Objects.isNull(newSkus) ? LyncsOdsConstant.TxnEditType.DELETE.getKey() : LyncsOdsConstant.TxnEditType.EDIT.getKey();
        JSONArray oldContent = getModInfos(oldSkus);
        JSONArray newContent = getModInfos(newSkus);
        TxnEditLog txnEditLog = new TxnEditLog()
                .setId(IdUtil.getSnowflakeNextId())
                .setType(type)
                .setRefId(refId)
                .setOperator(RequestHolder.getUserId())
                .setOperatorCompanyId(RequestHolder.getCompanyId())
                .setOperateType(operateType)
                .setOldContent(oldContent.toString())
                .setNewContent(newContent.toString())
                .setCreateTime(RequestHolder.getRequestDateTime());
        this.save(txnEditLog);
    }

    private <T> JSONObject getModInfos(T entity) {
        JSONObject info = new JSONObject();
        if (Objects.isNull(entity)) {
            return info;
        }
        Field[] fields = entity.getClass().getDeclaredFields();
        if (ArrayUtil.isEmpty(fields)) {
            return info;
        }
        try {
            for (Field field : fields) {
                if (!field.canAccess(this)) {
                    field.trySetAccessible();
                }
                Object fieldVal = field.get(this);
                String annotationVal = null;
                boolean annotationPresent = field.isAnnotationPresent(ApiModelProperty.class);
                if (annotationPresent) {
                    ApiModelProperty annotation = field.getAnnotation(ApiModelProperty.class);
                    // 获取注解值
                    annotationVal = annotation.value();
                }
                if (StringUtils.isEmpty(annotationVal)) {
                    log.info("get annotation value failed:{}", field);
                    continue;
                }
                info.putOnce(annotationVal, fieldVal);
            }
        } catch (Exception e) {
            log.info("get annotation value failed,", e);
        }
        return info;
    }

    private <T> JSONArray getModInfos(List<T> list) {
        JSONArray arr = new JSONArray();
        if (CollectionUtil.isEmpty(list)) {
            return arr;
        }
        list.forEach(entity -> {
            JSONObject jsonObject = getModInfos(entity);
            if (!jsonObject.isEmpty()) {
                arr.add(jsonObject);
            }
        });
        return arr;
    }
}
