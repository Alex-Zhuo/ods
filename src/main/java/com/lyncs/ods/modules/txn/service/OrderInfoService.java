package com.lyncs.ods.modules.txn.service;

import com.lyncs.ods.modules.txn.model.OrderInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 预约单总表 服务类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface OrderInfoService extends IService<OrderInfo> {

}
