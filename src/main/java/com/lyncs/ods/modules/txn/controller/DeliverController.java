package com.lyncs.ods.modules.txn.controller;


import com.lyncs.ods.common.api.CommonResult;
import com.lyncs.ods.modules.txn.service.DeliverService;
import com.lyncs.ods.req.ListPageSearchReq;
import com.lyncs.ods.req.SaveDeliverReq;
import com.lyncs.ods.resp.DeliverDetailInfoResp;
import com.lyncs.ods.resp.DeliverPageResp;
import com.lyncs.ods.resp.ListPageResp;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 交付单总表 前端控制器
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
@RestController
@RequestMapping("/txn/deliver")
@Api(tags = {"交付相关接口"})
public class DeliverController {


    @Autowired
    private DeliverService deliverService;

    @PostMapping("save")
    @ApiOperation(value = "创建/修改 交付单")
    public CommonResult<?> saveDeliver(@Valid @RequestBody SaveDeliverReq req) {
        deliverService.saveDeliver(req);
        return CommonResult.success();
    }

    @PostMapping("list")
    @ApiOperation(value = "查询 交付单列表")
    public CommonResult<ListPageResp> listDeliver(@RequestBody ListPageSearchReq req) {
        return CommonResult.success(deliverService.listDeliver(req));
    }

    @PostMapping("{deliverId}/detail")
    @ApiOperation(value = "查询 交付单详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "deliver_id", value = "交付单ID", dataType = "Long", paramType = "path"),
    })
    public CommonResult<DeliverDetailInfoResp> getDeliverDetail(@PathVariable Long deliverId) {
        return CommonResult.success(deliverService.getDeliverDetail(deliverId));
    }

    @PostMapping("{deliverId}/top")
    @ApiOperation(value = "置顶/取消置顶")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "deliver_id", value = "交付单ID", dataType = "Long", paramType = "path"),
            @ApiImplicitParam(name = "type", value = "操作类型:0.取消置顶;1.置顶", dataType = "Integer", paramType = "query"),
    })
    public CommonResult<?> stickyOnTop(@PathVariable Long deliverId, @RequestParam Integer type) {
        deliverService.stickyOnTop(deliverId, type);
        return CommonResult.success();
    }

    @GetMapping("{deliverId}/reverse")
    @ApiOperation(value = "撤销订单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "deliver_id", value = "交付单ID", dataType = "Long", paramType = "path"),
    })
    public CommonResult<?> reverseDeliver(@PathVariable Long deliverId) {
        deliverService.reverseDeliver(deliverId);
        return CommonResult.success();
    }

}

