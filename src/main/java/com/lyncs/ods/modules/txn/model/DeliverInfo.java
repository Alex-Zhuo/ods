package com.lyncs.ods.modules.txn.model;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 交付单总表
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("deliver_info")
@ApiModel(value = "DeliverInfo对象", description = "交付单总表")
public class DeliverInfo implements Serializable, TxnCommonInfo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("交付单id")
    @TableId(value = "id")
    private Long id;

    @ApiModelProperty("卖方企业id")
    @TableField("seller_id")
    private Long sellerId;

    @ApiModelProperty("买方企业id")
    @TableField("buyer_id")
    private Long buyerId;

    @ApiModelProperty("sku类别数")
    @TableField("category_count")
    private Integer categoryCount;

    @ApiModelProperty("货币")
    @TableField("currency")
    private String currency;

    @ApiModelProperty("交付创建时间")
    @TableField("start_time")
    private LocalDateTime startTime;

    @ApiModelProperty("状态：0.草稿；1.已创建；2.部分结算；3.全部结算")
    @TableField("status")
    private Integer status;

    @ApiModelProperty("备注")
    @TableField("remark")
    private String remark;

    @ApiModelProperty("创建者用户id")
    @TableField("creator")
    private Long creator;

    @ApiModelProperty("创建者所属企业ID")
    @TableField("creator_company_id")
    private Long creatorCompanyId;

    @ApiModelProperty("最后更新用户id")
    @TableField("last_updater")
    private Long lastUpdater;

    @ApiModelProperty("创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


}
