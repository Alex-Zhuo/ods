package com.lyncs.ods.modules.txn.service;

import com.lyncs.ods.modules.txn.model.TxnSkuDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * SKU交易详情 服务类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface TxnSkuDetailService extends IService<TxnSkuDetail> {

}
