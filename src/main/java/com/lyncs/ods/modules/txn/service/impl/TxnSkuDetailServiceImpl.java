package com.lyncs.ods.modules.txn.service.impl;

import com.lyncs.ods.modules.txn.model.TxnSkuDetail;
import com.lyncs.ods.modules.txn.mapper.TxnSkuDetailMapper;
import com.lyncs.ods.modules.txn.service.TxnSkuDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * SKU交易详情 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
@Service
public class TxnSkuDetailServiceImpl extends ServiceImpl<TxnSkuDetailMapper, TxnSkuDetail> implements TxnSkuDetailService {

}
