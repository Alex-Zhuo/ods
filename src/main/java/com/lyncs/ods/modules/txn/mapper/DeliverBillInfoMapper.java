package com.lyncs.ods.modules.txn.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lyncs.ods.modules.txn.model.DeliverBillInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 账款信息表 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
@Mapper
public interface DeliverBillInfoMapper extends BaseMapper<DeliverBillInfo> {

}
