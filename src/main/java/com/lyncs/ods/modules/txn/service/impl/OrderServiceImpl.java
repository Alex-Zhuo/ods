package com.lyncs.ods.modules.txn.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.google.common.collect.Sets;
import com.lyncs.ods.common.exception.ApiException;
import com.lyncs.ods.common.web.RequestHolder;
import com.lyncs.ods.constant.LyncsOdsConstant;
import com.lyncs.ods.modules.goods.model.SkuDetail;
import com.lyncs.ods.modules.goods.model.SkuInfo;
import com.lyncs.ods.modules.goods.service.SkuDetailService;
import com.lyncs.ods.modules.goods.service.SkuInfoService;
import com.lyncs.ods.modules.txn.model.OrderInfo;
import com.lyncs.ods.modules.txn.model.ReverseInfo;
import com.lyncs.ods.modules.txn.model.TxnOperateInfo;
import com.lyncs.ods.modules.txn.model.TxnSkuDetail;
import com.lyncs.ods.modules.txn.service.*;
import com.lyncs.ods.modules.company.service.CompanyContactInfoService;
import com.lyncs.ods.modules.company.service.CompanyInfoService;
import com.lyncs.ods.modules.user.service.CompanyUserRelationService;
import com.lyncs.ods.req.ListPageSearchReq;
import com.lyncs.ods.req.SaveOrderReq;
import com.lyncs.ods.resp.ListPageResp;
import com.lyncs.ods.resp.OrderDetailInfoResp;
import com.lyncs.ods.utils.ParamCheckUtil;
import com.lyncs.ods.utils.TxnCommonUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author alex
 * @date 2022/2/5 22:45
 * @description
 */
@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderInfoService orderInfoService;
    @Autowired
    private TxnSkuDetailService txnSkuDetailService;
    @Autowired
    private ReverseInfoService reverseInfoService;
    @Autowired
    private SkuInfoService skuInfoService;
    @Autowired
    private CompanyContactInfoService companyContactInfoService;
    @Autowired
    private CompanyInfoService companyInfoService;
    @Autowired
    private TxnEditLogService txnEditLogService;
    @Autowired
    private CompanyUserRelationService companyUserRelationService;
    @Autowired
    private SkuDetailService skuDetailService;
    @Autowired
    private TxnOperateInfoService txnOperateInfoService;

    @Override
    public void saveOrder(SaveOrderReq req) {
        OrderInfo dbOrder = null;
        OrderInfo orderInfo = new OrderInfo();
        ParamCheckUtil.checkTxnOptions(req);
        if (req.getOrderId() == null) {
            //视为新增
            orderInfo = new OrderInfo().setId(IdUtil.getSnowflakeNextId())
                    .setSellerId(req.getSellerId())
                    //.setSellerPartnerId(req.getSellerPartnerId())
                    .setBuyerId(req.getBuyerId())
                    //.setBuyerPartnerId(req.getBuyerPartnerId())
                    .setStartTime(RequestHolder.getRequestDateTime())
                    .setCreateTime(LocalDateTime.now())
                    .setCreator(RequestHolder.getUserId())
                    .setCreatorCompanyId(RequestHolder.getCompanyId());
        } else {
            //表示修改
            dbOrder = orderInfoService.lambdaQuery().eq(OrderInfo::getId, req.getOrderId()).one();
            if (Objects.isNull(dbOrder)) {
                throw new ApiException("can not update order info for not exist or disabled.");
            }
            BeanUtils.copyProperties(dbOrder, orderInfo);
        }

        if (req.getOperateType() == 1) {
            orderInfo.setStatus(LyncsOdsConstant.OrderStatus.DRAFT.getKey());
        } else {
            orderInfo.setStatus(LyncsOdsConstant.OrderStatus.INIT.getKey());
        }
        orderInfo.setInnerCode(req.getInnerCode())
                .setEstimatedDeliveryTime(req.getEstimatedDeliveryTime())
                .setCategoryCount(req.getSkuInfoList().size())
                .setRemark(req.getRemark())
                .setLastUpdater(RequestHolder.getUserId())
                .setUpdateTime(LocalDateTime.now());
        saveSku(req, orderInfo);
        orderInfoService.saveOrUpdate(orderInfo);
        txnEditLogService.saveLog(dbOrder, orderInfo, LyncsOdsConstant.TxnType.ORDER.getKey(), orderInfo.getId());
    }

    private void saveSku(SaveOrderReq req, OrderInfo orderInfo) {
        List<TxnSkuDetail> details = new ArrayList<>();
        if (req.getOrderId() != null) {
            details = txnSkuDetailService.lambdaQuery().eq(TxnSkuDetail::getOrderId, orderInfo.getId()).list();
        }
        Triple<List<TxnSkuDetail>, List<Long>, List<TxnSkuDetail>> orderDiffSkuTriple = TxnCommonUtils.getOrderDiffSkus(orderInfo.getId(), req.getSkuInfoList(), details);
        txnSkuDetailService.saveBatch(orderDiffSkuTriple.getLeft());
        txnSkuDetailService.updateBatchById(orderDiffSkuTriple.getRight());
        txnSkuDetailService.removeByIds(orderDiffSkuTriple.getMiddle());
        //txnEditLogService.saveSkuLog(null, orderDiffSkuTriple.getLeft(), LyncsOdsConstant.TxnType.ORDER.getKey(), orderInfo.getId());
    }


    /**
     * 删除订单
     *
     * @param orderId orderId
     */
    @Override
    public void reverseOrder(Long orderId) {
        OrderInfo orderInfo = orderInfoService.lambdaQuery().eq(OrderInfo::getId, orderId).one();
        if (Objects.isNull(orderInfo)) {
            throw new ApiException("cannot find order of:" + orderId);
        }
        List<TxnSkuDetail> orderDetails = txnSkuDetailService.lambdaQuery()
                .eq(TxnSkuDetail::getOrderId, orderId)
                .isNull(TxnSkuDetail::getDeliverId)
                .list();
        ReverseInfo reverseInfo = new ReverseInfo();
        reverseInfo.setId(IdUtil.getSnowflakeNextId())
                .setType(LyncsOdsConstant.TxnType.ORDER.getKey())
                .setReverseInfo(JSONUtil.toJsonStr(orderInfo))
                .setReverseSubInfo(JSONUtil.toJsonStr(orderDetails));
        reverseInfoService.save(reverseInfo);
        orderInfoService.removeById(orderInfo.getId());
        txnSkuDetailService.removeByIds(orderDetails.stream().map(TxnSkuDetail::getId).collect(Collectors.toList()));
        txnEditLogService.saveLog(orderInfo, null, LyncsOdsConstant.TxnType.ORDER.getKey(), orderInfo.getId());
    }

    @Override
    public ListPageResp listOrder(ListPageSearchReq req) {
        //FIXME 置顶单据
        List<String> companyIds = StringUtils.isBlank(req.getFilterCompanyId()) ? null : Arrays.asList(StringUtils.split(req.getFilterCompanyId(), LyncsOdsConstant.Common.COMMA));
        List<String> status = StringUtils.isBlank(req.getStatus()) ? null : Arrays.asList(StringUtils.split(req.getStatus(), LyncsOdsConstant.Common.COMMA));
        LambdaQueryChainWrapper<OrderInfo> wrapper = orderInfoService.lambdaQuery()
                //查询我发出的
                .eq(LyncsOdsConstant.ContactType.SUPPLIER.getKey().equals(req.getType()), OrderInfo::getSellerId, RequestHolder.getCompanyId())
                //查询我收到的
                .eq(LyncsOdsConstant.ContactType.CUSTOMER.getKey().equals(req.getType()), OrderInfo::getBuyerId, RequestHolder.getCompanyId())
                .in(LyncsOdsConstant.ContactType.SUPPLIER.getKey().equals(req.getType()) && CollectionUtil.isNotEmpty(companyIds), OrderInfo::getBuyerId, companyIds)
                .in(LyncsOdsConstant.ContactType.CUSTOMER.getKey().equals(req.getType()) && CollectionUtil.isNotEmpty(companyIds), OrderInfo::getSellerId, companyIds)
                .ge(StringUtils.isNotEmpty(req.getStartTime()), OrderInfo::getStartTime, req.getStartTime())
                .le(StringUtils.isNotEmpty(req.getEndTime()), OrderInfo::getStartTime, req.getEndTime())
                .in(CollectionUtil.isNotEmpty(status), OrderInfo::getStatus, status);

        Integer totalCount = wrapper.count();
        List<OrderInfo> orderInfoList = wrapper.last(req.getLimitSql()).list();
        List<Long> orderIds = orderInfoList.stream().map(OrderInfo::getId).collect(Collectors.toList());
        return TxnCommonUtils.getListPageRespData(
                req,
                totalCount,
                orderInfoList,
                txnSkuDetailService.lambdaQuery().isNull(TxnSkuDetail::getDeliverId).in(TxnSkuDetail::getOrderId, orderIds).list(),
                skuInfoService,
                companyInfoService,
                companyContactInfoService,
                companyUserRelationService
        );
    }

    /**
     * get order detail info
     *
     * @param orderId orderId
     * @return order detail info
     */
    @Override
    public OrderDetailInfoResp getOrderDetail(Long orderId) {
        OrderInfo orderInfo = orderInfoService.lambdaQuery().eq(OrderInfo::getId, orderId).one();
        if (Objects.isNull(orderInfo)) {
            throw new ApiException("can not get order info for not exist or disabled.");
        }

        Pair<String, String> companyNameMap = TxnCommonUtils.getCompanyName(companyInfoService, companyContactInfoService, orderInfo);

        List<TxnSkuDetail> txnSkuDetails = txnSkuDetailService.lambdaQuery().eq(TxnSkuDetail::getOrderId, orderId).list();
        Set<Long> skuIds = txnSkuDetails.stream().map(TxnSkuDetail::getSkuId).collect(Collectors.toSet());
        List<SkuInfo> skuInfos = skuInfoService.lambdaQuery().eq(SkuInfo::getCompanyId, orderInfo.getCreatorCompanyId()).in(SkuInfo::getId, skuIds).list();
        Map<Long, SkuInfo> skuInfoMap = skuInfos.stream().collect(Collectors.toMap(SkuInfo::getId, o -> o));

        List<SkuDetail> skuDetails = skuDetailService.lambdaQuery().in(SkuDetail::getSkuId, skuIds).list();
        Map<Long, Map<String, String>> skuAttrMap = skuDetails.stream().collect(Collectors.groupingBy(SkuDetail::getSkuId, Collectors.toMap(SkuDetail::getName, SkuDetail::getValue)));

        OrderDetailInfoResp resp = new OrderDetailInfoResp();
        resp.setOrderId(orderId)
                .setTotalAmount(txnSkuDetails.stream().map(TxnSkuDetail::getTotalAmount).reduce(BigDecimal.ZERO, BigDecimal::add).toPlainString())
                .setStatus(orderInfo.getStatus())
                .setCreateTime(orderInfo.getCreateTime())
                .setEstimatedDeliveryTime(orderInfo.getEstimatedDeliveryTime())
                .setInnerCode(orderInfo.getInnerCode())
                .setSellerName(companyNameMap.getRight())
                .setBuyerName(companyNameMap.getLeft())
                .setRemark(orderInfo.getRemark())
                .setCreatorName(TxnCommonUtils.getCreatorName(companyUserRelationService, orderInfo))
                .setUpdateTime(orderInfo.getUpdateTime());
        List<OrderDetailInfoResp.SkuInfo> orderSkus = txnSkuDetails.stream().map(txnSkuDetail -> {
            SkuInfo thisSkuInfo = skuInfoMap.get(txnSkuDetail.getSkuId());
            return new OrderDetailInfoResp.SkuInfo()
                    .setName(thisSkuInfo.getName())
                    .setTotalCount(txnSkuDetail.getTotalAmount())
                    .setDeliveredCount(txnSkuDetail.getDeliveredCount())
                    .setTotalAmount(txnSkuDetail.getTotalAmount().toPlainString())
                    .setAmount(txnSkuDetail.getAmount().toPlainString())
                    .setUnit(thisSkuInfo.getUnit())
                    .setAttrs(skuAttrMap.get(txnSkuDetail.getSkuId()));
        }).collect(Collectors.toList());
        resp.setSkuInfoList(orderSkus);
        return resp;
    }

    @Override
    public void stickyOnTop(Long orderId, Integer type) {
        txnOperateInfoService.save(new TxnOperateInfo().setType(LyncsOdsConstant.TxnType.ORDER.getKey()).setRefId(orderId).setCompanyId(RequestHolder.getCompanyId()).setCreatorId(RequestHolder.getUserId()).setOperateType(1).setStatus(LyncsOdsConstant.EnableStatus.DISABLE.getKey()));
    }
}
