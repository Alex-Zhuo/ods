package com.lyncs.ods.modules.txn.service.impl;

import com.lyncs.ods.modules.txn.model.TxnFeeDetail;
import com.lyncs.ods.modules.txn.mapper.TxnFeeDetailMapper;
import com.lyncs.ods.modules.txn.service.TxnFeeDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 款项详情表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
@Service
public class TxnFeeDetailServiceImpl extends ServiceImpl<TxnFeeDetailMapper, TxnFeeDetail> implements TxnFeeDetailService {

}
