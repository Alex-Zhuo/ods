package com.lyncs.ods.modules.txn.service.impl;

import com.lyncs.ods.modules.txn.model.OrderInfo;
import com.lyncs.ods.modules.txn.mapper.OrderInfoMapper;
import com.lyncs.ods.modules.txn.service.OrderInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 预约单总表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
@Service
public class OrderInfoServiceImpl extends ServiceImpl<OrderInfoMapper, OrderInfo> implements OrderInfoService {

}
