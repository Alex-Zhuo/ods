package com.lyncs.ods.modules.txn.model;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 撤销记录表
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("reverse_info")
@ApiModel(value = "ReverseInfo对象", description = "撤销记录表")
public class ReverseInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("自增主键id")
    @TableId(value = "id")
    private Long id;

    @ApiModelProperty("类型：1.order;2.deliver;3.settlement")
    @TableField("type")
    private Integer type;

    @ApiModelProperty("撤销信息")
    @TableField("reverse_info")
    private String reverseInfo;

    @ApiModelProperty("撤销关联信息（订单和交付关联sku/结算关联交付信息）")
    @TableField("reverse_sub_info")
    private String reverseSubInfo;

    @ApiModelProperty("创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


}
