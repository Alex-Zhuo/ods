package com.lyncs.ods.modules.txn.service.impl;

import com.lyncs.ods.modules.txn.model.SettlementInfo;
import com.lyncs.ods.modules.txn.mapper.SettlementInfoMapper;
import com.lyncs.ods.modules.txn.service.SettlementInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 结算单总表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-03-10
 */
@Service
public class SettlementInfoServiceImpl extends ServiceImpl<SettlementInfoMapper, SettlementInfo> implements SettlementInfoService {

}
