package com.lyncs.ods.modules.txn.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.lyncs.ods.common.web.RequestHolder;
import com.lyncs.ods.constant.LyncsOdsConstant;
import com.lyncs.ods.modules.company.service.CompanyContactInfoService;
import com.lyncs.ods.modules.company.service.CompanyInfoService;
import com.lyncs.ods.modules.txn.model.TxnOperateInfo;
import com.lyncs.ods.modules.txn.service.*;
import com.lyncs.ods.modules.user.service.CompanyUserRelationService;
import com.lyncs.ods.req.ListPageSearchReq;
import com.lyncs.ods.req.SaveSettlementReq;
import com.lyncs.ods.resp.SettlementDetailInfoResp;
import com.lyncs.ods.resp.SettlementPageResp;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author alex
 * @date 2022/3/10 14:19
 * @description
 */
@Service
@Slf4j
public class SettlementServiceImpl implements SettlementService {

    @Autowired
    private SettlementInfoService settlementInfoService;
    ///@Autowired
    // TxnBillInfoService txnBillInfoService;
    @Autowired
    private TxnFeeDetailService txnFeeDetailService;
    @Autowired
    private DeliverInfoService deliverInfoService;
    @Autowired
    private CompanyInfoService companyInfoService;
    @Autowired
    private CompanyContactInfoService companyContactInfoService;
    @Autowired
    private CompanyUserRelationService companyUserRelationService;
    @Autowired
    private ReverseInfoService reverseInfoService;
    @Autowired
    private TxnOperateInfoService txnOperateInfoService;

    @Override
    public void saveSettlement(SaveSettlementReq req) {
        /*ParamCheckUtil.checkTxnOptions(req);
        SettlementInfo dbSettlementInfo;
        SettlementInfo settlementInfo = new SettlementInfo();
        Pair<BigDecimal, BigDecimal> amountPair = getAmountPair(req);
        //List<TxnBillInfo> billInfos = new ArrayList<>();
        if (req.getSettlementId() == null) {
            //new
            settlementInfo.setId(IdUtil.getSnowflakeNextId())
                    .setSellerId(req.getSellerId())
                    .setBuyerId(req.getBuyerId())
                    //.setBuyerPartnerId(req.getBuyerPartnerId())
                    .setStartTime(RequestHolder.getRequestDateTime())
                    .setCreateTime(LocalDateTime.now())
                    .setCreator(RequestHolder.getUserId())
                    .setCreatorCompanyId(RequestHolder.getCompanyId());
        } else {
            //update
            dbSettlementInfo = settlementInfoService.lambdaQuery().eq(SettlementInfo::getId, req.getSettlementId()).one();
            if (Objects.isNull(dbSettlementInfo)) {
                throw new ApiException("can not update settlement info for not exist or disabled.");
            }
            BeanUtils.copyProperties(dbSettlementInfo, settlementInfo);
            settlementDetails = settlementDetailService.lambdaQuery().eq(SettlementDetail::getSettleId, req.getSettlementId()).list();
        }
        if (req.getOperateType() == 1) {
            settlementInfo.setStatus(LyncsOdsConstant.SettlementStatus.DRAFT.getKey());
        } else {
            settlementInfo.setStatus(LyncsOdsConstant.SettlementStatus.INIT.getKey());
        }
        Triple<List<SettlementDetail>, List<Long>, List<SettlementDetail>> settlementDiff = getSettlementDiff(settlementInfo.getId(), settlementDetails, req.getDeliverSettleInfoList());
        settlementInfo//.setTotalAmount(amountPair.getLeft())
                .setCategoryCount(req.getDeliverSettleInfoList().size())
                //.setPromotionAmount(amountPair.getRight())
                //.setPromotionInfos(CollectionUtil.isEmpty(req.getPromotionInfos()) ? null : JSONUtil.toJsonStr(req.getPromotionInfos()))
                .setRemark(req.getRemark())
                .setLastUpdater(RequestHolder.getUserId())
                .setDeadline(req.getDeadline())
                .setUpdateTime(LocalDateTime.now());
        settlementInfoService.saveOrUpdate(settlementInfo);
        settlementDetailService.saveBatch(settlementDiff.getLeft());
        settlementDetailService.removeByIds(settlementDiff.getMiddle());
        settlementDetailService.updateBatchById(settlementDiff.getRight());*/
        //log
    }

    @Override
    public SettlementPageResp listSettlement(ListPageSearchReq req) {
        //todo 置顶单据
        /*Long zero = 0L;
        //不能同时查询作为买方和卖方的订单
        if (ObjectUtils.anyNotNull(req.getSellerId()) && ObjectUtils.anyNotNull(req.getBuyerId())) {
            throw new ApiException("Please choose your role: supplier/customer");
        }
        if (zero.equals(req.getSellerId()) && zero.equals(req.getBuyerId())) {
            throw new ApiException("Please choose your role: supplier/customer");
        }
        LambdaQueryChainWrapper<SettlementInfo> wrapper = settlementInfoService.lambdaQuery()
                .eq(req.getSellerId() != null && !zero.equals(req.getSellerId()), SettlementInfo::getSellerId, req.getSellerId())
                //.eq(req.getSellerPartnerId() != null, SettlementInfo::getSellerPartnerId, req.getSellerPartnerId())
                .eq(req.getBuyerId() != null && !zero.equals(req.getBuyerId()), SettlementInfo::getBuyerId, req.getBuyerId());
                //.eq(req.getBuyerPartnerId() != null, SettlementInfo::getBuyerPartnerId, req.getBuyerPartnerId());
        Integer totalCount = wrapper.count();
        List<SettlementInfo> settlementInfos = wrapper.list();
        if (CollectionUtil.isEmpty(settlementInfos)) {
            return null;
        }
        Set<Long> companyIds = Sets.newHashSet();
        Set<Long> partnerContactIds = Sets.newHashSet();
        settlementInfos.forEach(info -> {
            companyIds.add(info.getSellerId());
            companyIds.add(info.getBuyerId());
            *//*partnerContactIds.add(info.getBuyerPartnerId());
            partnerContactIds.add(info.getSellerPartnerId());*//*
        });
        Pair<Map<Long, String>, Map<Long, String>> namePair = TxnCommonUtils.getCompanyInfo(companyInfoService, companyContactInfoService, companyIds, partnerContactIds);
        //Map<String, String> creatorNameMap = TxnCommonUtils.getCreatorName(companyUserRelationService, settlementInfos);

        SettlementPageResp resp = new SettlementPageResp();
        resp.setPage(req.getPage())
                .setPageSize(req.getPageSize())
                .setTotalSize(totalCount);
        resp.setData(settlementInfos.stream().map(settlementInfo -> {
            SettlementPageResp.ShortInfo shortInfo = new SettlementPageResp.ShortInfo();
            shortInfo.setId(settlementInfo.getId())
                    //.setCreatorName(creatorNameMap.get(stringutils.join(settlementInfo.getCreateCompanyId(), OdsConstant.WELL, settlementInfo.getCreator())))
                    .setCreateTime(settlementInfo.getCreateTime())
                    .setUpdateTime(settlementInfo.getUpdateTime())
                    .setSellerId(settlementInfo.getSellerId())
                    .setSellerName(TxnCommonUtils.getCompanyName(namePair.getLeft(), namePair.getRight(), settlementInfo.getSellerId(), null))
                    .setBuyerId(settlementInfo.getBuyerId())
                    .setBuyerName(TxnCommonUtils.getCompanyName(namePair.getLeft(), namePair.getRight(), settlementInfo.getBuyerId(), null))
                    //.setCategoryCount(settlementInfo.getCategoryCount())
                    //.setTotalAmount(settlementInfo.getTotalAmount().toPlainString())
                    .setStatus(settlementInfo.getStatus())
                    .setCreateCompanyId(settlementInfo.getCreatorCompanyId())
            ;
            return shortInfo;
        }).collect(Collectors.toList()));
        return resp;*/
        return null;
    }

    @Override
    public SettlementDetailInfoResp getSettlementDetail(Long settlementId) {
        /*SettlementInfo settlementInfo = settlementInfoService.lambdaQuery().eq(SettlementInfo::getId, settlementId).one();
        if (Objects.isNull(settlementInfo)) {
            throw new ApiException("can not get settlement info for not exist or disabled.");
        }
        //Pair<String, String> companyNameMap = TxnCommonUtils.getCompanyName(companyInfoService, companyContactInfoService, settlementInfo);

        List<SettlementDetail> settlementDetails = settlementDetailService.lambdaQuery().in(SettlementDetail::getSettleId, settlementId).list();
        //List<Long> deliverIds = settlementDetails.stream().map(SettlementDetail::getDeliverId).distinct().collect(Collectors.toList());
        //Map<Long, DeliverInfo> deliverInfoMap = deliverInfoService.lambdaQuery().in(DeliverInfo::getId, deliverIds).list().stream().collect(Collectors.toMap(DeliverInfo::getId, o -> o));

        SettlementDetailInfoResp resp = new SettlementDetailInfoResp();
        resp.setSettlementId(settlementInfo.getId())
                //.setTotalAmount(settlementInfo.getTotalAmount().toPlainString())
                .setStatus(settlementInfo.getStatus())
                //.setSellerName(companyNameMap.getRight())
                //.setBuyerName(companyNameMap.getLeft())
                .setCreateTime(settlementInfo.getCreateTime())
                *//*.setPromotionInfos(JSONArray.parseArray(settlementInfo.getPromotionInfos()))*//*;
        List<SettlementDetailInfoResp.SettlementDeliverInfo> settlementDeliverInfos = settlementDetails.stream().map(settlementDetail -> {
            SettlementDetailInfoResp.SettlementDeliverInfo settlementDeliverInfo = new SettlementDetailInfoResp.SettlementDeliverInfo();
            settlementDeliverInfo//.setDeliverId(settlementDetail.getDeliverId())
                    //.setSettleAmount(settlementDetail.getSettleAmount())
                    .setCreateTime(settlementDetail.getCreateTime());
                    //.setOtherFee(JSONObject.parseObject(deliverInfoMap.get(settlementDetail.getDeliverId()).getOtherFee()));
            return settlementDeliverInfo;
        }).collect(Collectors.toList());
        resp.setDeliverInfoList(settlementDeliverInfos);
        return resp;*/
        return null;
    }

    @Override
    public void reverseSettlement(Long settlementId) {
        /*SettlementInfo settlementInfo = settlementInfoService.lambdaQuery().eq(SettlementInfo::getId, settlementId).one();
        if (Objects.isNull(settlementInfo)) {
            throw new ApiException("can not get settlement info for not exist or disabled.");
        }
        List<SettlementDetail> settlementDetails = settlementDetailService.lambdaQuery().in(SettlementDetail::getSettleId, settlementId).list();
        ReverseInfo reverseInfo = new ReverseInfo();
        reverseInfo.setId(IdUtil.getSnowflakeNextId())
                .setType(LyncsOdsConstant.TxnType.SETTLEMENT.getKey())
                .setReverseInfo(JSONUtil.toJsonStr(settlementInfo))
                .setReverseSubInfo(JSONUtil.toJsonStr(settlementDetails));
        reverseInfoService.save(reverseInfo);
        settlementInfoService.removeById(settlementInfo.getId());
        settlementDetailService.removeByIds(settlementDetails.stream().map(SettlementDetail::getId).collect(Collectors.toList()));*/
    }

    @Override
    public void stickyOnTop(Long settlementId, Integer type) {
        txnOperateInfoService.save(new TxnOperateInfo().setType(LyncsOdsConstant.TxnType.SETTLEMENT.getKey()).setRefId(settlementId).setCompanyId(RequestHolder.getCompanyId()).setCreatorId(RequestHolder.getUserId()).setOperateType(1).setStatus(LyncsOdsConstant.EnableStatus.DISABLE.getKey()));
    }

    private Triple<List<?>, List<Long>, List<?>> getSettlementDiff(Long settlementId, List<?> existDetails, List<SaveSettlementReq.DeliverSettleInfo> reqDetail) {
        List<?> insertList = new ArrayList<>();
        List<Long> delList = new ArrayList<>();
        List<?> updateList = new ArrayList<>();

        /*Map<Long, SaveSettlementReq.DeliverSettleInfo> reqMap = reqDetail.stream().collect(Collectors.toMap(SaveSettlementReq.DeliverSettleInfo::getDeliverId, o -> o));
        Map<Long, SettlementDetail> existMap = existDetails.stream().collect(Collectors.toMap(SettlementDetail::getDeliverId, o -> o));
        List<Long> reqDelIds = new ArrayList<>(reqMap.keySet());
        List<Long> existDelIds = new ArrayList<>(existMap.keySet());
        List<Long> allDelIds = CollectionUtil.addAllIfNotContains(reqDelIds, existDelIds);
        for (Long delId : allDelIds) {
            SaveSettlementReq.DeliverSettleInfo reqDeliverSettleInfo = reqMap.get(delId);
            SettlementDetail existDeliverSettleInfo = existMap.get(delId);
            if (existDeliverSettleInfo != null && reqDeliverSettleInfo != null) {
                //update
                updateList.add(
                        existDeliverSettleInfo
                                .setSettleAmount(new BigDecimal(reqDeliverSettleInfo.getAmount()))
                                .setSettleTime(RequestHolder.getRequestDateTime())
                                .setStatus(OdsConstant.STATUS_INIT)
                                .setRemark(reqDeliverSettleInfo.getRemark())
                );
            } else if (existDeliverSettleInfo != null) {
                //delete
                delList.add(existDeliverSettleInfo.getId());
            } else {
                //insert
                insertList.add(
                        new SettlementDetail()
                                .setId(IdUtil.getSnowflakeNextId())
                                .setSettleId(settlementId)
                                .setDeliverId(reqDeliverSettleInfo.getDeliverId())
                                .setSettleAmount(new BigDecimal(reqDeliverSettleInfo.getAmount()))
                                .setSettleTime(RequestHolder.getRequestDateTime())
                                .setStatus(OdsConstant.STATUS_INIT)
                                .setRemark(reqDeliverSettleInfo.getRemark())
                                .setCreateTime(LocalDateTime.now())
                );
            }
        }*/
        return Triple.of(insertList, delList, updateList);
    }

    private Pair<BigDecimal, BigDecimal> getAmountPair(SaveSettlementReq req) {
        BigDecimal promotionAmount = BigDecimal.ZERO;
        BigDecimal totalAmount = req.getDeliverSettleInfoList().stream().map(item -> new BigDecimal(item.getAmount())).reduce(BigDecimal.ZERO, BigDecimal::add);
        if (CollectionUtil.isNotEmpty(req.getPromotionInfos())) {
            promotionAmount = req.getPromotionInfos().stream().map(item -> new BigDecimal(item.getAmount())).reduce(BigDecimal.ZERO, BigDecimal::add);
        }
        return Pair.of(totalAmount, promotionAmount);
    }

}
