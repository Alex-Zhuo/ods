package com.lyncs.ods.modules.txn.model;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 账款信息表
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("deliver_bill_info")
@ApiModel(value = "DeliverBillInfo对象", description = "交付单账款信息表")
public class DeliverBillInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("自增主键id")
    @TableId(value = "id")
    private Long id;

    @ApiModelProperty("交付单id")
    @TableField("deliver_id")
    private Long deliverId;

    @ApiModelProperty("账款总金额(冗余字段，值为所有款项金额之和)")
    @TableField("total_amount")
    private BigDecimal totalAmount;

    @ApiModelProperty("已结算金额")
    @TableField("settled_amount")
    private BigDecimal settledAmount;

    @ApiModelProperty("货币")
    @TableField("currency")
    private String currency;

    @ApiModelProperty("结算时间")
    @TableField("start_time")
    private LocalDateTime startTime;

    @ApiModelProperty("状态：1.已创建;2.部分结算;3.已结算")
    @TableField("status")
    private Integer status;

    @ApiModelProperty("备注")
    @TableField("remark")
    private String remark;

    @ApiModelProperty("创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


}
