package com.lyncs.ods.modules.txn.mapper;

import com.lyncs.ods.modules.txn.model.DeliverInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 交付单总表 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface DeliverInfoMapper extends BaseMapper<DeliverInfo> {

}
