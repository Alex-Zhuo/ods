package com.lyncs.ods.modules.txn.model;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * ods操作信息
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("txn_operate_info")
@ApiModel(value = "TxnOperateInfo对象", description = "ods操作信息")
public class TxnOperateInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("自增主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("类型：1.order;2.deliver;3.settlement")
    @TableField("type")
    private Integer type;

    @ApiModelProperty("类型关联ID")
    @TableField("ref_id")
    private Long refId;

    @ApiModelProperty("企业ID")
    @TableField("company_id")
    private Long companyId;

    @ApiModelProperty("创建用户ID")
    @TableField("creator_id")
    private Long creatorId;

    @ApiModelProperty("操作类型:1.置顶")
    @TableField("operate_type")
    private Integer operateType;

    @ApiModelProperty("状态：1.可用；2.删除")
    @TableField("status")
    private Integer status;

    @ApiModelProperty("创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


}
