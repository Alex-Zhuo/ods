package com.lyncs.ods.modules.txn.model;

import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;

/**
 * @author alex
 * @date 2022/2/27 15:25
 * @description
 */
public interface TxnCommonInfo {

    /**
     * 订单/交付单 ID
     *
     * @return 订单/交付单 ID
     */
    Long getId();

    /**
     * 获取卖方公司ID
     *
     * @return 卖方公司ID
     */
    Long getSellerId();

    /**
     * 获取买方公司ID
     *
     * @return 买方公司ID
     */
    Long getBuyerId();

    /**
     * 获取创建者UID
     *
     * @return 创建者UID
     */
    Long getCreator();

    /**
     * 获取创建者CID
     *
     * @return 创建者CID
     */
    Long getCreatorCompanyId();

    /**
     * sku类别数
     *
     * @return sku类别数
     */
    Integer getCategoryCount();

    /**
     * 获取 交付单/订单 状态
     *
     * @return 交付单/订单 状态
     */
    Integer getStatus();

    /**
     * 获取 交付单/订单 开始时间
     *
     * @return 交付单/订单 开始时间
     */
    LocalDateTime getStartTime();

    /**
     * 获取 交付单/订单 最后更新时间
     *
     * @return 交付单/订单 最后更新时间
     */
    LocalDateTime getUpdateTime();
}
