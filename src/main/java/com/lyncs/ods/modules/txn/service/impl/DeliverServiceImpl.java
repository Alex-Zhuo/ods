package com.lyncs.ods.modules.txn.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.lyncs.ods.common.exception.ApiException;
import com.lyncs.ods.common.web.RequestHolder;
import com.lyncs.ods.constant.LyncsOdsConstant;
import com.lyncs.ods.modules.goods.model.SkuDetail;
import com.lyncs.ods.modules.goods.model.SkuInfo;
import com.lyncs.ods.modules.goods.service.SkuDetailService;
import com.lyncs.ods.modules.goods.service.SkuInfoService;
import com.lyncs.ods.modules.txn.model.*;
import com.lyncs.ods.modules.txn.service.*;
import com.lyncs.ods.modules.company.service.CompanyContactInfoService;
import com.lyncs.ods.modules.company.service.CompanyInfoService;
import com.lyncs.ods.modules.user.service.CompanyUserRelationService;
import com.lyncs.ods.req.ListPageSearchReq;
import com.lyncs.ods.req.SaveDeliverReq;
import com.lyncs.ods.resp.DeliverDetailInfoResp;
import com.lyncs.ods.resp.DeliverPageResp;
import com.lyncs.ods.resp.ListPageResp;
import com.lyncs.ods.resp.OrderDetailInfoResp;
import com.lyncs.ods.utils.ParamCheckUtil;
import com.lyncs.ods.utils.TxnCommonUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * @author alex
 * @date 2022/2/26 18:46
 * @description
 */
@Service
@Slf4j
public class DeliverServiceImpl implements DeliverService {

    @Autowired
    private DeliverInfoServiceImpl deliverInfoService;
    @Autowired
    private ReverseInfoService reverseInfoService;
    @Autowired
    private TxnSkuDetailService txnSkuDetailService;
    @Autowired
    private SkuInfoService skuInfoService;
    @Autowired
    private CompanyContactInfoService companyContactInfoService;
    @Autowired
    private CompanyInfoService companyInfoService;
    /* @Autowired
     private UserTxnLogService txnLogService;*/
    @Autowired
    private CompanyUserRelationService companyUserRelationService;
    @Autowired
    private SkuDetailService skuDetailService;
    @Autowired
    private TxnOperateInfoService txnOperateInfoService;
    @Autowired
    private TxnEditLogService txnEditLogService;
    @Autowired
    private OrderInfoService orderInfoService;
    @Autowired
    private DeliverBillInfoService deliverBillInfoService;
    @Autowired
    private TxnFeeDetailService txnFeeDetailService;

    @Override
    public void saveDeliver(SaveDeliverReq req) {
        ParamCheckUtil.checkTxnOptions(req);
        DeliverInfo dbDeliver = null;
        DeliverInfo deliverInfo = new DeliverInfo();
        if (req.getDeliverId() == null) {
            //insert
            deliverInfo = new DeliverInfo().setId(IdUtil.getSnowflakeNextId())
                    .setSellerId(req.getSellerId())
                    .setBuyerId(req.getBuyerId())
                    .setStartTime(RequestHolder.getRequestDateTime())
                    .setCreateTime(LocalDateTime.now())
                    .setCreator(RequestHolder.getUserId())
                    .setCreatorCompanyId(RequestHolder.getCompanyId());
        } else {
            //update
            dbDeliver = deliverInfoService.lambdaQuery().eq(DeliverInfo::getId, req.getDeliverId()).one();
            if (Objects.isNull(dbDeliver)) {
                throw new ApiException("can not update deliver info for not exist or disabled.");
            }
            BeanUtils.copyProperties(dbDeliver, deliverInfo);
        }

        if (req.getOperateType() == 1) {
            deliverInfo.setStatus(LyncsOdsConstant.DeliverStatus.DRAFT.getKey());
        } else {
            deliverInfo.setStatus(LyncsOdsConstant.DeliverStatus.INIT.getKey());
        }

        deliverInfo.setCategoryCount(req.getSkuInfoList().size())
                .setRemark(req.getRemark())
                .setLastUpdater(RequestHolder.getUserId())
                .setUpdateTime(LocalDateTime.now());

        saveSkuInfo(req, deliverInfo);
        saveBillInfo(req, deliverInfo);
        deliverInfoService.saveOrUpdate(deliverInfo);
        txnEditLogService.saveLog(dbDeliver, deliverInfo, LyncsOdsConstant.TxnType.DELIVER.getKey(), deliverInfo.getId());
    }

    /**
     * 保存 交付单sku信息
     *
     * @param req request
     */
    private void saveSkuInfo(SaveDeliverReq req, DeliverInfo deliverInfo) {
        //交付单关联的sku
        List<TxnSkuDetail> deliverSkuDetails = new ArrayList<>();
        if (Objects.nonNull(req.getDeliverId())) {
            deliverSkuDetails = txnSkuDetailService.lambdaQuery()
                    .eq(TxnSkuDetail::getDeliverId, deliverInfo.getId())
                    .list();
        }
        List<Triple<Long, Long, BigDecimal>> orderSkuCountList = new ArrayList<>();
        Triple<List<TxnSkuDetail>, List<Long>, List<TxnSkuDetail>> deliverDiffSkus = TxnCommonUtils.getDeliverDiffSkus(deliverInfo.getId(), req.getSkuInfoList(), deliverSkuDetails, orderSkuCountList);
        txnSkuDetailService.saveBatch(deliverDiffSkus.getLeft());
        txnSkuDetailService.updateBatchById(deliverDiffSkus.getRight());
        txnSkuDetailService.removeByIds(deliverDiffSkus.getMiddle());
        saveOrderSkuInfo(orderSkuCountList);
        //txnOperateInfoService.saveLog(dbDeliver, deliverInfo, OdsConstant.TXN_TYPE_ORDER, deliverInfo.getId());

    }

    /**
     * 更改交付单关联订单sku状态
     *
     * @param orderSkuCountList orderSkuCountList
     */
    private void saveOrderSkuInfo(List<Triple<Long, Long, BigDecimal>> orderSkuCountList) {
        if (CollectionUtil.isEmpty(orderSkuCountList)) {
            return;
        }
        //新增，加订单已交付量
        Triple<Long, Long, BigDecimal> first = orderSkuCountList.get(0);
        LambdaQueryChainWrapper<TxnSkuDetail> queryWrapper = txnSkuDetailService.lambdaQuery().isNull(TxnSkuDetail::getDeliverId)
                .and(wrapper -> wrapper.eq(TxnSkuDetail::getOrderId, first.getLeft()).eq(TxnSkuDetail::getSkuId, first.getMiddle()));
        if (orderSkuCountList.size() > 1) {
            for (int i = 1; i < orderSkuCountList.size(); i++) {
                Triple<Long, Long, BigDecimal> item = orderSkuCountList.get(i);
                queryWrapper.or().eq(TxnSkuDetail::getOrderId, item.getLeft()).eq(TxnSkuDetail::getSkuId, item.getMiddle());
            }
        }
        List<TxnSkuDetail> orderSkuDetails = queryWrapper.list();
        Map<String, BigDecimal> orderSkuMap = orderSkuCountList.stream().collect(Collectors.toMap(triple -> StringUtils.join(triple.getLeft(), LyncsOdsConstant.Common.WELL, triple.getMiddle()), Triple::getRight));
        List<TxnSkuDetail> updateList = orderSkuDetails.stream().map(sku -> {
            BigDecimal bigDecimal = orderSkuMap.getOrDefault(StringUtils.join(sku.getOrderId(), LyncsOdsConstant.Common.WELL, sku.getSkuId()), null);
            if (Objects.isNull(bigDecimal)) {
                return null;
            }
            BigDecimal deliveredCount = sku.getDeliveredCount().add(bigDecimal);
            Integer status = deliveredCount.compareTo(BigDecimal.ZERO) == 0 ? LyncsOdsConstant.TxnSkuStatus.INIT.getKey() : deliveredCount.compareTo(sku.getTotalCount()) < 0 ? LyncsOdsConstant.TxnSkuStatus.PARTIAL_DELIVERY.getKey() : LyncsOdsConstant.TxnSkuStatus.DELIVERED.getKey();
            return new TxnSkuDetail().setId(sku.getId()).setDeliveredCount(deliveredCount).setStatus(status).setLastUpdater(RequestHolder.getUserId()).setUpdateTime(LocalDateTime.now());
        }).filter(Objects::nonNull).collect(Collectors.toList());
        txnSkuDetailService.updateBatchById(updateList);

        updateOrderInfo(updateList.stream().map(TxnSkuDetail::getOrderId).distinct().collect(Collectors.toList()));
    }

    private void updateOrderInfo(List<Long> orderIds) {
        if (CollectionUtil.isEmpty(orderIds)) {
            return;
        }
        List<OrderInfo> orderInfoList = orderInfoService.lambdaQuery().in(OrderInfo::getId, orderIds).list();
        List<TxnSkuDetail> orderSkuDetails = txnSkuDetailService.lambdaQuery().isNull(TxnSkuDetail::getDeliverId).in(TxnSkuDetail::getOrderId, orderIds).list();

        Map<Long, List<Integer>> orderSkuStatusMap = orderSkuDetails.stream().collect(Collectors.groupingBy(TxnSkuDetail::getOrderId, Collectors.mapping(TxnSkuDetail::getStatus, Collectors.toList())));
        List<OrderInfo> updateList = orderInfoList.stream().map(order -> {
            List<Integer> statusList = orderSkuStatusMap.getOrDefault(order.getId(), null);
            if (CollectionUtil.isEmpty(statusList)) {
                return null;
            }
            statusList = statusList.stream().distinct().collect(Collectors.toList());
            Integer first = statusList.get(0);
            Integer status = statusList.size() == 1 && LyncsOdsConstant.TxnSkuStatus.INIT.getKey().equals(first) ? LyncsOdsConstant.OrderStatus.INIT.getKey() :
                    statusList.size() == 1 && LyncsOdsConstant.TxnSkuStatus.DELIVERED.getKey().equals(first) ? LyncsOdsConstant.OrderStatus.DELIVERED.getKey() : LyncsOdsConstant.OrderStatus.PARTIAL_DELIVERY.getKey();
            return new OrderInfo().setId(order.getId()).setStatus(status).setLastUpdater(RequestHolder.getUserId()).setUpdateTime(LocalDateTime.now());
        }).collect(Collectors.toList());
        orderInfoService.updateBatchById(updateList);
    }

    /**
     * 保存 交付单账款信息
     *
     * @param req request
     */
    private void saveBillInfo(SaveDeliverReq req, DeliverInfo deliverInfo) {
        //FIXME: 货款金额校验
        List<TxnFeeDetail> feeDetails = new ArrayList<>();
        List<DeliverBillInfo> billInfos = new ArrayList<>();
        if (req.getDeliverId() != null) {
            billInfos = deliverBillInfoService.lambdaQuery().eq(DeliverBillInfo::getDeliverId, deliverInfo.getBuyerId()).list();
            List<Long> billIds = billInfos.stream().map(DeliverBillInfo::getDeliverId).collect(Collectors.toList());
            feeDetails = txnFeeDetailService.lambdaQuery().in(TxnFeeDetail::getBillId, billIds).eq(TxnFeeDetail::getStatus, LyncsOdsConstant.EnableStatus.ENABLE.getKey()).list();
        }
        Triple<List<Long>, List<DeliverBillInfo>, List<TxnFeeDetail>> deliverFeeDiff = TxnCommonUtils.getDeliverFeeDiff(deliverInfo.getId(), req.getCurrency(), req.getBillInfos(), billInfos, feeDetails);
        deliverBillInfoService.removeByIds(deliverFeeDiff.getLeft());
        txnFeeDetailService.lambdaUpdate().in(TxnFeeDetail::getBillId, deliverFeeDiff.getLeft()).remove();
        deliverBillInfoService.saveOrUpdateBatch(deliverFeeDiff.getMiddle());
        txnFeeDetailService.saveOrUpdateBatch(deliverFeeDiff.getRight());
    }

    @Override
    public ListPageResp listDeliver(ListPageSearchReq req) {
        //FIXME 置顶单据
        List<String> companyIds = StringUtils.isBlank(req.getFilterCompanyId()) ? null : Arrays.asList(StringUtils.split(req.getFilterCompanyId(), LyncsOdsConstant.Common.COMMA));
        List<String> status = StringUtils.isBlank(req.getStatus()) ? null : Arrays.asList(StringUtils.split(req.getStatus(), LyncsOdsConstant.Common.COMMA));

        LambdaQueryChainWrapper<DeliverInfo> wrapper = deliverInfoService.lambdaQuery()
                //查询我发出的
                .eq(LyncsOdsConstant.ContactType.SUPPLIER.getKey().equals(req.getType()), DeliverInfo::getSellerId, RequestHolder.getCompanyId())
                //查询我收到的
                .eq(LyncsOdsConstant.ContactType.CUSTOMER.getKey().equals(req.getType()), DeliverInfo::getBuyerId, RequestHolder.getCompanyId())
                .in(LyncsOdsConstant.ContactType.SUPPLIER.getKey().equals(req.getType()) && CollectionUtil.isNotEmpty(companyIds), DeliverInfo::getBuyerId, companyIds)
                .in(LyncsOdsConstant.ContactType.CUSTOMER.getKey().equals(req.getType()) && CollectionUtil.isNotEmpty(companyIds), DeliverInfo::getSellerId, companyIds)
                .ge(StringUtils.isNotEmpty(req.getStartTime()), DeliverInfo::getStartTime, req.getStartTime())
                .le(StringUtils.isNotEmpty(req.getEndTime()), DeliverInfo::getStartTime, req.getEndTime())
                .in(CollectionUtil.isNotEmpty(status), DeliverInfo::getStatus, status);
        Integer totalCount = wrapper.count();
        List<DeliverInfo> deliverInfos = wrapper.last(req.getLimitSql()).list();
        List<Long> deliverIds = deliverInfos.stream().map(DeliverInfo::getId).collect(Collectors.toList());
        return TxnCommonUtils.getListPageRespData(
                req,
                totalCount,
                deliverInfos,
                txnSkuDetailService.lambdaQuery().in(TxnSkuDetail::getOrderId, deliverIds).list(),
                skuInfoService,
                companyInfoService,
                companyContactInfoService,
                companyUserRelationService
        );
    }

    @Override
    public DeliverDetailInfoResp getDeliverDetail(Long deliverId) {
        DeliverInfo deliverInfo = deliverInfoService.getById(deliverId);
        if (Objects.isNull(deliverInfo)) {
            throw new ApiException("can not get deliver info for not exist or disabled.");
        }
        Pair<String, String> companyNameMap = TxnCommonUtils.getCompanyName(companyInfoService, companyContactInfoService, deliverInfo);

        List<DeliverBillInfo> billInfos = deliverBillInfoService.lambdaQuery().eq(DeliverBillInfo::getDeliverId, deliverInfo.getBuyerId()).list();
        List<Long> billIds = billInfos.stream().map(DeliverBillInfo::getId).collect(Collectors.toList());
        List<TxnFeeDetail> txnFeeDetails = txnFeeDetailService.lambdaQuery().in(TxnFeeDetail::getBillId, billIds).eq(TxnFeeDetail::getStatus, LyncsOdsConstant.EnableStatus.ENABLE.getKey()).list();
        Map<Long, List<TxnFeeDetail>> billFeeDetailMap = txnFeeDetails.stream().collect(Collectors.groupingBy(TxnFeeDetail::getBillId));

        List<TxnSkuDetail> txnSkuDetails = txnSkuDetailService.lambdaQuery().eq(TxnSkuDetail::getDeliverId, deliverId).list();
        Set<Long> skuIds = txnSkuDetails.stream().map(TxnSkuDetail::getSkuId).collect(Collectors.toSet());
        List<SkuInfo> skuInfos = skuInfoService.lambdaQuery().eq(SkuInfo::getCompanyId, deliverInfo.getCreatorCompanyId()).in(SkuInfo::getId, skuIds).list();
        Map<Long, SkuInfo> skuInfoMap = skuInfos.stream().collect(Collectors.toMap(SkuInfo::getId, o -> o));
        List<SkuDetail> skuDetails = skuDetailService.lambdaQuery().in(SkuDetail::getSkuId, skuIds).list();
        Map<Long, Map<String, String>> skuAttrMap = skuDetails.stream().collect(Collectors.groupingBy(SkuDetail::getSkuId, Collectors.toMap(SkuDetail::getName, SkuDetail::getValue)));


        DeliverDetailInfoResp resp = new DeliverDetailInfoResp();
        resp.setDeliverId(deliverId)
                .setTotalAmount(txnFeeDetails.stream().map(TxnFeeDetail::getFeeAmount).reduce(BigDecimal.ZERO, BigDecimal::add).toPlainString())
                .setStatus(deliverInfo.getStatus())
                .setCreateTime(deliverInfo.getCreateTime())
                .setSellerName(companyNameMap.getRight())
                .setBuyerName(companyNameMap.getLeft())
                .setRemark(deliverInfo.getRemark())
                .setCreatorName(TxnCommonUtils.getCreatorName(companyUserRelationService, deliverInfo))
                .setUpdateTime(deliverInfo.getUpdateTime());
        List<DeliverDetailInfoResp.SkuInfo> delSkus = txnSkuDetails.stream().map(txnSkuDetail -> {
            SkuInfo thisSkuInfo = skuInfoMap.get(txnSkuDetail.getSkuId());
            return new DeliverDetailInfoResp.SkuInfo()
                    .setOrderId(txnSkuDetail.getOrderId())
                    .setName(thisSkuInfo.getName())
                    .setTotalCount(txnSkuDetail.getTotalCount())
                    .setDeliveredCount(txnSkuDetail.getDeliveredCount())
                    .setTotalAmount(txnSkuDetail.getTotalAmount().toPlainString())
                    .setAmount(txnSkuDetail.getAmount().toPlainString())
                    .setUnit(thisSkuInfo.getUnit())
                    .setAttrs(skuAttrMap.get(txnSkuDetail.getSkuId()));
        }).collect(Collectors.toList());
        List<DeliverDetailInfoResp.DeliverBillInfo> deliverBillInfos = billInfos.stream().map(bill -> {
            DeliverDetailInfoResp.DeliverBillInfo deliverBillInfo = new DeliverDetailInfoResp.DeliverBillInfo();
            List<TxnFeeDetail> feeDetails = billFeeDetailMap.get(bill.getId());
            deliverBillInfo.setBillId(bill.getId())
                    .setFeeAmount(bill.getTotalAmount())
                    .setCategoryCount(feeDetails.size())
                    .setCreateTime(bill.getCreateTime())
                    .setFeeInfoList(feeDetails.stream().map(fee -> new DeliverDetailInfoResp.DeliverFeeInfo()
                            .setFeeId(fee.getId())
                            .setFeeAmount(fee.getFeeAmount().toPlainString())
                            .setFeeName(fee.getFeeName())
                            .setRemark(fee.getRemark())
                    ).collect(Collectors.toList()));
            return deliverBillInfo;
        }).collect(Collectors.toList());
        resp.setSkuInfoList(delSkus);
        resp.setBillInfos(deliverBillInfos);
        return resp;
    }

    @Override
    public void reverseDeliver(Long deliverId) {
        //FIXME 账款信息
        DeliverInfo deliverInfo = deliverInfoService.lambdaQuery().eq(DeliverInfo::getId, deliverId).one();
        if (Objects.isNull(deliverInfo)) {
            throw new ApiException("cannot find deliver of:" + deliverId);
        }
        List<TxnSkuDetail> deliverDetails = txnSkuDetailService.lambdaQuery().eq(TxnSkuDetail::getDeliverId, deliverId).list();
        ReverseInfo reverseInfo = new ReverseInfo();
        reverseInfo.setId(IdUtil.getSnowflakeNextId())
                .setType(LyncsOdsConstant.TxnType.DELIVER.getKey())
                .setReverseInfo(JSONUtil.toJsonStr(deliverInfo))
                .setReverseSubInfo(JSONUtil.toJsonStr(deliverDetails));
        reverseInfoService.save(reverseInfo);
        deliverInfoService.removeById(deliverInfo.getId());
        txnSkuDetailService.removeByIds(deliverDetails.stream().map(TxnSkuDetail::getId).collect(Collectors.toList()));
    }

    @Override
    public void stickyOnTop(Long deliverId, Integer type) {
        txnOperateInfoService.save(new TxnOperateInfo().setType(LyncsOdsConstant.TxnType.DELIVER.getKey()).setRefId(deliverId).setCompanyId(RequestHolder.getCompanyId()).setCreatorId(RequestHolder.getUserId()).setOperateType(1).setStatus(LyncsOdsConstant.EnableStatus.DISABLE.getKey()));
    }
}
