package com.lyncs.ods.modules.txn.service.impl;

import com.lyncs.ods.modules.txn.model.TxnOperateInfo;
import com.lyncs.ods.modules.txn.mapper.TxnOperateInfoMapper;
import com.lyncs.ods.modules.txn.service.TxnOperateInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * ods操作信息 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
@Service
public class TxnOperateInfoServiceImpl extends ServiceImpl<TxnOperateInfoMapper, TxnOperateInfo> implements TxnOperateInfoService {

}
