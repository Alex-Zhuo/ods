package com.lyncs.ods.modules.txn.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lyncs.ods.modules.txn.mapper.DeliverBillInfoMapper;
import com.lyncs.ods.modules.txn.model.DeliverBillInfo;
import com.lyncs.ods.modules.txn.service.DeliverBillInfoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 账款信息表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
@Service
public class DeliverBillInfoServiceImpl extends ServiceImpl<DeliverBillInfoMapper, DeliverBillInfo> implements DeliverBillInfoService {

}
