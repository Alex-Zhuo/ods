package com.lyncs.ods.modules.txn.mapper;

import com.lyncs.ods.modules.txn.model.ReverseInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 撤销记录表 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface ReverseInfoMapper extends BaseMapper<ReverseInfo> {

}
