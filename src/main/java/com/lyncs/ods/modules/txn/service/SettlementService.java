package com.lyncs.ods.modules.txn.service;

import com.lyncs.ods.req.ListPageSearchReq;
import com.lyncs.ods.req.SaveSettlementReq;
import com.lyncs.ods.resp.SettlementDetailInfoResp;
import com.lyncs.ods.resp.SettlementPageResp;

/**
 * @author alex
 * @date 2022/3/10 14:19
 * @description
 */
public interface SettlementService {

    /**
     * 保存结算单信息
     *
     * @param req 请求信息
     */
    void saveSettlement(SaveSettlementReq req);

    /**
     * 获取指定条件的结算单信息
     *
     * @param req request info
     * @return 结算单信息
     */
    SettlementPageResp listSettlement(ListPageSearchReq req);

    /**
     * 获取指定结算单详情
     *
     * @param settlementId 结算单id
     * @return 详情
     */
    SettlementDetailInfoResp getSettlementDetail(Long settlementId);

    /**
     * 撤销结算单
     *
     * @param settlementId 结算单id
     */
    void reverseSettlement(Long settlementId);

    /**
     * 结算单 置顶/取消
     *
     * @param settlementId settlementId
     * @param type         type
     */
    void stickyOnTop(Long settlementId, Integer type);
}
