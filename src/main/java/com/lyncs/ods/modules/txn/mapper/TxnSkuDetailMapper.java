package com.lyncs.ods.modules.txn.mapper;

import com.lyncs.ods.modules.txn.model.TxnSkuDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * SKU交易详情 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface TxnSkuDetailMapper extends BaseMapper<TxnSkuDetail> {

}
