package com.lyncs.ods.modules.txn.service;

import com.lyncs.ods.modules.txn.model.TxnOperateInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * ods操作信息 服务类
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
public interface TxnOperateInfoService extends IService<TxnOperateInfo> {

}
