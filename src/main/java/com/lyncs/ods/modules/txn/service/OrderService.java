package com.lyncs.ods.modules.txn.service;

import com.lyncs.ods.req.ListPageSearchReq;
import com.lyncs.ods.req.SaveOrderReq;
import com.lyncs.ods.resp.ListPageResp;
import com.lyncs.ods.resp.OrderDetailInfoResp;

/**
 * @author alex
 * @date 2022/2/5 22:45
 * @description
 */
public interface OrderService {

    /**
     * create order
     *
     * @param req request info
     */
    void saveOrder(SaveOrderReq req);

    /**
     * 删除订单
     *
     * @param orderId orderId
     */
    void reverseOrder(Long orderId);

    /**
     * 获取订单列表
     *
     * @param req request info
     */
    ListPageResp listOrder(ListPageSearchReq req);

    /**
     * get order detail info
     *
     * @param orderId orderId
     * @return order detail info
     */
    OrderDetailInfoResp getOrderDetail(Long orderId);

    /**
     * 订单 置顶/取消
     *
     * @param orderId orderId
     * @param type    type
     */
    void stickyOnTop(Long orderId, Integer type);
}
