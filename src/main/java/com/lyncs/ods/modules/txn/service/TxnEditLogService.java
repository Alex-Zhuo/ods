package com.lyncs.ods.modules.txn.service;

import com.lyncs.ods.modules.txn.model.TxnEditLog;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * ods编辑历史记录 服务类
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
public interface TxnEditLogService extends IService<TxnEditLog> {

    /**
     * 保存操作日志
     *
     * @param oldEntity old
     * @param newEntity new
     * @param type      type
     * @param refId     type id
     * @param <T>       object class
     */
    <T> void saveLog(T oldEntity, T newEntity, Integer type, Long refId);

    /**
     * 保存sku操作日志
     *
     * @param oldSkus old
     * @param newSkus new
     * @param type    type
     * @param refId   type id
     * @param <T>     object class
     */
    <T> void saveSkuLog(List<T> oldSkus, List<T> newSkus, Integer type, Long refId);
}
