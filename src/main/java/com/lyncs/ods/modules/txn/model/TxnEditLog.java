package com.lyncs.ods.modules.txn.model;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * ods编辑历史记录
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("txn_edit_log")
@ApiModel(value = "TxnEditLog对象", description = "ods编辑历史记录")
public class TxnEditLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("自增主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("类型：1.order;2.deliver;3.settlement")
    @TableField("type")
    private Integer type;

    @ApiModelProperty("类型关联ID")
    @TableField("ref_id")
    private Long refId;

    @ApiModelProperty("用户ID")
    @TableField("operator")
    private Long operator;

    @ApiModelProperty("用户所属企业ID")
    @TableField("operator_company_id")
    private Long operatorCompanyId;

    @ApiModelProperty("操作类型：1.新增;2.修改;3.删除")
    @TableField("operate_type")
    private Integer operateType;

    @ApiModelProperty("更新前内容")
    @TableField("old_content")
    private String oldContent;

    @ApiModelProperty("更新后内容")
    @TableField("new_content")
    private String newContent;

    @ApiModelProperty("创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;


}
