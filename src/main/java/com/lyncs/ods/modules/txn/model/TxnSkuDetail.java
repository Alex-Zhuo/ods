package com.lyncs.ods.modules.txn.model;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * SKU交易详情
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("txn_sku_detail")
@ApiModel(value = "TxnSkuDetail对象", description = "SKU交易详情")
public class TxnSkuDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("自增主键id")
    @TableId(value = "id")
    private Long id;

    @ApiModelProperty("订单id")
    @TableField("order_id")
    private Long orderId;

    @ApiModelProperty("交付单id")
    @TableField("deliver_id")
    private Long deliverId;

    @ApiModelProperty("sku的id")
    @TableField("sku_id")
    private Long skuId;

    @ApiModelProperty("本次交易本SKU需要交付数量")
    @TableField("total_count")
    private BigDecimal totalCount;

    @ApiModelProperty("已交付商品数")
    @TableField("delivered_count")
    private BigDecimal deliveredCount;

    @ApiModelProperty("本次交易本SKU单价（同一个SKU在不同订单有不同价格）")
    @TableField("amount")
    private BigDecimal amount;

    @ApiModelProperty("总金额")
    @TableField("total_amount")
    private BigDecimal totalAmount;

    @ApiModelProperty("交易开始时间")
    @TableField("start_time")
    private LocalDateTime startTime;

    @ApiModelProperty("状态：1.已创建；2.部分交付；3.全部交付")
    @TableField("status")
    private Integer status;

    @ApiModelProperty("备注")
    @TableField("remark")
    private String remark;

    @ApiModelProperty("创建者用户id")
    @TableField("creator")
    private Long creator;

    @ApiModelProperty("最后更新用户id")
    @TableField("last_updater")
    private Long lastUpdater;

    @ApiModelProperty("创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


}
