package com.lyncs.ods.modules.txn.mapper;

import com.lyncs.ods.modules.txn.model.TxnOperateInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * ods操作信息 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
@Mapper
public interface TxnOperateInfoMapper extends BaseMapper<TxnOperateInfo> {

}
