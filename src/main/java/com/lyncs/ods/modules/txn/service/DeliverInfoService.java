package com.lyncs.ods.modules.txn.service;

import com.lyncs.ods.modules.txn.model.DeliverInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 交付单总表 服务类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface DeliverInfoService extends IService<DeliverInfo> {

}
