package com.lyncs.ods.modules.goods.mapper;

import com.lyncs.ods.modules.goods.model.SpuDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * spu详细信息表 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface SpuDetailMapper extends BaseMapper<SpuDetail> {

}
