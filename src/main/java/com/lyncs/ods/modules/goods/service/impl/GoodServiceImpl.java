package com.lyncs.ods.modules.goods.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.IdUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.google.common.collect.MapDifference;
import com.google.common.collect.Maps;
import com.lyncs.ods.common.exception.ApiException;
import com.lyncs.ods.common.web.RequestHolder;
import com.lyncs.ods.constant.LyncsOdsConstant;
import com.lyncs.ods.modules.goods.model.*;
import com.lyncs.ods.modules.goods.service.*;
import com.lyncs.ods.req.ListSkuReq;
import com.lyncs.ods.req.SaveSkuReq;
import com.lyncs.ods.resp.SkuInfoResp;
import com.lyncs.ods.resp.SpuAttrInfoResp;
import com.lyncs.ods.resp.SpuInfoResp;
import com.lyncs.ods.utils.CurrencyUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author alex
 * @date 2022/2/5 21:28
 * @description
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class GoodServiceImpl implements GoodService {

    @Autowired
    private SpuDetailService spuDetailService;
    @Autowired
    private SpuInfoService spuInfoService;
    @Autowired
    private SkuInfoService skuInfoService;
    @Autowired
    private SkuDetailService skuDetailService;
    @Autowired
    private SkuAttrLogService skuAttrLogService;

    /**
     * get all available spu
     *
     * @return spu info
     */
    @Override
    public List<SpuInfoResp> listSpu(String industry) {
        List<SpuInfo> spuInfos = spuInfoService.lambdaQuery().eq(SpuInfo::getIndustry, industry).eq(SpuInfo::getStatus, LyncsOdsConstant.SPUStatus.ENABLE.getKey()).list();
        return spuInfos.stream().map(info -> new SpuInfoResp().setSpuId(info.getId()).setName(info.getName())).collect(Collectors.toList());
    }

    /**
     * get spu attr info by id
     *
     * @param spuId spu id
     * @return attr infos
     */
    @Override
    public List<SpuAttrInfoResp> getSpuAttrInfos(Long spuId) {
        List<SpuDetail> attrs = spuDetailService.lambdaQuery().eq(SpuDetail::getSpuId, spuId).eq(SpuDetail::getStatus, LyncsOdsConstant.SPUStatus.ENABLE.getKey()).list();
        return attrs.stream().map(info -> {
            SpuAttrInfoResp spuAttrInfoResp = new SpuAttrInfoResp();
            BeanUtils.copyProperties(info, spuAttrInfoResp);
            return spuAttrInfoResp;
        }).collect(Collectors.toList());
    }

    /**
     * batch save sku
     *
     * @param req request info
     * @return sku id
     */
    @Override
    public Long saveSku(SaveSkuReq req) {
        SkuInfo skuInfo;
        List<SkuDetail> skuDetails;
        Long skuId = req.getSkuId();
        if (skuId == null) {
            checkExist(req);
            skuId = IdUtil.getSnowflakeNextId();
            skuInfo = new SkuInfo()
                    .setId(skuId)
                    .setSpuId(req.getSpuId())
                    .setCompanyId(RequestHolder.getCompanyId());

            skuDetails = req.getAttrs().stream().map(attr -> {
                SkuDetail skuDetail = new SkuDetail();
                skuDetail.setId(IdUtil.getSnowflakeNextId())
                        .setSkuId(skuInfo.getId())
                        .setSpuId(req.getSpuId())
                        .setName(attr.getName())
                        .setValue(attr.getValue())
                        .setStatus(LyncsOdsConstant.SKUStatus.ENABLE.getKey());
                return skuDetail;
            }).collect(Collectors.toList());
        } else {
            skuInfo = skuInfoService.lambdaQuery().eq(SkuInfo::getId, req.getSkuId()).one();
            if (Objects.isNull(skuInfo) || !LyncsOdsConstant.SPUStatus.ENABLE.getKey().equals(skuInfo.getStatus())) {
                throw new ApiException("can not update sku for not exist or disabled.");
            }
            skuDetails = skuDetailService.lambdaQuery().eq(SkuDetail::getSkuId, skuInfo.getId()).list();
        }
        skuInfo.setName(req.getName())
                .setImgUrl(req.getImgUrl())
                .setAmount(req.getAmount())
                .setStatus(LyncsOdsConstant.SKUStatus.ENABLE.getKey())
                .setUnit(req.getUnit())
                .setCurrency(req.getCurrency())
                .setAmountJson(JSONObject.toJSONString(req.getAmountJson()));
        skuInfoService.saveOrUpdate(skuInfo);
        skuDetailService.saveOrUpdateBatch(skuDetails);
        return skuId;
    }

    private void checkExist(SaveSkuReq req) {
        List<SkuInfoResp> skuInfoResp = listSkuInfo(req.getSpuId());
        if (CollectionUtil.isEmpty(skuInfoResp)) {
            return;
        }
        Map<String, String> newSkuAttrMap = req.getAttrs().stream().collect(Collectors.toMap(SaveSkuReq.AttrInfo::getName, SaveSkuReq.AttrInfo::getValue));

        Map<Long, Map<String, String>> skuAttrMap = skuInfoResp.stream().collect(Collectors.toMap(SkuInfoResp::getSkuId, x -> x.getAttrs().stream().collect(Collectors.toMap(SkuInfoResp.SkuAttr::getName, SkuInfoResp.SkuAttr::getValue))));

        skuAttrMap.forEach((skuId, attrMap) -> {
            MapDifference<String, String> mapDifference = Maps.difference(newSkuAttrMap, attrMap);
            // entriesInCommon：返回两个map中key和value都相同的记录；
            // entriesOnlyOnLeft：返回左边Map独有的记录；
            // entriesOnlyOnRight：返回右边Map独有的记录；
            // entriesDiffering：返回两个map中key相同value不同的记录.
            //areEqual：返回两个map是否完全相同
            if (mapDifference.areEqual()) {
                throw new ApiException("sku has already exist.");
            }
        });
    }

    /**
     * get sku info by spu id
     *
     * @param req request info
     * @return sku infos
     */
    @Override
    public List<SkuInfoResp> listSkuInfo(ListSkuReq req) {
        saveAttrLog(req);
        LambdaQueryChainWrapper<SkuDetail> wrapper = skuDetailService.lambdaQuery().eq(SkuDetail::getSpuId, req.getSpuId()).eq(SkuDetail::getStatus, LyncsOdsConstant.SKUStatus.ENABLE.getKey());
        if (MapUtil.isNotEmpty(req.getAttrMap())) {
            req.getAttrMap().forEach((name, val) -> {
                wrapper.or().eq(SkuDetail::getName, name).eq(SkuDetail::getValue, val);
            });
        }
        List<SkuDetail> skuDetails = wrapper.list();
        if (CollectionUtil.isEmpty(skuDetails)) {
            return null;
        }
        List<Long> skuIds = skuDetails.stream().map(SkuDetail::getId).distinct().collect(Collectors.toList());

        List<SkuInfo> skuInfos = skuInfoService.lambdaQuery().eq(SkuInfo::getSpuId, req.getSpuId()).in(SkuInfo::getId, skuIds).eq(SkuInfo::getStatus, LyncsOdsConstant.SKUStatus.ENABLE.getKey()).list();
        if (CollectionUtil.isEmpty(skuInfos)) {
            return null;
        }

        Map<Long, List<SkuDetail>> skuDetailMap = skuDetails.stream().collect(Collectors.groupingBy(SkuDetail::getSkuId));
        return skuInfos.stream().map(sku -> {
            SkuInfoResp skuInfoResp = new SkuInfoResp();
            BeanUtils.copyProperties(sku, skuInfoResp);
            if (Objects.nonNull(req.getCurrency())) {
                skuInfoResp.setCurrency(req.getCurrency());
                skuInfoResp.setAmount(getAmount(sku.getCurrency(), req.getCurrency(), sku.getAmountJson(), sku.getAmount()));
            }
            List<SkuDetail> thisSkuDetails = skuDetailMap.get(sku.getId());
            if (CollectionUtil.isEmpty(thisSkuDetails)) {
                return null;
            }
            List<SkuInfoResp.SkuAttr> skuAttrs = thisSkuDetails.stream().map(detail -> {
                SkuInfoResp.SkuAttr attr = new SkuInfoResp.SkuAttr();
                attr.setName(detail.getName());
                attr.setValue(detail.getValue());
                return attr;
            }).collect(Collectors.toList());
            skuInfoResp.setAttrs(skuAttrs);
            return skuInfoResp;
        }).filter(Objects::nonNull).collect(Collectors.toList());
    }

    @Override
    public List<String> listSpuOptions(Long spuId, String name) {
        List<SkuAttrLog> skuAttrLogs = skuAttrLogService.lambdaQuery().eq(SkuAttrLog::getSpuId, spuId).eq(SkuAttrLog::getCompanyId, RequestHolder.getCompanyId()).eq(SkuAttrLog::getName, name).list();
        return skuAttrLogs.stream().map(SkuAttrLog::getName).distinct().collect(Collectors.toList());
    }

    @Async
    public void saveAttrLog(ListSkuReq req) {
        if (MapUtil.isEmpty(req.getAttrMap())) {
            return;
        }
        List<SkuAttrLog> skuAttrLogs = req.getAttrMap().entrySet().stream()
                .map(entry -> new SkuAttrLog().setCompanyId(RequestHolder.getCompanyId()).setSpuId(req.getSpuId()).setName(entry.getKey()).setValue(entry.getValue()))
                .collect(Collectors.toList());
        //todo：unique key 时会不会有问题
        skuAttrLogService.saveOrUpdateBatch(skuAttrLogs);
    }

    private List<SkuInfoResp> listSkuInfo(Long spuId) {
        return listSkuInfo(new ListSkuReq(spuId, null, null));
    }

    private BigDecimal getAmount(String fromCurrency, String toCurrency, String amountStr, BigDecimal amount) {
        if (Objects.isNull(fromCurrency) || Objects.isNull(toCurrency) || fromCurrency.equals(toCurrency)) {
            return amount;
        }
        JSONObject amountJson = JSONObject.parseObject(amountStr);
        if (amountJson.containsKey(toCurrency)) {
            return CurrencyUtil.fen2yuan(amountJson.getBigDecimal(toCurrency));
        }
        return CurrencyUtil.exchange(fromCurrency, toCurrency, amount, Boolean.TRUE);
    }
}
