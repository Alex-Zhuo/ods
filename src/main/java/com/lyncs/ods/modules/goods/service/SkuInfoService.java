package com.lyncs.ods.modules.goods.service;

import com.lyncs.ods.modules.goods.model.SkuInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * sku信息总表 服务类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface SkuInfoService extends IService<SkuInfo> {

}
