package com.lyncs.ods.modules.goods.service.impl;

import com.lyncs.ods.modules.goods.model.SkuDetail;
import com.lyncs.ods.modules.goods.mapper.SkuDetailMapper;
import com.lyncs.ods.modules.goods.service.SkuDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * sku详细信息表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
@Service
public class SkuDetailServiceImpl extends ServiceImpl<SkuDetailMapper, SkuDetail> implements SkuDetailService {

}
