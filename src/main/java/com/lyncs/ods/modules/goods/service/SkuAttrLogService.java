package com.lyncs.ods.modules.goods.service;

import com.lyncs.ods.modules.goods.model.SkuAttrLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * sku详细信息表 服务类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface SkuAttrLogService extends IService<SkuAttrLog> {

}
