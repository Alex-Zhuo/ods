package com.lyncs.ods.modules.goods.model;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * sku详细信息表
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sku_detail")
@ApiModel(value = "SkuDetail对象", description = "sku详细信息表")
public class SkuDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id")
    @TableId(value = "id")
    private Long id;

    @ApiModelProperty("sku id")
    @TableField("sku_id")
    private Long skuId;

    @ApiModelProperty("spu id")
    @TableField("spu_id")
    private Long spuId;

    @ApiModelProperty("sku属性名称")
    @TableField("name")
    private String name;

    @ApiModelProperty("sku属性值")
    @TableField("value")
    private String value;

    @ApiModelProperty("状态：1.启用；2.禁用")
    @TableField("status")
    private Integer status;

    @ApiModelProperty("创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


}
