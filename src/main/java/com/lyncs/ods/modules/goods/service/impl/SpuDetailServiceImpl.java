package com.lyncs.ods.modules.goods.service.impl;

import com.lyncs.ods.modules.goods.model.SpuDetail;
import com.lyncs.ods.modules.goods.mapper.SpuDetailMapper;
import com.lyncs.ods.modules.goods.service.SpuDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * spu详细信息表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
@Service
public class SpuDetailServiceImpl extends ServiceImpl<SpuDetailMapper, SpuDetail> implements SpuDetailService {

}
