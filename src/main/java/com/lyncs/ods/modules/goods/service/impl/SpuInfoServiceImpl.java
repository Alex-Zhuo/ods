package com.lyncs.ods.modules.goods.service.impl;

import com.lyncs.ods.modules.goods.model.SpuInfo;
import com.lyncs.ods.modules.goods.mapper.SpuInfoMapper;
import com.lyncs.ods.modules.goods.service.SpuInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * spu信息总表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
@Service
public class SpuInfoServiceImpl extends ServiceImpl<SpuInfoMapper, SpuInfo> implements SpuInfoService {

}
