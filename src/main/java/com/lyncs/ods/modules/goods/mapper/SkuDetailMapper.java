package com.lyncs.ods.modules.goods.mapper;

import com.lyncs.ods.modules.goods.model.SkuDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * sku详细信息表 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface SkuDetailMapper extends BaseMapper<SkuDetail> {

}
