package com.lyncs.ods.modules.goods.service;

import com.lyncs.ods.modules.goods.model.SpuInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * spu信息总表 服务类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface SpuInfoService extends IService<SpuInfo> {

}
