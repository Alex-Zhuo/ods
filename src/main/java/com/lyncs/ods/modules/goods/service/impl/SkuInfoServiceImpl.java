package com.lyncs.ods.modules.goods.service.impl;

import com.lyncs.ods.modules.goods.model.SkuInfo;
import com.lyncs.ods.modules.goods.mapper.SkuInfoMapper;
import com.lyncs.ods.modules.goods.service.SkuInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * sku信息总表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
@Service
public class SkuInfoServiceImpl extends ServiceImpl<SkuInfoMapper, SkuInfo> implements SkuInfoService {

}
