package com.lyncs.ods.modules.goods.mapper;

import com.lyncs.ods.modules.goods.model.SpuInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * spu信息总表 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface SpuInfoMapper extends BaseMapper<SpuInfo> {

}
