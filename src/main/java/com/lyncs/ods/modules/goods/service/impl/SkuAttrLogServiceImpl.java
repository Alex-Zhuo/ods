package com.lyncs.ods.modules.goods.service.impl;

import com.lyncs.ods.modules.goods.model.SkuAttrLog;
import com.lyncs.ods.modules.goods.mapper.SkuAttrLogMapper;
import com.lyncs.ods.modules.goods.service.SkuAttrLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * sku详细信息表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
@Service
public class SkuAttrLogServiceImpl extends ServiceImpl<SkuAttrLogMapper, SkuAttrLog> implements SkuAttrLogService {

}
