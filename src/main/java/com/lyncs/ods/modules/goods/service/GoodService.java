package com.lyncs.ods.modules.goods.service;

import com.lyncs.ods.req.ListSkuReq;
import com.lyncs.ods.req.SaveSkuReq;
import com.lyncs.ods.resp.SkuInfoResp;
import com.lyncs.ods.resp.SpuAttrInfoResp;
import com.lyncs.ods.resp.SpuInfoResp;

import java.util.List;

/**
 * @author alex
 * @date 2022/2/5 21:28
 * @description
 */
public interface GoodService {

    /**
     * get all available spu
     *
     * @return spu info
     */
    List<SpuInfoResp> listSpu(String industry);

    /**
     * get spu attr info by id
     *
     * @param spuId spu id
     * @return attr infos
     */
    List<SpuAttrInfoResp> getSpuAttrInfos(Long spuId);

    /**
     * save sku
     *
     * @param req request info
     * @return sku id
     */
    Long saveSku(SaveSkuReq req);

    /**
     * get sku info by spu id
     *
     * @param req request info
     * @return sku infos
     */
    List<SkuInfoResp> listSkuInfo(ListSkuReq req);

    /**
     * 根据指定spuId和属性名称查询候选项
     *
     * @param spuId spuId
     * @param name  属性名称
     * @return options
     */
    List<String> listSpuOptions(Long spuId, String name);
}
