package com.lyncs.ods.modules.goods.model;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * sku信息总表
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sku_info")
@ApiModel(value = "SkuInfo对象", description = "sku信息总表")
public class SkuInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("sku id")
    @TableId(value = "id")
    private Long id;

    @ApiModelProperty("spu的id，值为0表示自定义SKU")
    @TableField("spu_id")
    private Long spuId;

    @ApiModelProperty("sku名称")
    @TableField("name")
    private String name;

    @ApiModelProperty("自定义货号")
    @TableField("no")
    private String no;

    @ApiModelProperty("企业id")
    @TableField("company_id")
    private Long companyId;

    @ApiModelProperty("单价")
    @TableField("amount")
    private BigDecimal amount;

    @ApiModelProperty("货币")
    @TableField("currency")
    private String currency;

    @ApiModelProperty("不同币种的单价信息")
    @TableField("amount_json")
    private String amountJson;

    @ApiModelProperty("单位")
    @TableField("unit")
    private String unit;

    @ApiModelProperty("sku 图片地址")
    @TableField("img_url")
    private String imgUrl;

    @ApiModelProperty("备注")
    @TableField("remark")
    private String remark;

    @ApiModelProperty("状态：1.启用；2.禁用")
    @TableField("status")
    private Integer status;

    @ApiModelProperty("创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


}
