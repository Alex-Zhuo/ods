package com.lyncs.ods.modules.goods.controller;


import com.lyncs.ods.common.api.CommonResult;
import com.lyncs.ods.modules.goods.service.GoodService;
import com.lyncs.ods.req.ListSkuReq;
import com.lyncs.ods.req.SaveSkuReq;
import com.lyncs.ods.resp.SkuInfoResp;
import com.lyncs.ods.resp.SpuAttrInfoResp;
import com.lyncs.ods.resp.SpuInfoResp;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * sku信息 前端控制器
 * </p>
 *
 * @author alex
 * @since 2022-02-05
 */
@RestController
@RequestMapping("/goods")
@Api(tags = {"货物相关接口"})
public class GoodController {

    @Autowired
    private GoodService goodService;

    @GetMapping("spu/list")
    @ApiOperation(value = "获取所有spu")
    public CommonResult<List<SpuInfoResp>> listSpuInfo(@RequestParam("industry") String industry) {
        return CommonResult.success(goodService.listSpu(industry));
    }

    @GetMapping("spu/options")
    @ApiOperation(value = "获取所有spu某个属性的候选项")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "spu_id", value = "spu id", dataType = "Long"),
            @ApiImplicitParam(name = "name", value = "spu属性名称", dataType = "String"),
    })
    public CommonResult<List<String>> listSpuOptions(@RequestParam(name = "spu_id") Long spuId, @RequestParam(name = "name") String name) {
        return CommonResult.success(goodService.listSpuOptions(spuId, name));
    }

    @GetMapping("spu/detail")
    @ApiOperation(value = "根据spu id获取spu属性信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "spu_id", value = "spu id", dataType = "String"),
    })
    public CommonResult<List<SpuAttrInfoResp>> getSpuDetailInfo(@RequestParam(name = "spu_id") Long spuId) {
        return CommonResult.success(goodService.getSpuAttrInfos(spuId));
    }

    @PostMapping("sku/save")
    @ApiOperation(value = "保存或更新sku")
    public CommonResult<Long> saveSku(@Valid @RequestBody SaveSkuReq req) {
        return CommonResult.success(goodService.saveSku(req));
    }

    @GetMapping("sku/list")
    @ApiOperation(value = "根据spu id和属性获取sku信息")
    public CommonResult<List<SkuInfoResp>> listSkuInfo(@Valid @RequestBody ListSkuReq req) {
        return CommonResult.success(goodService.listSkuInfo(req));
    }
}

