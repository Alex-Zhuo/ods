package com.lyncs.ods.modules.setting.service;

import com.lyncs.ods.modules.setting.model.PaymentSetting;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 收款方式配置表 服务类
 * </p>
 *
 * @author alex
 * @since 2022-04-01
 */
public interface PaymentSettingService extends IService<PaymentSetting> {

}
