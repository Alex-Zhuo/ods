package com.lyncs.ods.modules.setting.mapper;

import com.lyncs.ods.modules.setting.model.CurrencyRateSetting;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 汇率配置表 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2022-04-06
 */
@Mapper
public interface CurrencyRateSettingMapper extends BaseMapper<CurrencyRateSetting> {

}
