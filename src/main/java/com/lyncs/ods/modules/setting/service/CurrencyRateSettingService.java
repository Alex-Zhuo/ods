package com.lyncs.ods.modules.setting.service;

import com.lyncs.ods.modules.setting.model.CurrencyRateSetting;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 汇率配置表 服务类
 * </p>
 *
 * @author alex
 * @since 2022-04-06
 */
public interface CurrencyRateSettingService extends IService<CurrencyRateSetting> {

}
