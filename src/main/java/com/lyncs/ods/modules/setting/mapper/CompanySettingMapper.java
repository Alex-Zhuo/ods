package com.lyncs.ods.modules.setting.mapper;

import com.lyncs.ods.modules.setting.model.CompanySetting;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 公司账户配置表 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface CompanySettingMapper extends BaseMapper<CompanySetting> {

}
