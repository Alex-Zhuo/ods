package com.lyncs.ods.modules.setting.mapper;

import com.lyncs.ods.modules.setting.model.PaymentSetting;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 收款方式配置表 Mapper 接口
 * </p>
 *
 * @author alex
 * @since 2022-04-01
 */
@Mapper
public interface PaymentSettingMapper extends BaseMapper<PaymentSetting> {

}
