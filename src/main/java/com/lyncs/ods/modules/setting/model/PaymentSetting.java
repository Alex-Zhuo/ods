package com.lyncs.ods.modules.setting.model;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 收款方式配置表
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("payment_setting")
@ApiModel(value = "PaymentSetting对象", description = "收款方式配置表")
public class PaymentSetting implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("自增id")
    @TableId(value = "id")
    private Long id;

    @ApiModelProperty("收款方式名称")
    @TableField("name")
    private String name;

    @ApiModelProperty("收款方式code")
    @TableField("code")
    private String code;

    @ApiModelProperty("收款方式logo")
    @TableField("logo")
    private String logo;

    @ApiModelProperty("收款方式配置属性")
    @TableField("attr_json")
    private String attrJson;

    @ApiModelProperty("状态：1.可用；2.禁用")
    @TableField("status")
    private Boolean status;

    @ApiModelProperty("创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


}
