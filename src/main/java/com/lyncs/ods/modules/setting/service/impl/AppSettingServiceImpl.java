package com.lyncs.ods.modules.setting.service.impl;

import com.lyncs.ods.modules.setting.model.AppSetting;
import com.lyncs.ods.modules.setting.mapper.AppSettingMapper;
import com.lyncs.ods.modules.setting.service.AppSettingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 公共配置表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
@Service
public class AppSettingServiceImpl extends ServiceImpl<AppSettingMapper, AppSetting> implements AppSettingService {

}
