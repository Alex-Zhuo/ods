package com.lyncs.ods.modules.setting.service;

import com.lyncs.ods.modules.setting.model.CompanySetting;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 公司账户配置表 服务类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface CompanySettingService extends IService<CompanySetting> {

}
