package com.lyncs.ods.modules.setting.service.impl;

import com.lyncs.ods.modules.setting.model.CurrencyRateSetting;
import com.lyncs.ods.modules.setting.mapper.CurrencyRateSettingMapper;
import com.lyncs.ods.modules.setting.service.CurrencyRateSettingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 汇率配置表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-04-06
 */
@Service
public class CurrencyRateSettingServiceImpl extends ServiceImpl<CurrencyRateSettingMapper, CurrencyRateSetting> implements CurrencyRateSettingService {

}
