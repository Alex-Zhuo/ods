package com.lyncs.ods.modules.setting.service.impl;

import com.lyncs.ods.modules.setting.model.CompanySetting;
import com.lyncs.ods.modules.setting.mapper.CompanySettingMapper;
import com.lyncs.ods.modules.setting.service.CompanySettingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 公司账户配置表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
@Service
public class CompanySettingServiceImpl extends ServiceImpl<CompanySettingMapper, CompanySetting> implements CompanySettingService {

}
