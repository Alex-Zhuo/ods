package com.lyncs.ods.modules.setting.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 汇率配置表
 * </p>
 *
 * @author alex
 * @since 2022-04-09
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("currency_rate_setting")
@ApiModel(value = "CurrencyRateSetting对象", description = "汇率配置表")
public class CurrencyRateSetting implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("自增主键id")
    @TableId(value = "id")
    private Long id;

    @ApiModelProperty("原货币")
    @TableField("from_currency")
    private String fromCurrency;

    @ApiModelProperty("目标货币")
    @TableField("to_currency")
    private String toCurrency;

    @ApiModelProperty("汇率")
    @TableField("rate")
    private BigDecimal rate;

    @ApiModelProperty("日期")
    @TableField("date")
    private LocalDate date;

    @ApiModelProperty("状态: 1.可用;2.禁用")
    @TableField("status")
    private Integer status;
}
