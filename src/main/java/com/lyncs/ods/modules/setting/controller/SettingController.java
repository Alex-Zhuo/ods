package com.lyncs.ods.modules.setting.controller;


import cn.hutool.core.util.ArrayUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.lyncs.ods.common.api.CommonResult;
import com.lyncs.ods.constant.LyncsOdsConstant;
import com.lyncs.ods.modules.company.service.CompanyPaymentInfoService;
import com.lyncs.ods.modules.setting.model.PaymentSetting;
import com.lyncs.ods.modules.setting.service.PaymentSettingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 * sku信息 前端控制器
 * </p>
 *
 * @author alex
 * @since 2022-02-05
 */
@RestController
@RequestMapping("/settings")
@Api(tags = {"系统配置项相关接口"})
@Slf4j
public class SettingController {

    @Autowired
    private PaymentSettingService paymentSettingService;

    @GetMapping("/list")
    @ApiOperation(value = "获取系统相关说明")
    public CommonResult<List<Setting>> listSpuInfo() {
        SerializeConfig serializeConfig = new SerializeConfig();
        List<Setting> settings = Arrays.stream(LyncsOdsConstant.SystemConfig.values())
                .map(item -> {
                    Enum<? extends Enum<?>>[] enumConstants = item.getClazz().getEnumConstants();
                    if (ArrayUtil.isEmpty(enumConstants)) {
                        return null;
                    }
                    //noinspection unchecked
                    serializeConfig.configEnumAsJavaBean(item.getClazz());
                    List<JSONObject> configs = Arrays.stream(enumConstants)
                            .map(enumConstant -> JSONObject.toJSONString(enumConstant, serializeConfig))
                            .map(JSONObject::parseObject)
                            .collect(Collectors.toList());
                    log.info("{}", JSON.toJSONString(configs));
                    return new Setting()
                            .setKey(item.getKey())
                            .setDesc(item.getDesc())
                            .setConfigs(configs);
                }).filter(Objects::nonNull).collect(Collectors.toList());
        return CommonResult.success(settings);
    }

    @GetMapping("/payments")
    @ApiOperation(value = "获取系统所有支持的支付方式")
    public CommonResult<List<PaymentSetting>> listPaymentsInfo() {
        List<PaymentSetting> paymentSettings = paymentSettingService.lambdaQuery().eq(PaymentSetting::getStatus, 1).list();
        return CommonResult.success(paymentSettings);
    }

    @Data
    @Accessors(chain = true)
    private static class Setting {
        private String key;
        private String desc;
        private List<JSONObject> configs;
    }
}

