package com.lyncs.ods.modules.setting.service;

import com.lyncs.ods.modules.setting.model.AppSetting;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 公共配置表 服务类
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
public interface AppSettingService extends IService<AppSetting> {

}
