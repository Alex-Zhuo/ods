package com.lyncs.ods.modules.setting.service.impl;

import com.lyncs.ods.modules.setting.model.PaymentSetting;
import com.lyncs.ods.modules.setting.mapper.PaymentSettingMapper;
import com.lyncs.ods.modules.setting.service.PaymentSettingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 收款方式配置表 服务实现类
 * </p>
 *
 * @author alex
 * @since 2022-04-01
 */
@Service
public class PaymentSettingServiceImpl extends ServiceImpl<PaymentSettingMapper, PaymentSetting> implements PaymentSettingService {

}
