package com.lyncs.ods.modules.share.controller;


import com.lyncs.ods.common.api.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 公司账号信息表 前端控制器
 * </p>
 *
 * @author alex
 * @since 2022-02-15
 */
@RestController
@RequestMapping("s")
@Api(tags = {"邀请相关接口"})
public class ShareController {


    /**
     * create a share link
     *
     * @return url
     */
    @PostMapping("link")
    @ApiOperation(value = "获取分享链接")
    public CommonResult<String> createShareLink() {
        return CommonResult.success();
    }

    /**
     * accept share
     *
     * @return
     */
    @PostMapping("accept")
    @ApiOperation(value = "接受邀请")
    public CommonResult<?> acceptShareLink() {
        return CommonResult.success();
    }

}

