package com.lyncs.ods;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author alex
 */
@SpringBootApplication
@EnableTransactionManagement
@EnableScheduling
public class OdsApplication {

    public static void main(String[] args) {
        SpringApplication.run(OdsApplication.class, args);
    }

}
