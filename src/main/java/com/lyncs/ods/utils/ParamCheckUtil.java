package com.lyncs.ods.utils;

import cn.hutool.core.util.ObjectUtil;
import com.lyncs.ods.common.exception.ApiException;
import com.lyncs.ods.req.SaveTxnBaseReq;

import java.util.List;

/**
 * @author alex
 * @date 2022/2/26 20:49
 * @description
 */
public class ParamCheckUtil {

    public static <T extends SaveTxnBaseReq> void checkTxnOptions(T req) {
        if (ObjectUtil.isAllEmpty(req.getSellerId())) {
            throw new ApiException("sellerId and sellerPartnerId cannot be null at the same time ");
        }
        if (ObjectUtil.isAllEmpty(req.getBuyerId())) {
            throw new ApiException("buyerId and buyerPartnerId cannot be null at the same time ");
        }
        if (ObjectUtil.isAllEmpty(req.getSellerId(), req.getBuyerId())) {
            throw new ApiException("buyerId and sellerId cannot be null at the same time ");
        }
        if (!List.of(1, 2).contains(req.getOperateType())) {
            throw new ApiException("do not support operate-type of: " + req.getOperateType());
        }
    }
}
