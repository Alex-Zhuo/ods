package com.lyncs.ods.utils;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONValidator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;
import java.util.function.Consumer;

/**
 * @author alex
 * @date 2022/4/3 23:06
 * @description 阿里产品工具类
 */
@Slf4j
public class AliUtils {

    public static final String SMS_URL = "https://dysmsapi.aliyuncs.com/?Action=SendSms&PhoneNumbers=%s&SignName=%s&TemplateCode=%s&TemplateParam=%s";

    public static <T> String sendSms(String phone, String sign, String code, String param, Consumer<T> successFunc, Consumer<T> failedFunc, T data) {
        String url = String.format(SMS_URL, phone, sign, code, param);
        String result = HttpUtil.get(url);
        if (StringUtils.isEmpty(result) || !JSONValidator.from(result).validate()) {
            log.error("send sms failed, phone:{}, code:{}", phone, code);
            return null;
        }
        JSONObject retJson = JSONObject.parseObject(result);
        if (Objects.nonNull(retJson) && "OK".equals(retJson.getString("Code"))) {
            successFunc.accept(data);
        } else {
            failedFunc.accept(data);
        }
        return retJson.getString("BizId");
    }

    public static <T> String senSmsCode(String phone, Consumer<T> successFunc, Consumer<T> failedFunc, T data) {
        final String templateCode = "SMS_238250828";
        //TODO:未申请
        final String sign = "";
        String code = RandomUtil.randomNumbers(4);
        JSONObject param = new JSONObject() {{
            put("code", code);
        }};
        return sendSms(phone, sign, templateCode, JSON.toJSONString(param), successFunc, failedFunc, data);
    }

    public static void main(String[] args) {
        String bizCode = senSmsCode("15297746757", System.out::println, System.out::println, "test");
        System.out.println(bizCode);
    }
}
