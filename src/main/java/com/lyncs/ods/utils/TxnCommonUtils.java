package com.lyncs.ods.utils;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.lyncs.ods.common.exception.ApiException;
import com.lyncs.ods.common.web.RequestHolder;
import com.lyncs.ods.constant.LyncsOdsConstant;
import com.lyncs.ods.modules.company.model.CompanyContactInfo;
import com.lyncs.ods.modules.company.model.CompanyInfo;
import com.lyncs.ods.modules.company.service.CompanyContactInfoService;
import com.lyncs.ods.modules.company.service.CompanyInfoService;
import com.lyncs.ods.modules.goods.model.SkuInfo;
import com.lyncs.ods.modules.goods.service.SkuInfoService;
import com.lyncs.ods.modules.txn.model.DeliverBillInfo;
import com.lyncs.ods.modules.txn.model.TxnCommonInfo;
import com.lyncs.ods.modules.txn.model.TxnFeeDetail;
import com.lyncs.ods.modules.txn.model.TxnSkuDetail;
import com.lyncs.ods.modules.user.model.CompanyUserRelation;
import com.lyncs.ods.modules.user.service.CompanyUserRelationService;
import com.lyncs.ods.req.ListPageSearchReq;
import com.lyncs.ods.req.SaveDeliverReq;
import com.lyncs.ods.req.SaveOrderReq;
import com.lyncs.ods.resp.ListPageResp;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author alex
 * @date 2022/2/27 15:14
 * @description
 */
public class TxnCommonUtils {

    /**
     * @param companyInfoService companyInfoService
     * @param contactInfoService contactInfoService
     * @param companyIds         companyIds
     * @param partnerContactIds  partnerContactIds
     * @return L:companyNameMap;R:contactNameMap
     */
    public static Pair<Map<Long, String>, Map<Long, String>> getCompanyInfo(CompanyInfoService companyInfoService, CompanyContactInfoService contactInfoService, Set<Long> companyIds, Set<Long> partnerContactIds) {
        List<CompanyInfo> companyInfos = new ArrayList<>();
        List<CompanyContactInfo> companyContactInfos = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(companyIds)) {
            companyInfos = companyInfoService.lambdaQuery().in(CompanyInfo::getId, companyIds).list();
        }
        if (CollectionUtil.isNotEmpty(partnerContactIds)) {
            companyContactInfos = contactInfoService.lambdaQuery().in(CompanyContactInfo::getInitCompanyId, partnerContactIds).list();
        }
        Map<Long, String> companyNameMap = companyInfos.stream().collect(Collectors.toMap(CompanyInfo::getId, CompanyInfo::getShortName));
        Map<Long, String> contactNameMap = companyContactInfos.stream().collect(Collectors.toMap(CompanyContactInfo::getInitCompanyId, CompanyContactInfo::getNickname));
        return Pair.of(companyNameMap, contactNameMap);
    }

    /**
     * 获取买卖方公司名称
     *
     * @param companyInfoService companyInfoService
     * @param contactInfoService contactInfoService
     * @param commonInfo         commonInfo
     * @return L:buyerName;R:sellerName
     */
    public static Pair<String, String> getCompanyName(CompanyInfoService companyInfoService, CompanyContactInfoService contactInfoService, TxnCommonInfo commonInfo) {
        //todo 分享查看可能会有问题
        Integer type = RequestHolder.getCompanyId().equals(commonInfo.getBuyerId()) ? LyncsOdsConstant.ContactType.SUPPLIER.getKey() : LyncsOdsConstant.ContactType.CUSTOMER.getKey();
        Pair<Map<Long, String>, Map<Long, String>> namPair = getTxnCompanyName(type, List.of(commonInfo), companyInfoService, contactInfoService);
        Map<Long, String> buyerMap = namPair.getLeft();
        Map<Long, String> sellerMap = namPair.getRight();
        return Pair.of(buyerMap.get(commonInfo.getBuyerId()), sellerMap.get(commonInfo.getSellerId()));
    }

    public static String getCreatorName(CompanyUserRelationService companyUserRelationService, TxnCommonInfo commonInfo) {
        CompanyUserRelation companyUser = companyUserRelationService.lambdaQuery().eq(CompanyUserRelation::getCompanyId, commonInfo.getCreatorCompanyId()).eq(CompanyUserRelation::getUserId, commonInfo.getCreator()).last(" limit 1").one();
        if (Objects.isNull(companyUser)) {
            throw new ApiException("cannot found creator user name.");
        }
        return companyUser.getNickname();
    }

    public static <T extends TxnCommonInfo> Map<String, String> getCreatorName(CompanyUserRelationService companyUserRelationService, @NotNull List<T> commonInfos) {
        LambdaQueryChainWrapper<CompanyUserRelation> lambdaQueryWrapper = companyUserRelationService.lambdaQuery();
        for (TxnCommonInfo commonInfo : commonInfos) {
            lambdaQueryWrapper.or(wrapper -> wrapper.eq(CompanyUserRelation::getCompanyId, commonInfo.getCreatorCompanyId()).eq(CompanyUserRelation::getUserId, commonInfo.getCreator()));
        }
        List<CompanyUserRelation> companyUserRelations = lambdaQueryWrapper.list();
        return companyUserRelations.stream().collect(Collectors.toMap(o -> StringUtils.join(o.getCompanyId(), LyncsOdsConstant.Common.WELL, o.getUserId()), CompanyUserRelation::getNickname, (o1, o2) -> o1));
    }

    /**
     * 获取差异sku
     *
     * @param skuInfoList     order request sku infos
     * @param existSkuDetails order sku infos from database
     * @return L:insert list;M:delete id list;R:update list
     */
    public static Triple<List<TxnSkuDetail>, List<Long>, List<TxnSkuDetail>> getOrderDiffSkus(Long orderId, List<SaveOrderReq.OrderSkuInfo> skuInfoList, List<TxnSkuDetail> existSkuDetails) {
        List<TxnSkuDetail> skuInsertList = new ArrayList<>();
        List<Long> skuDelList = new ArrayList<>();
        List<TxnSkuDetail> skuUpdateList = new ArrayList<>();

        Map<Long, TxnSkuDetail> existSkuMap = existSkuDetails.stream().collect(Collectors.toMap(TxnSkuDetail::getSkuId, o -> o));
        Map<Long, SaveOrderReq.OrderSkuInfo> reqSkuMap = skuInfoList.stream().collect(Collectors.toMap(SaveOrderReq.OrderSkuInfo::getSkuId, o -> o));
        List<Long> reqSkuIds = new ArrayList<>(reqSkuMap.keySet());
        List<Long> existSkuIds = new ArrayList<>(existSkuMap.keySet());
        List<Long> allSkuIds = CollectionUtil.addAllIfNotContains(reqSkuIds, existSkuIds);
        for (Long skuId : allSkuIds) {
            TxnSkuDetail existSku = existSkuMap.get(skuId);
            SaveOrderReq.OrderSkuInfo reqSkuInfo = reqSkuMap.get(skuId);

            if (existSku != null && reqSkuInfo != null) {
                //update
                skuUpdateList.add(
                        existSku.setTotalCount(new BigDecimal(reqSkuInfo.getCount()))
                                .setAmount(new BigDecimal(reqSkuInfo.getAmount()))
                                .setTotalAmount(new BigDecimal(reqSkuInfo.getAmount()).multiply(new BigDecimal(reqSkuInfo.getCount())))
                                .setRemark(reqSkuInfo.getRemark())
                                .setLastUpdater(RequestHolder.getUserId())
                );
            } else if (existSku != null) {
                //delete
                skuDelList.add(existSkuMap.get(skuId).getId());
            } else {
                //insert
                skuInsertList.add(
                        new TxnSkuDetail().setId(IdUtil.getSnowflakeNextId())
                                .setOrderId(orderId)
                                .setSkuId(reqSkuInfo.getSkuId())
                                .setTotalCount(new BigDecimal(reqSkuInfo.getCount()))
                                .setDeliveredCount(BigDecimal.ZERO)
                                .setAmount(new BigDecimal(reqSkuInfo.getAmount()))
                                .setTotalAmount(new BigDecimal(reqSkuInfo.getAmount()).multiply(new BigDecimal(reqSkuInfo.getCount())))
                                .setStartTime(RequestHolder.getRequestDateTime())
                                .setStatus(LyncsOdsConstant.OrderStatus.INIT.getKey())
                                .setRemark(reqSkuInfo.getRemark())
                                .setCreator(RequestHolder.getUserId())
                                .setLastUpdater(RequestHolder.getUserId())
                                .setCreateTime(LocalDateTime.now())
                                .setUpdateTime(LocalDateTime.now())
                );
            }
        }
        return Triple.of(skuInsertList, skuDelList, skuUpdateList);
    }

    /**
     * 获取差异sku
     *
     * @param skuInfoList      deliver request sku infos
     * @param existDeliverSkus deliver sku infos from database
     * @return L:insert list;M:delete id list;R:update list
     */
    public static Triple<List<TxnSkuDetail>, List<Long>, List<TxnSkuDetail>> getDeliverDiffSkus(Long deliverId, List<SaveDeliverReq.DeliverSkuInfo> skuInfoList, List<TxnSkuDetail> existDeliverSkus, List<Triple<Long, Long, BigDecimal>> orderSkuCountList) {
        Triple<List<TxnSkuDetail>, List<Long>, List<TxnSkuDetail>> triple = Triple.of(new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
        Map<Boolean, Map<Long, SaveDeliverReq.DeliverSkuInfo>> newSkuMap = skuInfoList.stream().collect(Collectors.groupingBy(sku -> Objects.nonNull(sku.getOrderId()) ? Boolean.TRUE : Boolean.FALSE, Collectors.toMap(SaveDeliverReq.DeliverSkuInfo::getSkuId, o -> o)));
        Map<Boolean, Map<Long, TxnSkuDetail>> oldSkuMap = existDeliverSkus.stream().collect(Collectors.groupingBy(sku -> Objects.nonNull(sku.getOrderId()) ? Boolean.TRUE : Boolean.FALSE, Collectors.toMap(TxnSkuDetail::getSkuId, o -> o)));

        //关联订单的交付单sku差异信息
        orderSkuCountList = getDeliverDiffSkus(deliverId, newSkuMap.get(Boolean.TRUE), oldSkuMap.get(Boolean.TRUE), triple);
        //未关联订单的交付单sku差异信息
        getDeliverDiffSkus(deliverId, newSkuMap.get(Boolean.FALSE), oldSkuMap.get(Boolean.FALSE), triple);
        return triple;
    }

    private static List<Triple<Long, Long, BigDecimal>> getDeliverDiffSkus(Long deliverId, Map<Long, SaveDeliverReq.DeliverSkuInfo> newSkuMap, Map<Long, TxnSkuDetail> oldSkuMap, Triple<List<TxnSkuDetail>, List<Long>, List<TxnSkuDetail>> triple) {
        List<Long> existSkuIds = new ArrayList<>(oldSkuMap.keySet());
        List<Long> allSkuIds = CollectionUtil.addAllIfNotContains(new ArrayList<>(newSkuMap.keySet()), existSkuIds);
        List<Triple<Long, Long, BigDecimal>> orderSkuCountList = new ArrayList<>();
        for (Long skuId : allSkuIds) {
            TxnSkuDetail existSku = oldSkuMap.get(skuId);
            SaveDeliverReq.DeliverSkuInfo reqSkuInfo = newSkuMap.get(skuId);
            BigDecimal deliverCount = reqSkuInfo == null ? null : new BigDecimal(reqSkuInfo.getDeliverCount());
            if (existSku != null && reqSkuInfo != null) {
                //update
                triple.getRight().add(
                        existSku.setTotalCount(deliverCount)
                                .setDeliveredCount(deliverCount)
                                .setStatus(LyncsOdsConstant.TxnSkuStatus.DELIVERED.getKey())
                                .setLastUpdater(RequestHolder.getUserId())
                                .setRemark(reqSkuInfo.getRemark())
                );
                if (Objects.nonNull(existSku.getOrderId())) {
                    orderSkuCountList.add(Triple.of(existSku.getOrderId(), existSku.getSkuId(), existSku.getDeliveredCount().subtract(deliverCount)));
                }
            } else if (existSku != null) {
                deliverCount = BigDecimal.ZERO.subtract(existSku.getDeliveredCount());
                //delete
                triple.getMiddle().add(existSku.getId());
                if (Objects.nonNull(existSku.getOrderId())) {
                    orderSkuCountList.add(Triple.of(existSku.getOrderId(), existSku.getSkuId(), deliverCount));
                }
            } else if (reqSkuInfo != null) {
                //insert
                triple.getLeft().add(new TxnSkuDetail()
                        .setId(IdUtil.getSnowflakeNextId())
                        .setDeliverId(deliverId)
                        .setOrderId(reqSkuInfo.getOrderId())
                        .setSkuId(reqSkuInfo.getSkuId())
                        .setTotalCount(new BigDecimal(reqSkuInfo.getDeliverCount()))
                        .setDeliveredCount(new BigDecimal(reqSkuInfo.getDeliverCount()))
                        .setAmount(new BigDecimal(reqSkuInfo.getAmount()))
                        .setTotalAmount(new BigDecimal(reqSkuInfo.getAmount()).multiply(new BigDecimal(reqSkuInfo.getDeliverCount())))
                        .setStartTime(RequestHolder.getRequestDateTime())
                        .setStatus(LyncsOdsConstant.DeliverStatus.INIT.getKey())
                        .setRemark(reqSkuInfo.getRemark())
                        .setCreator(RequestHolder.getUserId())
                        .setLastUpdater(RequestHolder.getUserId())
                        .setCreateTime(LocalDateTime.now())
                        .setUpdateTime(LocalDateTime.now()));
                if (Objects.nonNull(reqSkuInfo.getOrderId())) {
                    orderSkuCountList.add(Triple.of(reqSkuInfo.getOrderId(), reqSkuInfo.getSkuId(), deliverCount));
                }
            }
        }
        return orderSkuCountList;
    }

    public static <T extends TxnCommonInfo> ListPageResp getListPageRespData(ListPageSearchReq req, Integer totalCount, List<T> infos, List<TxnSkuDetail> skuDetails, SkuInfoService skuInfoService, CompanyInfoService companyInfoService, CompanyContactInfoService companyContactInfoService, CompanyUserRelationService companyUserRelationService) {
        ListPageResp resp = new ListPageResp().setPage(req.getPage()).setPageSize(req.getPageSize()).setTotalSize(totalCount);
        if (CollectionUtil.isEmpty(infos)) {
            return resp;
        }
        Map<String, String> creatorNameMap = getCreatorName(companyUserRelationService, infos);
        Pair<Map<Long, String>, Map<Long, String>> txnCompanyName = getTxnCompanyName(req.getType(), infos, companyInfoService, companyContactInfoService);
        Map<Long, String> buyerMap = txnCompanyName.getLeft();
        Map<Long, String> sellerMap = txnCompanyName.getRight();

        Set<Long> skuIds = skuDetails.stream().map(TxnSkuDetail::getSkuId).collect(Collectors.toSet());
        List<SkuInfo> skuInfos = skuInfoService.lambdaQuery().in(SkuInfo::getId, skuIds).list();
        Map<Long, SkuInfo> skuInfoMap = skuInfos.stream().collect(Collectors.toMap(SkuInfo::getId, o -> o, (o1, o2) -> o1));
        Map<Long, List<TxnSkuDetail>> skuDetailMap = skuDetails.stream().collect(Collectors.groupingBy(TxnSkuDetail::getOrderId));

        resp.setData(infos.stream().map(info -> {
            List<TxnSkuDetail> thisSkuDetails = skuDetailMap.get(info.getId());
            List<String> thisSkuImg = thisSkuDetails.stream().map(skuDetail -> skuInfoMap.get(skuDetail.getSkuId()).getImgUrl()).collect(Collectors.toList());
            return new ListPageResp.ShortInfo()
                    .setId(info.getId())
                    .setCategoryCount(info.getCategoryCount())
                    .setSellerName(sellerMap.get(info.getSellerId()))
                    .setBuyerName(buyerMap.get(info.getBuyerId()))
                    .setSkuImages(thisSkuImg)
                    .setTotalAmount(thisSkuDetails.stream().map(TxnSkuDetail::getTotalAmount).reduce(BigDecimal.ZERO, BigDecimal::add).toPlainString())
                    .setStatus(info.getStatus())
                    .setCreatorName(creatorNameMap.get(StringUtils.join(info.getCreatorCompanyId(), LyncsOdsConstant.Common.WELL, info.getCreator())))
                    .setCreateTime(info.getStartTime())
                    .setUpdateTime(info.getUpdateTime())
                    .setSellerId(info.getSellerId())
                    .setBuyerId(info.getBuyerId())
                    .setCreateCompanyId(info.getCreatorCompanyId());
        }).collect(Collectors.toList()));
        return resp;
    }


    private static <T extends TxnCommonInfo> Pair<Map<Long, String>, Map<Long, String>> getTxnCompanyName(Integer type, List<T> infos, CompanyInfoService companyInfoService, CompanyContactInfoService companyContactInfoService) {
        List<Long> companyIds = infos.stream().map(info -> LyncsOdsConstant.ContactType.SUPPLIER.getKey().equals(type) ? info.getSellerId() : info.getBuyerId()).distinct().collect(Collectors.toList());
        Map<Long, String> concatInfoMap = companyContactInfoService.getConcatNameMap(companyIds, type);
        CompanyInfo companyInfo = companyInfoService.lambdaQuery().eq(CompanyInfo::getId, RequestHolder.getCompanyId()).one();
        Map<Long, String> sellerNameMap = LyncsOdsConstant.ContactType.SUPPLIER.getKey().equals(type) ? Map.of(companyInfo.getId(), companyInfo.getShortName()) : concatInfoMap;
        Map<Long, String> buyerNameMap = LyncsOdsConstant.ContactType.CUSTOMER.getKey().equals(type) ? Map.of(companyInfo.getId(), companyInfo.getShortName()) : concatInfoMap;
        return Pair.of(buyerNameMap, sellerNameMap);
    }

    public static Triple<List<Long>, List<DeliverBillInfo>, List<TxnFeeDetail>> getDeliverFeeDiff(Long deliverId, String currency, List<SaveDeliverReq.DeliverBillInfo> reqBillInfos, List<DeliverBillInfo> existBillInfos, List<TxnFeeDetail> existFeeDetails) {
        List<Long> reqExistBillIds = reqBillInfos.stream().map(SaveDeliverReq.DeliverBillInfo::getBillId).filter(Objects::nonNull).distinct().collect(Collectors.toList());
        Map<Long, DeliverBillInfo> billMap = existBillInfos.stream().collect(Collectors.toMap(DeliverBillInfo::getId, o -> o));
        List<Long> billDeleteIds = existBillInfos.stream().filter(bill -> !reqExistBillIds.contains(bill.getId())).map(DeliverBillInfo::getId).collect(Collectors.toList());
        Map<Long, Map<Long, TxnFeeDetail>> billFeeMap = existFeeDetails.stream().collect(Collectors.groupingBy(TxnFeeDetail::getBillId, Collectors.toMap(TxnFeeDetail::getId, o -> o)));
        List<DeliverBillInfo> updateBillList = new ArrayList<>();
        List<TxnFeeDetail> updateFeeList = new ArrayList<>();
        reqBillInfos.forEach(bill -> {
            DeliverBillInfo txnBillInfo = new DeliverBillInfo();
            List<SaveDeliverReq.DeliverFeeInfo> feeInfoList = bill.getFeeInfoList();
            BigDecimal totalAmount = bill.getFeeInfoList().stream().map(SaveDeliverReq.DeliverFeeInfo::getFeeAmount).map(BigDecimal::new).reduce(BigDecimal.ZERO, BigDecimal::add);
            Long billId = bill.getBillId();
            if (Objects.isNull(billId)) {
                billId = IdUtil.getSnowflakeNextId();
                txnBillInfo.setId(billId)
                        .setDeliverId(deliverId)
                        .setTotalAmount(totalAmount)
                        .setSettledAmount(BigDecimal.ZERO)
                        .setCurrency(currency)
                        .setStartTime(RequestHolder.getRequestDateTime())
                        .setStatus(LyncsOdsConstant.DeliverStatus.INIT.getKey())
                        .setCreateTime(RequestHolder.getRequestDateTime());
                Long finalBillId = billId;
                updateFeeList.addAll(feeInfoList.stream().map(fee -> new TxnFeeDetail()
                        .setId(IdUtil.getSnowflakeNextId())
                        .setBillId(finalBillId)
                        .setFeeName(fee.getFeeName())
                        .setFeeAmount(new BigDecimal(fee.getFeeAmount()))
                        .setRemark(fee.getRemark())
                        .setStatus(LyncsOdsConstant.EnableStatus.ENABLE.getKey())
                        .setCreateTime(LocalDateTime.now())
                        .setUpdateTime(LocalDateTime.now())
                ).collect(Collectors.toList()));
            } else {
                DeliverBillInfo existBillInfo = billMap.get(bill.getBillId());
                txnBillInfo.setId(existBillInfo.getId())
                        .setTotalAmount(totalAmount);
                Map<Long, TxnFeeDetail> feeDetailMap = billFeeMap.get(bill.getBillId());
                updateFeeList.addAll(feeInfoList.stream().map(fee -> {
                    TxnFeeDetail txnFeeDetail = feeDetailMap.get(fee.getFeeId());
                    if (Objects.isNull(txnFeeDetail)) {
                        return null;
                    }
                    return new TxnFeeDetail()
                            .setId(txnFeeDetail.getId())
                            .setFeeName(fee.getFeeName())
                            .setFeeAmount(new BigDecimal(fee.getFeeAmount()))
                            .setRemark(fee.getRemark())
                            .setUpdateTime(LocalDateTime.now());
                }).collect(Collectors.toList()));
            }
            txnBillInfo.setUpdateTime(LocalDateTime.now());
            updateBillList.add(txnBillInfo);
        });
        return Triple.of(billDeleteIds, updateBillList, updateFeeList);
    }
}
