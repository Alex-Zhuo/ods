package com.lyncs.ods.task;

import com.lyncs.ods.constant.LyncsOdsConstant;
import com.lyncs.ods.modules.msg.model.MsgSendLog;
import com.lyncs.ods.modules.msg.model.MsgValidateLog;
import com.lyncs.ods.modules.msg.service.MsgSendLogService;
import com.lyncs.ods.modules.msg.service.MsgValidateLogService;
import lombok.AllArgsConstructor;

import java.util.Objects;
import java.util.TimerTask;

/**
 * @author alex
 */
@AllArgsConstructor
public class CleanTask extends TimerTask {

    private Long validateLogId;
    private Long sendLogId;
    private MsgValidateLogService msgValidateLogService;
    private MsgSendLogService msgSendLogService;

    @Override
    public void run() {
        if (Objects.nonNull(validateLogId) && Objects.nonNull(msgValidateLogService)) {
            msgValidateLogService.lambdaUpdate().eq(MsgValidateLog::getId, validateLogId)
                    .ne(MsgValidateLog::getStatus, LyncsOdsConstant.MessageStatus.VERIFIED.getKey())
                    .set(MsgValidateLog::getStatus, LyncsOdsConstant.MessageStatus.EXPIRED.getKey())
                    .update();
        }
        if (Objects.nonNull(sendLogId) && Objects.nonNull(msgSendLogService)) {
            msgSendLogService.lambdaUpdate().eq(MsgSendLog::getId, sendLogId)
                    .ne(MsgSendLog::getStatus, LyncsOdsConstant.MessageStatus.VERIFIED.getKey())
                    .set(MsgSendLog::getStatus, LyncsOdsConstant.MessageStatus.EXPIRED.getKey())
                    .update();
        }
    }
}