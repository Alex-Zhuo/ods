package com.lyncs.ods.config;

import com.lyncs.ods.common.config.BaseRedisConfig;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

/**
 * Redis配置类
 *
 * @author alex
 * @date 2022/01/25
 */
@EnableCaching
@Configuration
public class RedisConfig extends BaseRedisConfig {

}
