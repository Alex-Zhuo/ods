package com.lyncs.ods.config;

import com.google.common.collect.Maps;
import com.lyncs.ods.common.exception.ApiException;
import com.lyncs.ods.properties.WxMpProperties;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.config.impl.WxMpDefaultConfigImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * WeChat open config
 *
 * @author alex
 */
@Slf4j
@Configuration
@EnableConfigurationProperties(WxMpProperties.class)
public class WxMpConfiguration {
    private static Map<String, WxMpService> mpServices = Maps.newHashMap();

    public static Map<String, WxMpService> getMpServices() {
        return mpServices;
    }

    @Autowired
    private WxMpProperties properties;

    @PostConstruct
    public void initServices() {
        // 代码里 getConfigs()处报错的同学，请注意仔细阅读项目说明，你的IDE需要引入lombok插件！！！！
        final List<WxMpProperties.MpConfig> configs = this.properties.getConfigs();
        if (configs == null) {
            throw new ApiException("第三方账号信息未配置");
        }

        mpServices = configs.stream().map(a -> {
            WxMpDefaultConfigImpl wxMpDefaultConfig = new WxMpDefaultConfigImpl();
            wxMpDefaultConfig.setAppId(a.getAppId());
            wxMpDefaultConfig.setSecret(a.getSecret());
            wxMpDefaultConfig.setToken(a.getToken());
            wxMpDefaultConfig.setAesKey(a.getAesKey());
            WxMpService service = new WxMpServiceImpl();
            service.setWxMpConfigStorage(wxMpDefaultConfig);
            return service;
        }).collect(Collectors.toMap(s -> s.getWxMpConfigStorage().getAppId(), a -> a, (o, n) -> o));
    }
}