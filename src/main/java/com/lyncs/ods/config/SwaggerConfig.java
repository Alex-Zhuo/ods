package com.lyncs.ods.config;

import com.lyncs.ods.common.config.BaseSwaggerConfig;
import com.lyncs.ods.common.domain.SwaggerProperties;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger API文档相关配置
 *
 * @author alex
 * @date 2022/01/25
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig extends BaseSwaggerConfig {

    @Override
    public SwaggerProperties swaggerProperties() {
        return SwaggerProperties.builder()
                .apiBasePackage("com.lyncs.ods.modules")
                .title("ODS")
                .description("ODS接口文档")
                .contactName("alex")
                .version("1.0")
                .enableSecurity(true)
                .build();
    }
}
