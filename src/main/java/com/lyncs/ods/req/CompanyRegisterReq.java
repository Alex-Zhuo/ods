package com.lyncs.ods.req;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * @author alex
 * @date 2022/1/30 01:35
 * @description
 */
@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CompanyRegisterReq {

    @Length(max = 64, message = " of length must between 1 and 64")
    private String name;

    @NotNull
    @Length(max = 15, message = " of length must between 1 and 15")
    private String shortName;

    @ApiModelProperty(value = "所属行业")
    private String industry;

    @ApiModelProperty(value = "员工规模")
    private String scale;

    @Length(min = 20, max = 256, message = " of length must between 20 and 256")
    private String avatarUrl;

    private String address;
}
