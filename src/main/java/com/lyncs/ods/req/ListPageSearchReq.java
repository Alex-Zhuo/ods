package com.lyncs.ods.req;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.NotNull;

/**
 * @author alex
 * @date 2022/2/21 22:17
 * @description
 */
@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ListPageSearchReq {

    @ApiModelProperty(value = "当前查询者选择的身份。1.供应商（查询我发出的）;2.客户（查询我收到的）")
    @NotNull
    private Integer type;

    @ApiModelProperty(value = "type=1时过滤此项指定的客户企业ID，type=2时过滤此项指定的供应商企业ID。多个使用英文逗号分隔")
    private String filterCompanyId;

    @ApiModelProperty(value = "订单查询开始时间")
    private String startTime;

    @ApiModelProperty(value = "订单查询结束时间")
    private String endTime;

    @ApiModelProperty(value = "订单状态，多个使用英文逗号分隔")
    private String status;

    @ApiModelProperty(value = "当前页数")
    @NotNull
    private Integer page;

    @ApiModelProperty(value = "当前页请求数量，默认20")
    @NotNull
    private Integer pageSize = 20;

    public String getLimitSql() {
        int offset = (page - 1) * pageSize + 1;
        return " limit " + offset + ", " + pageSize;
    }
}
