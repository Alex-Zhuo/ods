package com.lyncs.ods.req;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * @author alex
 * @date 2022/2/2 14:26
 * @description
 */
@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SavePartnerCompanyReq {

    @ApiModelProperty(value = "被关联的企业初始id，新增时不传该字段")
    private Long companyId;

    @NotNull
    @ApiModelProperty(value = "合作企业备注")
    @Length(max = 32, message = " of length must between 1 and 32")
    private String nickname;

    @ApiModelProperty(value = "电话号码。多个使用英文逗号分隔")
    private String telephone;

    @ApiModelProperty(value = "传真号码")
    private String fax;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "公司地址")
    private String address;

    @ApiModelProperty("关系类型：1.供应商；2.客户;多个使用英文逗号分隔")
    @NotNull
    private String type;
}
