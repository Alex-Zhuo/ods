package com.lyncs.ods.req;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author alex
 * @date 2022/2/26 20:51
 * @description
 */
@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SaveTxnBaseReq {

    @ApiModelProperty(value = "卖方公司id")
    @NotNull
    private Long sellerId;

    @ApiModelProperty(value = "买方公司id")
    @NotNull
    private Long buyerId;

    @ApiModelProperty(value = "操作类型：1.存为草稿;2.保存订单;默认为1")
    @NotNull
    private Integer operateType = 1;
}
