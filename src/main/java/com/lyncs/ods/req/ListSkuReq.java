package com.lyncs.ods.req;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;

import java.util.Map;

/**
 * @author alex
 * @date 2022/03/25 21:36
 * @description
 */
@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class ListSkuReq {

    @NonNull
    @ApiModelProperty(value = "spu id")
    private Long spuId;

    @ApiModelProperty(value = "货币，默认为sku对应的货币")
    private String currency;

    @ApiModelProperty(value = "属性map")
    private Map<String, String> attrMap;
}
