package com.lyncs.ods.req;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author alex
 * @date 2022/2/5 20:43
 * @description
 */
@EqualsAndHashCode(callSuper = true)
@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SaveOrderReq extends SaveTxnBaseReq {

    @ApiModelProperty(value = "内部单号")
    private String innerCode;

    @ApiModelProperty(value = "预计发货时间")
    private LocalDateTime estimatedDeliveryTime;

    @ApiModelProperty(value = "订单id")
    @NotNull
    private Long orderId;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "sku info")
    @Length(min = 1, message = " cannot be empty")
    private List<OrderSkuInfo> skuInfoList;

    @Data
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class OrderSkuInfo {
        @ApiModelProperty(value = "sku id")
        @NotNull
        private Long skuId;

        @ApiModelProperty(value = "总商品数")
        @NotNull
        @Min(value = 0, message = " cannot less than 0")
        private String count;

        @ApiModelProperty(value = "SKU 单价")
        @NotNull
        @Pattern(regexp = "\\d+(\\.\\d+)?", message = " illegal")
        private String amount;

        @ApiModelProperty(value = "备注")
        private String remark;
    }
}
