package com.lyncs.ods.req;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @author alex
 * @date 2022/2/5 14:52
 * @description
 */
@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SaveUserInfoReq {

    @ApiModelProperty(value = "手机号码")
    @NotNull
    @Pattern(regexp = "^1\\d{10}$", message = " invalid")
    private String phone;

    @NotNull
    //@ApiModelProperty(value = "短信验证码")
    //@Length(max = 10, message = " of length must between 1 and 10")
    private String code;

    @ApiModelProperty(value = "用户头像url")
    private String avatarUrl;

    @ApiModelProperty(value = "用户姓名")
    private String username;

    @ApiModelProperty(value = "用户绑定邮箱")
    private String email;
}
