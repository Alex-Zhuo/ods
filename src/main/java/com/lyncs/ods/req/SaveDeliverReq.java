package com.lyncs.ods.req;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author alex
 * @date 2022/2/26 20:21
 * @description
 */
@EqualsAndHashCode(callSuper = true)
@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SaveDeliverReq extends SaveTxnBaseReq {

    @ApiModelProperty(value = "订单id")
    @NotNull
    private Long deliverId;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty("货币")
    private String currency;

    @ApiModelProperty(value = "sku 信息")
    private List<DeliverSkuInfo> skuInfoList;

    @ApiModelProperty(value = "款项 信息")
    private List<DeliverBillInfo> billInfos;

    @Data
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class DeliverSkuInfo {

        @ApiModelProperty(value = "订单ID")
        private Long orderId;

        @ApiModelProperty(value = "sku id")
        @NotNull
        private Long skuId;

        @ApiModelProperty(value = "SKU 单价")
        @NotNull
        @Pattern(regexp = "\\d+(\\.\\d+)?", message = " illegal")
        private String amount;

        @ApiModelProperty(value = "备注")
        private String remark;

        @NotNull
        @ApiModelProperty(value = "交付商品数")
        @Pattern(regexp = "\\d+(\\.\\d+)?", message = " illegal")
        private String deliverCount;
    }

    @Data
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class DeliverBillInfo {

        @ApiModelProperty(value = "账款ID")
        private Long billId;

        @ApiModelProperty(value = "款项 信息")
        private List<DeliverFeeInfo> feeInfoList;
    }

    @Data
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class DeliverFeeInfo {

        @ApiModelProperty(value = "款项ID")
        private Long feeId;

        @ApiModelProperty(value = "款项名称")
        private String feeName;

        @ApiModelProperty(value = "款项金额")
        private String feeAmount;

        @ApiModelProperty("备注")
        private String remark;
    }
}
