package com.lyncs.ods.req;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author alex
 * @date 2022/2/2 14:24
 * @description
 */
@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CompanyRelateReq {

    /*@ApiModelProperty(value = "用户id")
    @NotNull
    private String userId;*/

    @ApiModelProperty(value = "公司id")
    @NotNull
    private Long companyId;

    private Integer role;
}
