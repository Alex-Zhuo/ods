package com.lyncs.ods.req;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author alex
 * @date 2022/2/5 21:46
 * @description
 */
@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SaveSkuReq {

    @ApiModelProperty(value = "spu id")
    @NotNull
    private Long spuId;

    @ApiModelProperty(value = "sku id，有这个值视为更新")
    private Long skuId;

    @ApiModelProperty(value = "sku name")
    @NotNull
    private String name;

    @ApiModelProperty(value = "sku 单位")
    @NotNull
    private String unit;

    @ApiModelProperty(value = "sku 图片地址")
    @NotNull
    private String imgUrl;

    @ApiModelProperty(value = "单价(分)")
    private BigDecimal amount;

    @ApiModelProperty("货币")
    @TableField("currency")
    private String currency;

    @ApiModelProperty("不同币种的单价信息。key:货币;val:金额(分)")
    @TableField("amount_json")
    private Map<String, BigDecimal> amountJson;

    @ApiModelProperty(value = "属性KV")
    @NotNull
    private List<AttrInfo> attrs;

    @Data
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class AttrInfo {
        @ApiModelProperty(value = "spu名称")
        private String name;

        @ApiModelProperty(value = "sku的值")
        private String value;
    }
}

