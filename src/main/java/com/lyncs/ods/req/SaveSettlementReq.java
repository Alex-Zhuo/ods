package com.lyncs.ods.req;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * @author alex
 * @date 2022/3/11 22:17
 * @description
 */
@EqualsAndHashCode(callSuper = true)
@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SaveSettlementReq extends SaveTxnBaseReq{

    @ApiModelProperty(value = "交付单ID")
    private Long settlementId;

    @ApiModelProperty(value = "截止日期")
    private LocalDateTime deadline;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "交付信息")
    @Length(min = 1, message = " cannot be empty")
    private List<DeliverSettleInfo> deliverSettleInfoList;

    @ApiModelProperty(value = "优惠信息")
    private List<DeliverPromotionInfo> promotionInfos;

    @Data
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class DeliverSettleInfo {

        @ApiModelProperty(value = "deliver id")
        @NotNull
        private Long deliverId;

        @ApiModelProperty(value = "本次交付金额")
        @NotNull
        @Pattern(regexp = "\\d+(\\.\\d+)?", message = " illegal")
        private String amount;

        @ApiModelProperty(value = "备注")
        private String remark;
    }

    @Data
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class DeliverPromotionInfo{


        @ApiModelProperty(value = "优惠金额")
        @NotNull
        @Pattern(regexp = "\\d+(\\.\\d+)?", message = " illegal")
        private String amount;

        @ApiModelProperty(value = "备注")
        private String remark;
    }
}
