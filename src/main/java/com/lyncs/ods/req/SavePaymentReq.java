package com.lyncs.ods.req;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * @author alex
 * @date 2022/3/11 22:17
 * @description
 */
@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SavePaymentReq {

    @ApiModelProperty(value = "ID")
    private Long id;

    @ApiModelProperty(value = "款项库配置项ID")
    @NotNull
    private Long paymentId;

    @ApiModelProperty(value = "属性信息")
    @NotNull
    private List<Map<String, Object>> attrs;
}
