package com.lyncs.ods.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author alex
 * @date 2022/03/01 23:20
 * @description
 */
public class LyncsOdsConstant {

    /**
     * 授权登录平台枚举
     */
    @AllArgsConstructor
    @Getter
    public enum AuthPlatform {
        /**
         * weChatWebApp
         */
        WECHAT_WEB_APP("weChatWebApp", "weChatWebApp", "微信网站应用"),

        /**
         * weChatMiniApp
         */
        WECHAT_MINI_APP("weChatMiniApp", "weChatMiniApp", "微信小程序");

        private final String key;
        private final String value;
        private final String desc;
    }

    /**
     * lyncs 通用字符
     */
    public static class Common {
        /**
         * 时间格式
         */
        public static final String DATE_PATTERN = "yyyyMMdd";
        /**
         * md5 加密前缀
         */
        public static final String MD5_PREFIX = "ods2022";
        /**
         * 空格 点 空格
         */
        public static final String SYMBOL_POINT = " · ";
        /**
         * 逗号
         */
        public static final String COMMA = ",";

        /**
         * 冒号
         */
        public static final String COLON = ":";

        /**
         * 空格
         */
        public static final String BLANK = " ";

        /**
         * 空
         */
        public static final String EMPTY = "";

        /**
         * 下划线
         */
        public static final String LINE = "_";

        /**
         * 中划线
         */
        public static final String MID_LINE = "-";
        /**
         * The constant SYMBOL_MID_LINE.
         */
        public static final String SYMBOL_MID_LINE = " - ";

        /**
         * 井号
         */
        public static final String WELL = "#";

        /**
         * The constant AT_SIGN.
         */
        public static final String AT_SIGN = "@";

        /**
         * 反斜线
         */
        public static final String REV_BIAS = "/";

        /**
         * 竖线
         */
        public static final String VERTICAL = "|";

        /**
         * NULL
         */
        public static final String NULL = null;

        /**
         * 百分号
         */
        public static final String PER_CENT = "%";
        /**
         * The constant SEMICOLON.
         */
        public static final String SEMICOLON = ";";
    }

    /**
     * lyncs 请求头
     */
    public static class Header {
        public static final String COOKIE = "x-lyncs-cookie";
        public static final String COOKIE_PT = "_pt";
        public static final String COOKIE_PC = "_pc";
        public static final String AUTH_PLATFORM = "platform";
        public static final String REQUEST_TIME = "request_time";
        public static final String REQUEST_ID = "x-lyncs-request-id";
    }

    /**
     * 用户状态枚举类
     */
    @AllArgsConstructor
    @Getter
    public enum UserStatus {
        /**
         * init
         */
        INIT(0, "初始状态：用户已创建，未完成注册流程"),
        /**
         * enable
         */
        ENABLE(1, "正常可用状态的用户"),
        /**
         * disable
         */
        DISABLE(2, "用户已被禁用");

        private final Integer key;
        private final String desc;
    }

    /**
     * spu状态枚举类
     */
    @AllArgsConstructor
    @Getter
    public enum SPUStatus {
        /**
         * enable
         */
        ENABLE(1, "可用"),
        /**
         * disable
         */
        DISABLE(2, "已禁用");

        private final Integer key;
        private final String desc;
    }

    /**
     * sku状态枚举类
     */
    @AllArgsConstructor
    @Getter
    public enum SKUStatus {
        /**
         * enable
         */
        ENABLE(1, "可用"),
        /**
         * disable
         */
        DISABLE(2, "已禁用");

        private final Integer key;
        private final String desc;
    }

    /**
     * 企业状态枚举类
     */
    @AllArgsConstructor
    @Getter
    public enum CompanyStatus {
        /**
         * virtual
         */
        VIRTUAL(0, "B类企业，并非实际存在的企业"),
        /**
         * enable
         */
        ENABLE(1, "正常创建的可用企业"),
        /**
         * disable
         */
        DISABLE(2, "企业已被禁用");

        private final Integer key;
        private final String desc;
    }

    /**
     * 消息发送枚举类
     */
    @AllArgsConstructor
    @Getter
    public enum MessageStatus {
        /**
         * init
         */
        INIT(0, "未发送"),
        /**
         * success
         */
        SUCCESS(1, "发送成功"),
        /**
         * fail
         */
        FAIL(2, "发送失败"),
        /**
         * expired
         */
        EXPIRED(3, "验证码已超时"),
        /**
         * verified
         */
        VERIFIED(4, "已验证");

        private final Integer key;
        private final String desc;
    }

    /**
     * Enable通用枚举
     */
    @AllArgsConstructor
    @Getter
    public enum EnableStatus {
        /**
         * enable
         */
        ENABLE(1, "正常"),
        /**
         * disable
         */
        DISABLE(2, "禁用");

        private final Integer key;
        private final String desc;
    }

    /**
     * 企业和 用户/企业 关联关系状态枚举类
     */
    @AllArgsConstructor
    @Getter
    public enum BindStatus {
        /**
         * bind
         */
        BIND(1, "已绑定"),
        /**
         * unbind
         */
        UNBIND(2, "未绑定");

        private final Integer key;
        private final String desc;
    }

    /**
     * 订单状态枚举类
     */
    @AllArgsConstructor
    @Getter
    public enum OrderStatus {
        /**
         * draft
         */
        DRAFT(0, "草稿"),
        /**
         * init
         */
        INIT(1, "已创建"),
        /**
         * partial delivery
         */
        PARTIAL_DELIVERY(2, "部分交付"),
        /**
         * delivered
         */
        DELIVERED(3, "全部交付");

        private final Integer key;
        private final String desc;
    }

    /**
     * 交付单状态枚举类
     */
    @AllArgsConstructor
    @Getter
    public enum DeliverStatus {
        /**
         * draft
         */
        DRAFT(0, "草稿"),
        /**
         * init
         */
        INIT(1, "已创建"),
        /**
         * partial settlement
         */
        PARTIAL_SETTLEMENT(2, "部分结算"),
        /**
         * settled
         */
        SETTLED(3, "已结算"),
        ;
        private final Integer key;
        private final String desc;
    }

    /**
     * 结算单状态枚举类
     */
    @AllArgsConstructor
    @Getter
    public enum SettlementStatus {
        /**
         * draft
         */
        DRAFT(0, "草稿"),
        /**
         * init
         */
        INIT(1, "已出账单"),
        /**
         * to be confirmed
         */
        TO_BE_CONFIRMED(2, "已结算待确认"),
        /**
         * confirmed
         */
        CONFIRMED(3, "已确认");

        private final Integer key;
        private final String desc;
    }
    /**
     * ods中sku状态枚举类
     */
    @AllArgsConstructor
    @Getter
    public enum TxnSkuStatus {
        /**
         * init
         */
        INIT(1, "已创建"),
        /**
         * partial delivery
         */
        PARTIAL_DELIVERY(2, "部分交付"),
        /**
         * delivered
         */
        DELIVERED(3, "全部交付");

        private final Integer key;
        private final String desc;
    }

    /**
     * VIP状态枚举类
     */
    @AllArgsConstructor
    @Getter
    public enum VIPStatus {
        /**
         * free
         */
        FREE(0, "free"),
        /**
         * vip
         */
        VIP(1, "vip"),
        /**
         * svip
         */
        SVIP(2, "svip");

        private final Integer key;
        private final String desc;
    }

    /**
     * 消息类型
     */
    @AllArgsConstructor
    @Getter
    public enum MessageType {

        /**
         * sms
         */
        SMS(1, "sms", "短信"),
        /**
         * email
         */
        EMAIL(2, "email", "邮件"),
        /**
         * weChat service
         */
        WECHAT_SERVICE(3, "wechat service", "微信服务通知"),
        /**
         * weChat subscription account
         */
        WECHAT_SUBSCRIPTION_ACCOUNT(4, "wechat subscription account", "微信订阅号"),
        /**
         * weChat service account
         */
        WECHAT_SERVICE_ACCOUNT(5, "wechat service account", "微信服务号");
        private final Integer key;
        private final String code;
        private final String desc;
    }

    /**
     * 交易系统中存在的类型
     */
    @AllArgsConstructor
    @Getter
    public enum TxnType {
        /**
         * order
         */
        ORDER(1, "order"),
        /**
         * deliver
         */
        DELIVER(2, "deliver"),
        /**
         * settlement
         */
        SETTLEMENT(3, "settlement"),
        ;
        private final Integer key;
        private final String desc;
    }

    /**
     * 交易系统中存在的日志编辑类型
     */
    @AllArgsConstructor
    @Getter
    public enum TxnEditType {
        /**
         * add
         */
        ADD(1, "add"),
        /**
         * edit
         */
        EDIT(2, "edit"),
        /**
         * delete
         */
        DELETE(3, "delete"),
        ;
        private final Integer key;
        private final String desc;
    }

    /**
     * 企业间的关系类型
     */
    @AllArgsConstructor
    @Getter
    public enum ContactType {
        /**
         * 供应商
         */
        SUPPLIER(1, "供应商"),
        /**
         * 客户
         */
        CUSTOMER(2, "客户"),
        ;
        private final Integer key;
        private final String desc;
    }

    /**
     * 企业角色类型
     */
    @AllArgsConstructor
    @Getter
    public enum CompanyRole {
        /**
         * ADMIN
         */
        ADMIN(1, "ADMIN"),
        ;
        private final Integer key;
        private final String desc;
    }

    /**
     * 系统配置说明
     */
    @AllArgsConstructor
    @Getter
    public enum SystemConfig {

        /**
         * authPlatform
         */
        AUTH_PLATFORM("authPlatform", "授权登录平台", AuthPlatform.class),
        /**
         * spuStatus
         */
        SPU_STATUS("spuStatus", "spu状态", SPUStatus.class),
        /**
         * skuStatus
         */
        SKU_STATUS("skuStatus", "sku状态", SKUStatus.class),
        /**
         * userStatus
         */
        USER_STATUS("userStatus", "用户状态", UserStatus.class),
        /**
         * companyStatus
         */
        COMPANY_STATUS("companyStatus", "企业状态", CompanyStatus.class),
        /**
         * bindStatus
         */
        BIND_STATUS("bindStatus", "关系绑定状态", BindStatus.class),
        /**
         * bindStatus
         */
        ORDER_STATUS("bindStatus", "订单状态", OrderStatus.class),
        /**
         * deliverStatus
         */
        DELIVER_STATUS("deliverStatus", "交付单状态", DeliverStatus.class),
        /**
         * settlementStatus
         */
        SETTLEMENT_STATUS("settlementStatus", "结算单状态", SettlementStatus.class),
        /**
         * messageType
         */
        MESSAGE_TYPE("messageType", "ods消息验证平台", MessageType.class),
        /**
         * contactType
         */
        CONTACT_TYPE("contactType", "合作关系类型", ContactType.class),
        /**
         * companyRole
         */
        COMPANY_ROLE("companyRole", "员工在企业的角色类型", CompanyRole.class),
        /**
         * Currency
         */
        CURRENCY("currency", "所有货币信息，请过滤status=1的货币", CurrencyEnum.class);

        private final String key;
        private final String desc;
        private Class<? extends Enum<? extends Enum<?>>> clazz;
    }
}
