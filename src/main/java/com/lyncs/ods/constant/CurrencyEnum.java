package com.lyncs.ods.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author alex
 * @date 2022-03-01 10:14
 */
@Getter
@AllArgsConstructor
public enum CurrencyEnum {
    /**
     * 人民币
     */
    CNY("CNY", "¥", LyncsOdsConstant.EnableStatus.ENABLE.getKey()),
    /**
     * 美元
     */
    USD("USD", "US$", LyncsOdsConstant.EnableStatus.ENABLE.getKey()),
    /**
     * 欧元
     */
    EUR("EUR", "€", LyncsOdsConstant.EnableStatus.ENABLE.getKey()),
    /**
     * 英镑
     */
    GBP("GBP", "£", LyncsOdsConstant.EnableStatus.ENABLE.getKey()),
    /**
     * 港币
     */
    HKD("HKD", "HK$", LyncsOdsConstant.EnableStatus.ENABLE.getKey()),
    /**
     * 日元
     */
    JPY("JPY", "¥", LyncsOdsConstant.EnableStatus.ENABLE.getKey()),

    AFN("AFN", "AFN", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    AED("AED", "AED", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    OMR("OMR", "OMR", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    AZN("AZN", "AZN", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    PKR("PKR", "₨", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    BHD("BHD", "BHD", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    BTN("BTN", "BTN", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    KPW("KPW", "₩", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    PHP("PHP", "₱", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    GEL("GEL", "GEL", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    KZT("KZT", "Т", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    KRW("KRW", "₩", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    KHR("KHR", "KHR", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    KGS("KGS", "KGS", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    QAR("QAR", "QR", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    KWD("KWD", "KWD", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    LAK("LAK", "₭", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    LBP("LBP", "LBP", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    MVR("MVR", "ރ", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    MYR("MYR", "RM", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    BDT("BDT", "৳", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    MNT("MNT", "₮", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    MMK("MMK", "BUK", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    NPR("NPR", "₨", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    SAR("SAR", "SAR", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    LKR("LKR", "₨", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    TJS("TJS", "ЅМ", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    THB("THB", "฿", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    UZS("UZS", "UZS", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    BND("BND", "$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    SGD("SGD", "S$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    SYP("SYP", "£", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    AMD("AMD", "AMD", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    YER("YER", "YER", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    IQD("IQD", "IQD", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    IRR("IRR", "IRR", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    ILS("ILS", "IS", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    INR("INR", "₹", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    TRY("TRY", "YTL", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    IDR("IDR", "Rp", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    JOD("JOD", "JOD", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    VND("VND", "₫", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    TWD("TWD", "NT$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    RUB("RUB", "₽", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    MOP("MOP", "MOP$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    ALL("ALL", "L", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    BYN("BYN", "Br", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    ISK("ISK", "kr", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    BAM("BAM", "KM", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    BGN("BGN", "лв", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    PLN("PLN", "zł", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    DKK("DKK", "kr", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    CZK("CZK", "Kč", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    HRK("HRK", "kn", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    CHF("CHF", "CHF", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    LTL("LTL", "Lt", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    RON("RON", "RON", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    MKD("MKD", "ден", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    MDL("MDL", "L", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    NOK("NOK", "kr", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    SEK("SEK", "kr", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    UAH("UAH", "₴", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    HUF("HUF", "Ft", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    GIP("GIP", "£", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    DZD("DZD", "AD.", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    EGP("EGP", "EGP", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    ETB("ETB", "ETB", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    AOA("AOA", "Kz", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    BWP("BWP", "P", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    BIF("BIF", "Fr", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    ERN("ERN", "ERN", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    CVE("CVE", "$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    GMD("GMD", "D", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    CDF("CDF", "Fr", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    DJF("DJF", "Fr", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    GNF("GNF", "Fr", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    KMF("KMF", "Fr", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    KES("KES", "Sh", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    LSL("LSL", "L", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    LRD("LRD", "$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    LYD("LYD", "LD.", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    RWF("RWF", "Fr", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    MGA("MGA", "MGA", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    MWK("MWK", "MK", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    MRO("MRO", "UM", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    MUR("MUR", "₨", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    MAD("MAD", "DH", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    MZN("MZN", "MTn", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    NAD("NAD", "$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    ZAR("ZAR", "R", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    NGN("NGN", "₦", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    STD("STD", "Db", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    SCR("SCR", "₨", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    SLL("SLL", "Le", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    SOS("SOS", "Sh", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    SZL("SZL", "L", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    TZS("TZS", "Sh", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    TND("TND", "TD", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    UGX("UGX", "Sh", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    ZMK("ZMK", "ZK", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    XOF("XOF", "Fr", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    XAF("XAF", "Fr", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    BSD("BSD", "$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    BBD("BBD", "$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    BZD("BZD", "$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    PAB("PAB", "PAB", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    DOP("DOP", "$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    CRC("CRC", "₡", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    CUC("CUC", "$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    CUP("CUP", "$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    HTG("HTG", "G", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    HNL("HNL", "L", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    CAD("CAD", "$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    SVC("SVC", "₡", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    MXN("MXN", "$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    NIO("NIO", "C$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    TTD("TTD", "$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    GTQ("GTQ", "Q", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    JMD("JMD", "$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    XCD("XCD", "$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    KYD("KYD", "$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    BMD("BMD", "$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    ARS("ARS", "$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    BOB("BOB", "BOB", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    BRL("BRL", "R$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    PYG("PYG", "₲", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    PEN("PEN", "PES", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    COP("COP", "$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    GYD("GYD", "$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    SRD("SRD", "$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    UYU("UYU", "$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    CLP("CLP", "$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    FKP("FKP", "£", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    ANG("ANG", "ƒ", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    AUD("AUD", "AUD", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    PGK("PGK", "K", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    FJD("FJD", "$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    WST("WST", "T", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    SBD("SBD", "$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    TOP("TOP", "T$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    VUV("VUV", "Vt", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    NZD("NZD", "NZ$", LyncsOdsConstant.EnableStatus.DISABLE.getKey()),
    XPF("XPF", "Fr", LyncsOdsConstant.EnableStatus.DISABLE.getKey());

    /**
     * 货币三位码
     */
    private String currency;
    /**
     * 货币描述符
     */
    private String description;
    /**
     * 状态：1.启用；2.禁用
     */
    private Integer status;
}
