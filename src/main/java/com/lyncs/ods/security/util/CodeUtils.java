package com.lyncs.ods.security.util;

import cn.hutool.core.util.StrUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Supplier;

/**
 * code generator
 *
 * @author alex
 * @date 2022-02-07 16:46
 */
public class CodeUtils {

    public static final String PREFIX_ORDER = "ORD";
    public static final String PREFIX_DELIVER = "DEL";
    public static final String PREFIX_SETTLE = "SET";
    public static final Integer PAD_LENGTH = 4;
    public static final SimpleDateFormat SDF = new SimpleDateFormat("yyyyMMddHHmmss");

    /**
     * 生成code
     *
     * @param prefix prefix
     * @param func   今日订单函数
     * @return code, eg: ORD202202070000000001
     */
    private static String generatorCode(String prefix, Supplier<Integer> func) {
        return getPrefixStr(prefix) + StrUtil.padPre(func.get().toString(), PAD_LENGTH, "0");
    }

    private static String getPrefixStr(String prefix) {
        return prefix + SDF.format(new Date());
    }
}
