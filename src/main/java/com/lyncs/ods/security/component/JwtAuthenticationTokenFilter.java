package com.lyncs.ods.security.component;

import com.lyncs.ods.common.web.RequestHolder;
import com.lyncs.ods.constant.LyncsOdsConstant;
import com.lyncs.ods.domain.AdminUserDetails;
import com.lyncs.ods.modules.user.model.UserAuthInfo;
import com.lyncs.ods.modules.user.model.UserInfo;
import com.lyncs.ods.modules.user.service.UserService;
import com.lyncs.ods.security.util.JwtTokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * JWT登录授权过滤器
 *
 * @author alex
 * @date 2022/01/25
 */
@Slf4j
@Component
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private UserService userService;
    @Value("${spring.profiles.active}")
    private String env;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain) throws ServletException, IOException {
        String authToken = RequestHolder.getToken();
        if (StringUtils.isEmpty(authToken) && "dev".equals(env)) {
            authToken = getDebuggerToken();
            log.info("dev generate debugger user, token:{}", authToken);
        }
        if (StringUtils.isNotEmpty(authToken)) {
            String openId = jwtTokenUtil.getOpenIdFromToken(authToken);
            log.info("checking openId:{}", openId);
            if (openId != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                UserDetails userDetails = userService.loadUserByOpenId(openId);
                if (jwtTokenUtil.validateToken(authToken, userDetails)) {
                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    log.info("authenticated openId:{}", openId);
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            }
        }
        response.addHeader(LyncsOdsConstant.Header.COOKIE, RequestHolder.getCookie());
        response.addHeader(LyncsOdsConstant.Header.REQUEST_ID, RequestHolder.getData(LyncsOdsConstant.Header.REQUEST_ID));
        chain.doFilter(request, response);
    }

    private String getDebuggerToken() {
        UserInfo userInfo = new UserInfo().setId(1L).setStatus(LyncsOdsConstant.UserStatus.ENABLE.getKey());
        UserAuthInfo userAuthInfo = new UserAuthInfo().setUserId(1L).setAppId("wx5fbafb66272c83fa").setAuthType(LyncsOdsConstant.AuthPlatform.WECHAT_WEB_APP.getKey()).setOpenId("o2yzl6Pp1CAOcV_LwrV-u4lXZ4Z4");
        UserDetails userDetails = new AdminUserDetails(userInfo, userAuthInfo);
        //UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        //SecurityContextHolder.getContext().setAuthentication(authentication);
        return jwtTokenUtil.generateToken(userDetails);
    }
}
