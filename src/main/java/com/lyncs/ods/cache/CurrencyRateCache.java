package com.lyncs.ods.cache;

import cn.hutool.cache.CacheUtil;
import cn.hutool.cache.impl.TimedCache;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.util.StrUtil;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author alex
 * @date 2022/03/20 22:23
 * @description
 */
public class CurrencyRateCache {

    /**
     * 默认缓存时长
     */
    private static final Long DEFAULT_TIMEOUT = DateUnit.DAY.getMillis();
    /**
     * 默认清理间隔时间 单位s
     */
    private static final Long CLEAN_TIMEOUT = DateUnit.DAY.getMillis();
    /**
     * currency:rate:USD:CNY:20220401
     */
    public static final String CACHE_KEY = "currency:rate:{}:{}:{}";
    /**
     * 缓存对象
     */
    public static TimedCache<String, BigDecimal> timedCache = CacheUtil.newTimedCache(DEFAULT_TIMEOUT);

    static {
        //启动定时任务
        timedCache.schedulePrune(CLEAN_TIMEOUT);
    }

    public static void put(String fromCurrency, String toCurrency, BigDecimal rate) {
        put(fromCurrency, toCurrency, rate, DEFAULT_TIMEOUT);
    }

    public static void put(String fromCurrency, String toCurrency, BigDecimal rate, Boolean clean) {
        put(fromCurrency, toCurrency, rate, DEFAULT_TIMEOUT, clean);
    }

    public static void put(String fromCurrency, String toCurrency, BigDecimal rate, Long expire) {
        put(fromCurrency, toCurrency, rate, expire, Boolean.TRUE);
    }

    /**
     * @param fromCurrency 愿货币
     * @param toCurrency   目标货币
     * @param rate         转换金额
     * @param expire       过期时间
     * @param clean        是否需要清理旧缓存
     */
    public static void put(String fromCurrency, String toCurrency, BigDecimal rate, Long expire, Boolean clean) {
        if (clean) {
            remove(fromCurrency, toCurrency);
        }
        timedCache.put(getCacheKey(fromCurrency, toCurrency), rate, expire);
    }

    /**
     * @param fromCurrency       原货币
     * @param toCurrency         目标货币
     * @param isUpdateLastAccess 是否禁止延迟缓存
     */
    public static BigDecimal get(String fromCurrency, String toCurrency, boolean isUpdateLastAccess) {
        return timedCache.get(getCacheKey(fromCurrency, toCurrency), isUpdateLastAccess);
    }

    public static BigDecimal get(String fromCurrency, String toCurrency) {
        return get(fromCurrency, toCurrency, Boolean.TRUE);
    }

    public static void remove(String fromCurrency, String toCurrency) {
        timedCache.remove(getCacheKey(fromCurrency, toCurrency));
    }

    public static void clear() {
        timedCache.clear();
    }

    public static String getCacheKey(String fromCurrency, String toCurrency) {
        return getCacheKey(fromCurrency, toCurrency, LocalDate.now());
    }

    public static String getCacheKey(String fromCurrency, String toCurrency, LocalDate date) {
        return StrUtil.format(CACHE_KEY, fromCurrency, toCurrency, date);
    }

}
