package com.lyncs.ods.resp;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author alex
 * @date 2022/3/12 14:02
 * @description
 */
@Data
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SettlementDetailInfoResp {

    @ApiModelProperty(value = "结算单ID")
    private Long settlementId;

    @ApiModelProperty(value = "总金额")
    private String totalAmount;

    @ApiModelProperty(value = "状态")
    private Integer status;

    @ApiModelProperty(value = "卖方公司简称")
    private String sellerName;

    @ApiModelProperty(value = "买方公司简称")
    private String buyerName;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "结算截止时间")
    private LocalDateTime deadline;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "创建者用户名称")
    private String creatorName;

    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "优惠信息")
    private JSONArray promotionInfos;

    @ApiModelProperty(value = "账款信息")
    private List<SettlementDeliverInfo> deliverInfoList;

    @Data
    @Accessors(chain = true)
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class SettlementDeliverInfo {

        @ApiModelProperty(value = "关联的交付单ID")
        private Long deliverId;

        @ApiModelProperty(value = "结算金额")
        private BigDecimal settleAmount;

        @ApiModelProperty(value = "款项创建时间")
        private LocalDateTime createTime;

        @ApiModelProperty(value = "款项信息")
        private JSONObject otherFee;
    }
}
