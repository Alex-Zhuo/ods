package com.lyncs.ods.resp;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author alex
 * @date 2022/2/6 15:00
 * @description
 */
@Data
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class StaffInfoResp {

    @ApiModelProperty(value = "用户昵称")
    private String nickname;

    @ApiModelProperty(value = "用户绑定手机号")
    private String phoneNo;

    @ApiModelProperty(value = "用户绑定邮箱")
    private String email;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "部门")
    private String dept;

    @ApiModelProperty(value = "岗位")
    private String post;

    @ApiModelProperty(value = "角色")
    private Integer role;

    @ApiModelProperty(value = "备注")
    private String remark;
}
