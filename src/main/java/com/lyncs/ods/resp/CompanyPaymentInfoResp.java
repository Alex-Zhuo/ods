package com.lyncs.ods.resp;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Map;

/**
 * @author alex
 * @date 2022/3/26 19:30
 * @description
 */
@Data
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CompanyPaymentInfoResp {

    @ApiModelProperty(value = "配置款项名称")
    private String name;

    @ApiModelProperty(value = "配置款项logo")
    private String logo;

    @ApiModelProperty(value = "款项属性信息")
    private List<JSONObject> attrs;
}
