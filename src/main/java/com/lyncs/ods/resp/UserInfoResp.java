package com.lyncs.ods.resp;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author alex
 * @date 2022/2/6 14:13
 * @description
 */
@Data
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class UserInfoResp {

    @ApiModelProperty(value = "用户头像url")
    private String avatarUrl;

    @ApiModelProperty(value = "用户昵称")
    private String nickname;

    @ApiModelProperty(value = "用户绑定手机号")
    private String phoneNo;

    @ApiModelProperty(value = "用户绑定邮箱")
    private String email;

    @ApiModelProperty(value = "用户昵称")
    private String weChatNickname;
}
