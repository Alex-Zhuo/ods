package com.lyncs.ods.resp;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.lyncs.ods.req.SaveDeliverReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * @author alex
 * @date 2022/2/26 20:26
 * @description
 */
@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Accessors(chain = true)
public class DeliverDetailInfoResp {

    @ApiModelProperty(value = "交付单ID")
    private Long deliverId;

    @ApiModelProperty(value = "总金额")
    private String totalAmount;

    @ApiModelProperty(value = "状态")
    private Integer status;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "卖方公司简称")
    private String sellerName;

    @ApiModelProperty(value = "买方公司简称")
    private String buyerName;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "创建者用户名称")
    private String creatorName;

    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "sku info")
    private List<SkuInfo> skuInfoList;

    @Data
    @Accessors(chain = true)
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class SkuInfo {

        @ApiModelProperty(value = "关联的订单ID")
        private Long orderId;

        @ApiModelProperty(value = "sku名称")
        private String name;

        @ApiModelProperty(value = "本次交易本SKU需要交付数量")
        private BigDecimal totalCount;

        @ApiModelProperty(value = "已交付商品数")
        private BigDecimal deliveredCount;

        @ApiModelProperty(value = "总价")
        private String totalAmount;

        @ApiModelProperty(value = "单价")
        private String amount;

        @ApiModelProperty(value = "单位")
        private String unit;

        @ApiModelProperty(value = "sku属性")
        private Map<String, String> attrs;
    }

    @ApiModelProperty(value = "款项 信息")
    private List<DeliverBillInfo> billInfos;

    @Data
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class DeliverBillInfo {

        @ApiModelProperty(value = "账款ID")
        private Long billId;

        @ApiModelProperty("款项金额")
        @TableField("fee_amount")
        private BigDecimal feeAmount;

        @ApiModelProperty("款项数")
        private Integer categoryCount;

        @ApiModelProperty(value = "款项 信息")
        private List<DeliverFeeInfo> feeInfoList;

        @ApiModelProperty("创建时间")
        private LocalDateTime createTime;

    }

    @Data
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class DeliverFeeInfo {

        @ApiModelProperty(value = "款项ID")
        private Long feeId;

        @ApiModelProperty(value = "款项名称")
        private String feeName;

        @ApiModelProperty(value = "款项金额")
        private String feeAmount;

        @ApiModelProperty("备注")
        private String remark;
    }
}
