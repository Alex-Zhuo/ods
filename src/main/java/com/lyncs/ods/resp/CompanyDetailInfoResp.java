package com.lyncs.ods.resp;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.lyncs.ods.modules.company.model.CompanyContactInfo;
import com.lyncs.ods.modules.company.model.CompanyInfo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * @author alex
 * @date 2022/2/2 14:38
 * @description
 */
@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CompanyDetailInfoResp {

    @ApiModelProperty(value = "全称")
    private String name;

    @ApiModelProperty(value = "简称")
    private String shortName;

    @ApiModelProperty(value = "所属行业")
    private String industry;

    @ApiModelProperty(value = "员工规模")
    private String scale;

    @ApiModelProperty(value = "头像")
    private String avatarUrl;

    @ApiModelProperty(value = "地址")
    private String address;

    public CompanyDetailInfoResp() {
    }

    public CompanyDetailInfoResp(CompanyInfo companyInfo) {
        this.name = companyInfo.getName();
        this.shortName = companyInfo.getShortName();
        this.industry = companyInfo.getIndustry();
        this.scale = companyInfo.getScale();
        this.avatarUrl = companyInfo.getAvatarUrl();
        this.address = companyInfo.getAddress();
    }

    public CompanyDetailInfoResp(CompanyContactInfo companyContactInfo) {
        //this.name = companyContactInfo.getPartnerContactName();
        //this.shortName = companyContactInfo.getPartnerContactName();
        this.industry = null;
        this.scale = null;
        this.avatarUrl = null;
        //this.address = companyContactInfo.getAddress();
    }
}
