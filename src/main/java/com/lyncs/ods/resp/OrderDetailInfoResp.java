package com.lyncs.ods.resp;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * @author alex
 * @date 2022/2/8 23:19
 * @description
 */
@Data
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class OrderDetailInfoResp {

    @ApiModelProperty(value = "订单ID")
    private Long orderId;

    @ApiModelProperty(value = "总金额")
    private String totalAmount;

    @ApiModelProperty(value = "状态")
    private Integer status;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "预计交付时间")
    private LocalDateTime estimatedDeliveryTime;

    @ApiModelProperty(value = "内部单号")
    private String innerCode;

    @ApiModelProperty(value = "卖方公司简称")
    private String sellerName;

    @ApiModelProperty(value = "买方公司简称")
    private String buyerName;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "创建者用户名称")
    private String creatorName;

    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "sku 详情")
    private List<SkuInfo> skuInfoList;

    @Data
    @Accessors(chain = true)
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class SkuInfo {

        @ApiModelProperty(value = "sku名称")
        private String name;

        @ApiModelProperty(value = "本次交易本SKU需要交付数量")
        private BigDecimal totalCount;

        @ApiModelProperty(value = "已交付商品数")
        private BigDecimal deliveredCount;

        @ApiModelProperty(value = "总价")
        private String totalAmount;

        @ApiModelProperty(value = "单价")
        private String amount;

        @ApiModelProperty(value = "单位")
        private String unit;

        @ApiModelProperty(value = "sku属性")
        private Map<String, String> attrs;
    }
}
