package com.lyncs.ods.resp;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author alex
 * @date 2022/2/5 21:34
 * @description
 */
@Data
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SpuAttrInfoResp {

    @ApiModelProperty(value = "子spu的id")
    private String subSpuId;

    @ApiModelProperty(value = "spu的id")
    private String spuId;

    @ApiModelProperty(value = "spu名称")
    private String name;

    @ApiModelProperty(value = "spu属性key")
    private String key;

    @ApiModelProperty(value = "是否必填:1.必填;0.非必填")
    private Boolean required;
}
