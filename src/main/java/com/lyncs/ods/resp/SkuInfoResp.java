package com.lyncs.ods.resp;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author alex
 * @date 2022/2/5 22:13
 * @description
 */
@Data
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SkuInfoResp {

    @ApiModelProperty(value = "sku的id")
    private Long skuId;

    @ApiModelProperty(value = "spu的id")
    private Long spuId;

    @ApiModelProperty(value = "sku名称")
    private String name;

    @ApiModelProperty(value = "货币")
    private String currency;

    @ApiModelProperty(value = "金额")
    private BigDecimal amount;

    @ApiModelProperty(value = "sku 图片地址")
    private String imgUrl;

    @ApiModelProperty(value = "sku 属性信息")
    private List<SkuAttr> attrs;

    @Data
    @Accessors(chain = true)
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class SkuAttr {

        @ApiModelProperty(value = "sku属性名称")
        private String name;

        @ApiModelProperty(value = "sku属性值")
        private String value;
    }
}
