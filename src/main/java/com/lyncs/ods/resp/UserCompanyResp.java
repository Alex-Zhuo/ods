package com.lyncs.ods.resp;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author alex
 * @date 2022/2/2 14:20
 * @description
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class UserCompanyResp extends CompanyShortInfoResp {

    @ApiModelProperty(value = "用户id")
    private Long userId;

    @ApiModelProperty(value = "上次登录的公司ID")
    private Long recentLoginCompanyId;
}
