package com.lyncs.ods.resp;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author alex
 * @date 2022/2/26 20:23
 * @description
 */
@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Accessors(chain = true)
public class DeliverPageResp {

    @ApiModelProperty(value = "当前页数")
    private Integer page;

    @ApiModelProperty(value = "当前页请求数量，默认20")
    private Integer pageSize;

    @ApiModelProperty(value = "总数")
    private Integer totalSize;

    @ApiModelProperty(value = "订单信息")
    private List<DeliverShortInfo> data;

    @Data
    @Accessors(chain = true)
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class DeliverShortInfo {

        @ApiModelProperty(value = "订单ID")
        private Long deliverId;

        @ApiModelProperty(value = "创建者用户id")
        private Long creatorName;

        @ApiModelProperty(value = "创建时间")
        private LocalDateTime createTime;

        @ApiModelProperty(value = "更新时间")
        private LocalDateTime updateTime;

        @ApiModelProperty(value = "卖方公司简称")
        private String sellerName;

        @ApiModelProperty(value = "买方公司简称")
        private String buyerName;

        @ApiModelProperty(value = "商品类目数")
        private Integer categoryCount;

        @ApiModelProperty(value = "sku 图片")
        private List<String> skuImages;

        @ApiModelProperty(value = "总金额")
        private Long totalAmount;

        @ApiModelProperty(value = "状态")
        private String status;
    }
}
