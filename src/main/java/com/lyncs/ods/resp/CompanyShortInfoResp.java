package com.lyncs.ods.resp;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author alex
 * @date 2022/2/2 14:33
 * @description
 */
@Data
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CompanyShortInfoResp {

    @ApiModelProperty(value = "公司id")
    private Long companyId;

    @ApiModelProperty(value = "通讯录备注名称")
    private String nickname;

    @ApiModelProperty(value = "公司头像url")
    private String avatarUrl;

    @ApiModelProperty(value = "公司地址")
    private String address;
}
