package com.lyncs.ods.resp;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author alex
 * @date 2022/3/12 13:54
 * @description
 */
@Data
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SettlementPageResp {

    @ApiModelProperty(value = "当前页数")
    private Integer page;

    @ApiModelProperty(value = "当前页请求数量，默认20")
    private Integer pageSize;

    @ApiModelProperty(value = "总数")
    private Integer totalSize;

    @ApiModelProperty(value = "订单信息")
    private List<ShortInfo> data;

    @Data
    @Accessors(chain = true)
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class ShortInfo {

        @ApiModelProperty(value = "结算单 ID")
        private Long id;

        @ApiModelProperty(value = "创建者用户id")
        private String creatorName;

        @ApiModelProperty(value = "创建时间")
        private LocalDateTime createTime;

        @ApiModelProperty(value = "更新时间")
        private LocalDateTime updateTime;

        @ApiModelProperty(value = "卖方公司ID")
        private Long sellerId;

        @ApiModelProperty(value = "卖方公司简称")
        private String sellerName;

        @ApiModelProperty(value = "买方公司ID")
        private Long buyerId;

        @ApiModelProperty(value = "买方公司简称")
        private String buyerName;

        @ApiModelProperty(value = "结算账款数")
        private Integer categoryCount;

        @ApiModelProperty(value = "总金额")
        private String totalAmount;

        @ApiModelProperty(value = "状态")
        private Integer status;

        @ApiModelProperty(value = "创建者用户所属公司ID")
        private Long createCompanyId;
    }
}
