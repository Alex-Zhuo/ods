package com.lyncs.ods.resp;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author alex
 * @date 2022/2/5 21:26
 * @description
 */
@Data
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SpuInfoResp {


    @ApiModelProperty(value = "spu id")
    private Long spuId;

    @ApiModelProperty(value = "spu名称")
    private String name;
}
