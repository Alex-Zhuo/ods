package com.lyncs.ods.domain;

import com.lyncs.ods.constant.LyncsOdsConstant;
import com.lyncs.ods.modules.user.model.UserAuthInfo;
import com.lyncs.ods.modules.user.model.UserInfo;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

/**
 * SpringSecurity需要的用户详情
 *
 * @author alex
 * @date 2022/01/25
 */
public class AdminUserDetails implements UserDetails {

    private final String appId;
    private final String authType;
    private final Integer status;
    private final Long userId;
    private final String openId;

    public AdminUserDetails(UserInfo userInfo, UserAuthInfo userAuthInfo) {
        this.appId = userAuthInfo.getAppId();
        this.authType = userAuthInfo.getAuthType();
        this.status = userInfo.getStatus();
        this.userId = userInfo.getId();
        this.openId = userAuthInfo.getOpenId();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        //返回当前用户的角色
        return List.of(new SimpleGrantedAuthority(authType));
    }

    @Override
    public String getPassword() {
        return openId;
    }

    @Override
    public String getUsername() {
        return userId.toString();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return LyncsOdsConstant.UserStatus.ENABLE.getKey().equals(status);
    }
}
