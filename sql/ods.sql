/*
 Navicat Premium Data Transfer

 Source Server         : 【local】MySQL
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : localhost:3306
 Source Schema         : ods

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 06/04/2022 00:07:15
*/

SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for app_setting
-- ----------------------------
DROP TABLE IF EXISTS `app_setting`;
CREATE TABLE `app_setting`
(
    `id`            bigint                                                         NOT NULL AUTO_INCREMENT COMMENT '自增主键id',
    `setting_key`   varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NOT NULL COMMENT '配置键',
    `setting_value` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '配置值',
    `status`        tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态:1.ENABLE;2.DISABLE',
    `create_time`   timestamp                                                      NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`   timestamp                                                      NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='公共配置表';

-- ----------------------------
-- Table structure for company_contact_info
-- ----------------------------
DROP TABLE IF EXISTS `company_contact_info`;
CREATE TABLE `company_contact_info`
(
    `id`              bigint                                                       NOT NULL AUTO_INCREMENT COMMENT '自增主键id',
    `company_id`      bigint                                                       NOT NULL COMMENT '企业id',
    `init_company_id` bigint                                                       NOT NULL COMMENT '被关联的B类企业初始id',
    `relation_id`     bigint                                                       NOT NULL COMMENT '被关联的企业id',
    `nickname`        varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '备注名称',
    `status`          tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态:1.已绑定;2.已解绑',
    `type`            tinyint(1) NOT NULL DEFAULT '1' COMMENT '关系类型:1.供应商;2.客户',
    `create_time`     timestamp                                                    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`     timestamp                                                    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='企业通讯录';

-- ----------------------------
-- Table structure for company_fee_info
-- ----------------------------
DROP TABLE IF EXISTS `company_fee_info`;
CREATE TABLE `company_fee_info`
(
    `id`          bigint                                                       NOT NULL AUTO_INCREMENT COMMENT '主键ID',
    `company_id`  bigint                                                       NOT NULL COMMENT '关联的企业ID',
    `fee_name`    varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '款项名称',
    `fee_code`    varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '款项code',
    `status`      tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：1.启用；2.禁用',
    `create_time` datetime                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` datetime                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='企业款项配置表';

-- ----------------------------
-- Table structure for company_info
-- ----------------------------
DROP TABLE IF EXISTS `company_info`;
CREATE TABLE `company_info`
(
    `id`                bigint                                                       NOT NULL AUTO_INCREMENT COMMENT '企业id',
    `name`              varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '企业名称',
    `short_name`        varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '企业简称',
    `industry`          varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '所属行业',
    `scale`             varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '员工规模',
    `avatar_url`        varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '企业头像url',
    `level_status`      tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态：0.体验;1.VIP;2.SVIP',
    `level_expire_time` timestamp NULL DEFAULT NULL COMMENT '企业当前等级过期时间',
    `telephone`         varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '电话号码',
    `fax`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '传真号码',
    `email`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '邮箱',
    `address`           varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '企业地址',
    `slogan`            varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '企业标语',
    `background_img`    varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '企业背景图URL',
    `status`            tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：1.启用；2.禁用',
    `creator`           bigint                                                       NOT NULL COMMENT '创建者用户id',
    `create_time`       timestamp                                                    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`       timestamp                                                    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='企业账号信息表';

-- ----------------------------
-- Table structure for company_invitation_log
-- ----------------------------
DROP TABLE IF EXISTS `company_invitation_log`;
CREATE TABLE `company_invitation_log`
(
    `id`                 bigint                                                        NOT NULL AUTO_INCREMENT COMMENT '自增主键id',
    `company_id`         bigint                                                        NOT NULL COMMENT '邀请企业id',
    `invited_company_id` bigint NULL COMMENT '受邀企业id',
    `type`               tinyint(1) NOT NULL DEFAULT '1' COMMENT '类型：1.分享订单；2.分享交付单；3.分享结算单；4.邀请企业加入；5.邀请员工加入',
    `key`                varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '短链key',
    `data`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '短链对应的数据',
    `send_log_id`        bigint                                                        NOT NULL COMMENT '发送邀请记录id(msg_send_log)',
    `expire_time`        timestamp                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '邀请过期时间',
    `status`             tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：0.待发送 1.已发送；2.已接受',
    `remark`             varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '备注',
    `creator`            bigint                                                        NOT NULL COMMENT '发起用户id',
    `create_time`        timestamp                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`        timestamp                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='企业邀请记录表';

-- ----------------------------
-- Table structure for company_payment_info
-- ----------------------------
DROP TABLE IF EXISTS `company_payment_info`;
CREATE TABLE `company_payment_info`
(
    `id`              bigint                                                        NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `company_id`      bigint                                                        NOT NULL COMMENT '企业ID',
    `payment_id`      bigint                                                        NOT NULL COMMENT '付款方式ID',
    `payment_setting` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '付款配置',
    `status`          tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：1.启用；2.禁用',
    `create_time`     timestamp                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`     timestamp                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='企业收款信息';

-- ----------------------------
-- Table structure for company_setting
-- ----------------------------
DROP TABLE IF EXISTS `company_setting`;
CREATE TABLE `company_setting`
(
    `id`            bigint                                                         NOT NULL AUTO_INCREMENT COMMENT '自增主键id',
    `company_id`    bigint                                                         NOT NULL COMMENT '企业id',
    `setting_key`   varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NOT NULL COMMENT '配置键',
    `setting_value` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '配置值',
    `status`        tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：1.启用；2.禁用',
    `create_time`   timestamp                                                      NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`   timestamp                                                      NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='企业账户配置表';

-- ----------------------------
-- Table structure for company_user_relation
-- ----------------------------
DROP TABLE IF EXISTS `company_user_relation`;
CREATE TABLE `company_user_relation`
(
    `id`          bigint                                                        NOT NULL AUTO_INCREMENT COMMENT '自增主键id',
    `company_id`  bigint                                                        NOT NULL COMMENT '企业id',
    `user_id`     bigint                                                        NOT NULL COMMENT '用户id',
    `nickname`    varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户昵称',
    `no`          varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '工号',
    `dept`        varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '部门',
    `post`        varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '岗位',
    `role`        tinyint(4) NULL COMMENT '角色:1.admin;',
    `remark`      varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '备注',
    `status`      tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：1.绑定；2.解绑',
    `create_time` timestamp                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='个人与企业关联关系表';

-- ----------------------------
-- Table structure for company_user_relation_log
-- ----------------------------
DROP TABLE IF EXISTS `company_user_relation_log`;
CREATE TABLE `company_user_relation_log`
(
    `id`          bigint    NOT NULL AUTO_INCREMENT COMMENT '自增主键id',
    `company_id`  bigint    NOT NULL COMMENT '企业id',
    `user_id`     bigint    NOT NULL COMMENT '用户id',
    `status`      tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：1.绑定；2.解绑',
    `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='企业与个人关联关系操作记录表';

-- ----------------------------
-- Table structure for deliver_info
-- ----------------------------
DROP TABLE IF EXISTS `deliver_info`;
CREATE TABLE `deliver_info`
(
    `id`                 bigint                                                       NOT NULL AUTO_INCREMENT COMMENT '交付单id',
    `seller_id`          bigint                                                                DEFAULT NULL COMMENT '卖方企业id',
    `buyer_id`           bigint                                                                DEFAULT NULL COMMENT '买方企业id',
    `category_count`     int                                                          NOT NULL COMMENT 'sku类别数',
    `currency`           varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL DEFAULT 'CNY' COMMENT '货币',
    `start_time`         timestamp                                                    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '交付创建时间',
    `status`             tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：0.草稿；1.已创建；2.部分结算；3.全部结算',
    `remark`             varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '备注',
    `creator`            bigint                                                       NOT NULL COMMENT '创建者用户id',
    `creator_company_id` bigint                                                       NOT NULL COMMENT '创建者所属企业ID',
    `last_updater`       bigint                                                       NOT NULL COMMENT '最后更新用户id',
    `create_time`        timestamp                                                    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`        timestamp                                                    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='交付单总表';

-- ----------------------------
-- Table structure for msg_send_log
-- ----------------------------
DROP TABLE IF EXISTS `msg_send_log`;
CREATE TABLE `msg_send_log`
(
    `id`          bigint    NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `type`        tinyint(4) NOT NULL DEFAULT '1' COMMENT '发送渠道:1.短信;2.邮箱;3.服务通知(微信);4.订阅号通知(微信);5.服务号通知(微信)',
    `company_id`  bigint NULL COMMENT '企业id',
    `user_id`     bigint NULL COMMENT '用户id',
    `content`     text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '发送内容',
    `status`      tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态：0.未发送；1.发送成功；2.发送失败',
    `remark`      varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '备注',
    `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='发送消息记录表';

-- ----------------------------
-- Table structure for msg_validate_log
-- ----------------------------
DROP TABLE IF EXISTS `msg_validate_log`;
CREATE TABLE `msg_validate_log`
(
    `id`          bigint                                                       NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `type`        tinyint(4) NOT NULL DEFAULT '1' COMMENT '发送渠道:1.短信;2.邮箱;3.服务通知(微信);4.订阅号通知(微信);5.服务号通知(微信)',
    `company_id`  bigint                                                                DEFAULT NULL COMMENT '企业id',
    `user_id`     bigint                                                       NOT NULL COMMENT '用户id',
    `code`        varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '验证码',
    `status`      tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态：0.未发送;1.发送成功;2.发送失败;3.已超时;4.已验证',
    `count`       tinyint                                                      NOT NULL DEFAULT '0' COMMENT '当前校验次数',
    `max_count`   tinyint                                                      NOT NULL DEFAULT '1' COMMENT '最大校验次数',
    `expire_time` timestamp NULL DEFAULT NULL COMMENT '过期时间',
    `create_time` timestamp                                                    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp                                                    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='消息验证流水表';

-- ----------------------------
-- Table structure for order_info
-- ----------------------------
DROP TABLE IF EXISTS `order_info`;
CREATE TABLE `order_info`
(
    `id`                      bigint                                                      NOT NULL AUTO_INCREMENT COMMENT '订单id',
    `seller_id`               bigint                                                               DEFAULT NULL COMMENT '卖方企业id',
    `buyer_id`                bigint                                                               DEFAULT NULL COMMENT '买方企业id',
    `inner_code`              varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci         DEFAULT NULL COMMENT '内部单号',
    `category_count`          int                                                         NOT NULL COMMENT 'sku类别数',
    `estimated_delivery_time` timestamp NULL DEFAULT NULL COMMENT '预计交付时间',
    `currency`                varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'CNY' COMMENT '货币',
    `start_time`              timestamp                                                   NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '订单开始时间',
    `status`                  tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：0.草稿；1.已创建；2.部分交付；3.全部交付',
    `remark`                  varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '备注',
    `creator`                 bigint                                                      NOT NULL COMMENT '创建者用户id',
    `creator_company_id`      bigint                                                      NOT NULL COMMENT '创建者所属企业ID',
    `last_updater`            bigint                                                      NOT NULL COMMENT '最后更新用户id',
    `create_time`             timestamp                                                   NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`             timestamp                                                   NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='预约单总表';

-- ----------------------------
-- Table structure for payment_setting
-- ----------------------------
DROP TABLE IF EXISTS `payment_setting`;
CREATE TABLE `payment_setting`
(
    `id`          bigint                                                        NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `name`        varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '收款方式名称',
    `code`        varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '收款方式code',
    `logo`        varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '收款方式logo',
    `attr_json`   varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '收款方式配置属性',
    `status`      tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：1.可用；2.禁用',
    `create_time` timestamp                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='收款方式配置表';

-- ----------------------------
-- Table structure for reverse_info
-- ----------------------------
DROP TABLE IF EXISTS `reverse_info`;
CREATE TABLE `reverse_info`
(
    `id`               bigint    NOT NULL AUTO_INCREMENT COMMENT '自增主键id',
    `type`             tinyint(4) NOT NULL COMMENT '类型：1.order;2.deliver;3.settlement',
    `reverse_info`     text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '撤销信息',
    `reverse_sub_info` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '撤销关联信息（订单和交付关联sku/结算关联交付信息）',
    `create_time`      timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`      timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='撤销记录表';

-- ----------------------------
-- Table structure for deliver_bill_info
-- ----------------------------
DROP TABLE IF EXISTS `deliver_bill_info`;
CREATE TABLE `deliver_bill_info`
(
    `id`             bigint                                                       NOT NULL AUTO_INCREMENT COMMENT '自增主键id',
    `deliver_id`      bigint NULL COMMENT '交付单id',
    `total_amount`   varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '账款总金额(冗余字段，值为所有款项金额之和)',
    `settled_amount` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '已结算金额',
    `currency`       varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL DEFAULT 'CNY' COMMENT '货币',
    `start_time`     timestamp                                                    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '结算时间',
    `status`         tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：1.已创建;2.部分结算;3.已结算',
    `remark`         varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '备注',
    `create_time`    timestamp                                                    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`    timestamp                                                    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='交付单账款信息表';

-- ----------------------------
-- Table structure for settlement_bill_info
-- ----------------------------
DROP TABLE IF EXISTS `settlement_bill_info`;
CREATE TABLE `settlement_bill_info`
(
    `id`             bigint                                                       NOT NULL AUTO_INCREMENT COMMENT '自增主键id',
    `settle_id`      bigint NULL COMMENT '结算单id',
    `settle_bill_id`      bigint NULL COMMENT '结算单账款id',
    `amount`   varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '本次结算金额',
    `start_time`     timestamp                                                    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '结算时间',
    `remark`         varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '备注',
    `create_time`    timestamp                                                    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='结算单单账款信息表';

-- ----------------------------
-- Table structure for txn_fee_detail
-- ----------------------------
DROP TABLE IF EXISTS `txn_fee_detail`;
CREATE TABLE `txn_fee_detail`
(
    `id`          bigint                                                       NOT NULL AUTO_INCREMENT COMMENT '自增主键id',
    `bill_id`     bigint                                                       NOT NULL COMMENT '账款ID',
    `fee_name`    varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '款项名称',
    `fee_amount`  varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '款项金额',
    `remark`      varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '备注',
    `status`      tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：1.可用;2.禁用/删除',
    `create_time` timestamp                                                    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp                                                    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='款项详情表';

-- ----------------------------
-- Table structure for settlement_info
-- ----------------------------
DROP TABLE IF EXISTS `settlement_info`;
CREATE TABLE `settlement_info`
(
    `id`                 bigint                                                       NOT NULL AUTO_INCREMENT COMMENT '结算单id',
    `seller_id`          bigint                                                                DEFAULT NULL COMMENT '卖方企业id',
    `buyer_id`           bigint                                                                DEFAULT NULL COMMENT '买方企业id',
    `total_amount`       varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '结算单总金额(冗余字段，值为所有款项金额之和)',
    `category_count`     int                                                          NOT NULL COMMENT '关联的账款数量',
    `currency`           varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL DEFAULT 'CNY' COMMENT '结算货币',
    `start_time`         timestamp                                                    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '结算开始时间',
    `status`             tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：0.草稿；1.已创建（已出账单）；2.结算中（已结算待确认）；3.已确认',
    `remark`             varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '备注',
    `creator`            bigint                                                       NOT NULL COMMENT '创建者用户id',
    `creator_company_id` bigint                                                       NOT NULL COMMENT '创建者所属企业ID',
    `last_updater`       bigint                                                       NOT NULL COMMENT '最后更新用户id',
    `deadline`           timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '结算截止时间',
    `create_time`        timestamp                                                    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`        timestamp                                                    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='结算单总表';

-- ----------------------------
-- Table structure for sku_attr_log
-- ----------------------------
DROP TABLE IF EXISTS `sku_attr_log`;
CREATE TABLE `sku_attr_log`
(
    `id`         bigint                                                         NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `company_id` bigint                                                         NOT NULL COMMENT '企业id',
    `spu_id`     bigint                                                         NOT NULL COMMENT 'spu id',
    `sku_id`     bigint                                                         NOT NULL COMMENT 'sku id',
    `name`       varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NOT NULL COMMENT 'sku属性名称',
    `value`      varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'sku属性值',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='sku详细信息表';

-- ----------------------------
-- Table structure for sku_detail
-- ----------------------------
DROP TABLE IF EXISTS `sku_detail`;
CREATE TABLE `sku_detail`
(
    `id`          bigint                                                         NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `sku_id`      bigint                                                         NOT NULL COMMENT 'sku id',
    `spu_id`      bigint                                                         NOT NULL COMMENT 'spu id',
    `name`        varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NOT NULL COMMENT 'sku属性名称',
    `value`       varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'sku属性值',
    `status`      tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：1.启用；2.禁用',
    `create_time` timestamp                                                      NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp                                                      NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='sku详细信息表';

-- ----------------------------
-- Table structure for sku_info
-- ----------------------------
DROP TABLE IF EXISTS `sku_info`;
CREATE TABLE `sku_info`
(
    `id`          bigint                                                        NOT NULL AUTO_INCREMENT COMMENT 'sku id',
    `spu_id`      bigint                                                        NOT NULL DEFAULT '0' COMMENT 'spu的id，值为0表示自定义SKU',
    `name`        varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT 'sku名称',
    `no`          varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '自定义货号',
    `company_id`  bigint                                                        NOT NULL COMMENT '企业id',
    `amount`      varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '单价',
    `currency`    varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NOT NULL DEFAULT 'CNY' COMMENT '货币',
    `amount_json` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '不同币种的单价信息',
    `unit`        varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '单位',
    `img_url`     varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'sku 图片地址',
    `remark`      varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '备注',
    `status`      tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：1.启用；2.禁用',
    `create_time` timestamp                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='sku信息总表';

-- ----------------------------
-- Table structure for spu_detail
-- ----------------------------
DROP TABLE IF EXISTS `spu_detail`;
CREATE TABLE `spu_detail`
(
    `id`          bigint                                                      NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `spu_id`      bigint                                                      NOT NULL COMMENT 'spu的id',
    `name`        varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'spu名称',
    `key`         varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT 'spu属性key',
    `required`    tinyint(1) NOT NULL COMMENT '是否必填:1.必填;0.非必填',
    `status`      tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：1.启用；2.禁用',
    `create_time` timestamp                                                   NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp                                                   NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='spu详细信息表';

-- ----------------------------
-- Table structure for spu_info
-- ----------------------------
DROP TABLE IF EXISTS `spu_info`;
CREATE TABLE `spu_info`
(
    `id`          bigint                                                       NOT NULL AUTO_INCREMENT COMMENT 'spu id',
    `name`        varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'spu名称',
    `industry`    varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '所属行业',
    `status`      tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：1.启用；2.禁用',
    `create_time` timestamp                                                    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp                                                    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='spu信息总表';

-- ----------------------------
-- Table structure for company_spu_relation
-- ----------------------------
DROP TABLE IF EXISTS `company_spu_relation`;
CREATE TABLE `company_spu_relation`
(
    `id`          bigint    NOT NULL AUTO_INCREMENT COMMENT '自增主键id',
    `company_id`  bigint    NOT NULL COMMENT '企业id',
    `spu_id`      bigint    NOT NULL COMMENT '用户id',
    `status`      tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：1.绑定；2.解绑',
    `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='企业与spu关联表';

-- ----------------------------
-- Table structure for txn_sku_detail
-- ----------------------------
DROP TABLE IF EXISTS `txn_sku_detail`;
CREATE TABLE `txn_sku_detail`
(
    `id`              bigint                                                        NOT NULL AUTO_INCREMENT COMMENT '自增主键id',
    `order_id`        bigint                                                                 DEFAULT NULL COMMENT '订单id',
    `deliver_id`      bigint                                                                 DEFAULT NULL COMMENT '交付单id',
    `sku_id`          bigint                                                        NOT NULL COMMENT 'sku的id',
    `total_count`     varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '本次交易本SKU需要交付数量',
    `delivered_count` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '已交付商品数',
    `amount`    varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci           DEFAULT NULL COMMENT '本次交易本SKU单价（同一个SKU在不同订单有不同价格）',
    `total_amount`    varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci           DEFAULT NULL COMMENT '总金额',
    `start_time`      timestamp                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '交易开始时间',
    `status`          tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：1.已创建；2.部分交付；3.全部交付',
    `remark`          varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '备注',
    `creator`         bigint                                                        NOT NULL COMMENT '创建者用户id',
    `last_updater`    bigint                                                        NOT NULL COMMENT '最后更新用户id',
    `create_time`     timestamp                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`     timestamp                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='SKU交易详情';

-- ----------------------------
-- Table structure for user_auth_info
-- ----------------------------
DROP TABLE IF EXISTS `user_auth_info`;
CREATE TABLE `user_auth_info`
(
    `id`            bigint                                                        NOT NULL AUTO_INCREMENT COMMENT '自增主键id',
    `user_id`       bigint                                                        NOT NULL COMMENT '用户id',
    `app_id`        varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT 'appId',
    `auth_type`     varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '用户来源类型:WeChatMiniApp/WeChatWebApp...',
    `open_id`       varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '第三方授权ID',
    `nickname`      varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '第三方用户昵称',
    `union_id`      varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '第三方授权UnionID',
    `access_token`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT 'access_token',
    `refresh_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '刷新access_token的凭证',
    `create_time`   timestamp                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`   timestamp                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='第三方账号信息授权表';

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info`
(
    `id`                      bigint                                                       NOT NULL AUTO_INCREMENT COMMENT '用户id',
    `avatar_url`              varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '用户头像url',
    `username`                varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户姓名',
    `phone`                   varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '用户绑定手机号',
    `email`                   varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '用户绑定邮箱',
    `recent_login_company_id` bigint                                                                DEFAULT NULL COMMENT '上次登录的企业ID',
    `status`                  tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态：0.已创建；1.可用；2.已禁用',
    `create_time`             timestamp                                                    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`             timestamp                                                    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='个人账号信息表';

-- ----------------------------
-- Table structure for user_login_log
-- ----------------------------
DROP TABLE IF EXISTS `user_login_log`;
CREATE TABLE `user_login_log`
(
    `id`          bigint                                                       NOT NULL AUTO_INCREMENT COMMENT '自增主键id',
    `token`       varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '第三方登录token',
    `user_id`     bigint                                                       NOT NULL COMMENT '用户id',
    `company_id`  bigint                                                                DEFAULT NULL COMMENT '登录企业id',
    `type`        varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'WeChatWebApp' COMMENT '登录方式:WeChatMiniApp/WeChatWebApp/Mobile...',
    `remark`      varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '备注',
    `create_time` timestamp                                                    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='用户登录日志';

-- ----------------------------
-- Table structure for txn_operate_info
-- ----------------------------
DROP TABLE IF EXISTS `txn_operate_info`;
CREATE TABLE `txn_operate_info`
(
    `id`           bigint    NOT NULL AUTO_INCREMENT COMMENT '自增主键id',
    `type`         tinyint(4) NOT NULL COMMENT '类型：1.order;2.deliver;3.settlement',
    `ref_id`       bigint    NOT NULL COMMENT '类型关联ID',
    `company_id`   bigint    NOT NULL COMMENT '企业ID',
    `creator_id`   bigint    NOT NULL COMMENT '创建用户ID',
    `operate_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '操作类型:1.置顶',
    `status`       tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：1.可用；2.删除',
    `create_time`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='ods操作信息';

-- ----------------------------
-- Table structure for txn_edit_log
-- ----------------------------
DROP TABLE IF EXISTS `txn_edit_log`;
CREATE TABLE `txn_edit_log`
(
    `id`                  bigint    NOT NULL AUTO_INCREMENT COMMENT '自增主键id',
    `type`                tinyint(4) NOT NULL COMMENT '类型：1.order;2.deliver;3.settlement',
    `ref_id`              bigint    NOT NULL COMMENT '类型关联ID',
    `operator`            bigint    NOT NULL COMMENT '用户ID',
    `operator_company_id` bigint    NOT NULL COMMENT '用户所属企业ID',
    `operate_type`        tinyint(1) NOT NULL COMMENT '操作类型：1.新增;2.修改;3.删除',
    `old_content`         text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '更新前内容',
    `new_content`         text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '更新后内容',
    `create_time`         timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='ods编辑历史记录';

-- ----------------------------
-- Table structure for currency_rate_setting
-- ----------------------------
DROP TABLE IF EXISTS `currency_rate_setting`;
CREATE TABLE currency_rate_setting
(
    `id`            bigint      NOT NULL AUTO_INCREMENT COMMENT '自增主键id',
    `from_currency` VARCHAR(10) NOT NULL COMMENT '原货币',
    `to_currency`   VARCHAR(10) NOT NULL COMMENT '目标货币',
    `rate`          VARCHAR(20) NOT NULL COMMENT '汇率',
    `date`          DATE        NOT NULL COMMENT '日期',
    `status`        TINYINT(1) NOT NULL COMMENT '状态: 1.可用;2.禁用',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='汇率配置表';

SET
FOREIGN_KEY_CHECKS = 1;


INSERT INTO user_info (id, avatar_url, username, phone, email, recent_login_company_id, status, create_time,
                       update_time)
VALUES (1,
        'https://res.klook.com/image/upload/v1639722469/UED%20Team%EF%BC%88for%20DE%20only%EF%BC%89/System%20Icon/Hotel/Public%20icon/icon_time_time_s.png',
        'alex', '15297746757', 'alex@idwarf.cn', null, 1, now(), now());
INSERT INTO user_auth_info (id, user_id, app_id, auth_type, open_id, nickname, union_id, access_token, refresh_token,
                            create_time, update_time)
VALUES (1, 1, 'wx5fbafb66272c83fa', 'weChatWebApp', 'alex_test', 'alex_test', 'alex_test_union_id',
        'alex_test_access_token', 'alex_test_refresh_token', now(), now());
